#!/usr/bin/env python

from setuptools import setup

setup(
    name='SGSISoft',
    version='1.0',
    description='Sistema de Gestion de la seguridad de la informacion',
    author='Andres Betin',
    author_email='andresdavidbetin@gmail.com',
    url='http://sgsisoft-sgsisoft.rhcloud.com',
    install_requires=['Django<=1.4.6'],
)
