# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.contrib.auth.models import User, Group
from django.contrib.sites.models import Site
from email_confirm_la.models import EmailConfirmation
from notifications.models import Notification
from user_sessions.models import Session

from django.contrib import admin

#admin.site.register(Log)

#quitando registro de aplicaciones
#admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.unregister(Site)
del admin.site._registry[Session] #Eliminacion de session
del admin.site._registry[Notification] #Eliminacion de notification
del admin.site._registry[EmailConfirmation] #Eliminacion de emailConfirmations