# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

# Create your views here.
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ObjectDoesNotExist
from gestion_documental.servicios import ActividadServicios
from gestion_roles.servicios import PermisoServicios

from util.Comunicaciones.NotificacionesUtil import NotificacionesUtil
from util.PermisosUtil import permisos_req
from util.Util import *

import json
import redis

from django.db.models.signals import post_save
from django.dispatch import receiver

from notifications.models import Notification

actividadServicios= ActividadServicios()
notificacionUtil= NotificacionesUtil()
permisoServicio= PermisoServicios()

def logout_view(request):
    auth.logout(request)
    # Redirect to a success page.
    return Util.redireccionar('inicio')

def login(request):
    if request.method == 'POST':
        username = request.POST['usuario']
        password = request.POST['contrasena']
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            if user.is_staff:
                auth.login(request, user)
                return Util.redireccionar('inicio')
            else:
                try:
                    auth.login(request, user)
                    return Util.redireccionar('inicio')
                except ObjectDoesNotExist:
                    print 'no -esta autenticado'
                    return Util.pagina(request, 'login.html', {'error_ingreso': True})
        else:
            return Util.pagina(request, 'login.html', {'error_ingreso': True})
    else:
        usuario = request.user

        if usuario is not AnonymousUser and usuario.is_active:
            return Util.redireccionar('inicio')
        else:
            return Util.pagina(request, 'login.html', {})


@login_required(login_url="/login", redirect_field_name=None)
@permisos_req
def home(request):
    request.user.menu = permisoServicio.getMenuByUser(request.user)
    actividades = actividadServicios.getFechasActividades(request.user)# TODO: revisar por que se solicitan actividades
    return Util.pagina(request, "inicio.html", {'actividades': actividades})

# def notificationRender(request): TODO: eliminar o reimplementar
#     usuario = request.user
#     noti = notificacionUtil.getNotificacionesByUsuario(usuario)
#     return Util.pagina(request, 'gestion_notificaciones/actividad_listar.html', {'notificaciones': noti})

# def notificationDetailRender(request, id): TODO: eliminar o reimplementar
#     notificacion = get_object_or_404(Notificaciones, pk=id)
#     notificacion.vista = True
#     notificacion.save()
#     id = notificacion.actividad.id
#
#     return activityRender(request, id)

@login_required
def notificationsRead(request):
    """
    [GET] Permite obtener la lista de las notificaciones asociadas al usuario en sesion
    :param request:  Request.
    :return: Lista de notificaciones asociadas al usuario en sesion
    """
    if request.is_ajax():
        notifications = notificacionUtil.getNotificacionesByUser(request.user)
        respuesta = [notificacionUtil.getDTO(notification) for notification in notifications]
        serializado = json.dumps(respuesta)
        a= serializado
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def riesgosRender(request):
    return Util.pagina(request, '404.html',{})

@receiver(post_save, sender=Notification)
def on_notification_post_save(sender, **kwargs):
    """
    This method is called when a notification is saved. Create a redis connection with the server and send the
    notification
    :param sender: Notificacion almacenada.
    :param kwargs: variables adicionales de notificacion. Funcionan como metadata.
    :return:
    """
    redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)

    notification = kwargs['instance']
    recipient = notification.recipient

    print "esta es la notificacion"
    print notification.__dict__


    for session in recipient.session_set.all():
        redis_client.publish(
            'notifications.%s' % session.session_key,
            json.dumps(
                dict(
                    timestamp=notification.timestamp.isoformat(),
                    recipient=notification.recipient.username,
                    actor=notification.actor.username,
                    verb=notification.verb,
                    action_object=notification.action_object,
                    target=notification.target,
                    description=notification.description
                )
            )
        )