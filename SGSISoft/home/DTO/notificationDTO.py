# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

class NotificationDTO():#TODO: usar clase general DTO, la cual debería ir en util o en algo generico

    def __init__(self, notification):
        self.action_object = notification.action_object
        self.actor = notification.actor.username
        self.description = notification.description
        self.recipient = notification.recipient.username
        self.target = notification.target
        self.timestamp = notification.timestamp.isoformat()
        self.verb = notification.verb

    def toJSON(self): #TODO: eliminar este metodo. debe salir de DTO.
        return self.__dict__
