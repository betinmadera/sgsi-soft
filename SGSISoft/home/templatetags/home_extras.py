__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_documental.models import TipoActividad, EstadoActividad, TipoDocumento, EstadoDocumento
from django import template
from util.Constantes import Constantes
from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.utils import simplejson
from django.utils.safestring import mark_safe

register=template.Library()

@register.filter(name='enum_activity')
def enum_activity(value, enum):
    if enum=="tipoActividad":
        return TipoActividad.get(value).getNombre()
    elif enum=="estadoActividad":
        return EstadoActividad.get(value).getNombre()
    else:
        return ""

@register.filter(name='enum_document')
def enum_document(value, enum):
    if enum=="tipoDocumento":
        return TipoDocumento.get(value).getNombre()
    elif enum=="estadoDocumento":
        return EstadoDocumento.get(value).getNombre()
    else:
        return ""

@register.filter(name='enum_style_activity')
def enum_style_activity(value):
    return EstadoActividad.get(value).estilo

@register.filter(name='enum_style_document')
def enum_style_document(value):
    return EstadoDocumento.get(value).estilo

@register.simple_tag
def is_in_role(rol, list):
    exist = False
    for item in list:
        if item >= rol.getValor():
            exist=True
            break
    return exist

# settings value
@register.simple_tag
def constants_value(tag):
    print eval("Constantes."+tag);
    return eval("Constantes."+tag);