# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.conf.urls import patterns, url
from home.views import *

urlpatterns = patterns('',
        url(r'^$', home, name='inicio'),
        url(r'^login$', login, name='login'),
        url(r'^logout$', logout_view, name='logout'),
        #url(r'^notification$', notificationRender, name='notificaciones'), TODO: eliminar o reimplementar
        #url(r'^notification/([\d]+)$', notificationDetailRender, name='eNotificacion'), TODO: eliminar o reimplementar
        url(r'^notifications$', notificationsRead, name='notifications_ajax_read'),
)