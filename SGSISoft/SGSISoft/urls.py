__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.conf.urls import patterns, include, url
from django.contrib import admin

#from model_report import report
import notifications
from django.conf import settings

admin.autodiscover()
#report.autodiscover()

urlpatterns = patterns('',

                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url('^inbox/notifications/', include(notifications.urls)),
                       url(r'', include('user_sessions.urls', 'user_sessions')),
                       url(r'^pass/', include('password_reset.urls')), #TODO Corregir paginas
                       url(r'^', include('home.urls')),
                       url(r'^autoevaluacion/', include('autoevaluacion.urls')),
                       url(r'^activos/', include('gestion_activos.urls')),
                       url(r'^documental/', include('gestion_documental.urls')),
                       url(r'^riesgos/', include('gestion_riesgos.urls')),
                       url(r'^roles/', include('gestion_roles.urls')),
                       url(r'^reports/', include('reports.urls')),
                       url(r'^upload/', include('fileupload.urls')),
                       #url(r'^reportes/', include('model_report.urls')),


)
urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))