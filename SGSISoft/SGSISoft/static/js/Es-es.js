jQuery.labels= {
    "sEmptyTable":     "No hay datos en la tabla",
    "sInfo":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
    "sInfoEmpty":      "Mostrando 0 a 0 de 0 entradas",
    "sInfoFiltered":   "(filtrados de _MAX_ entradas totales)",
    "sInfoPostFix":    "",
    "sInfoThousands":      ",",
    "sLengthMenu":     "Mostrando _MENU_ entradas",
    "sLoadingRecords": "Cargando...",
    "sProcessing":     "Procesando...",
    "sSearch":         "Buscar:",
    "sZeroRecords":    "No hay entradas que coincidan con la búsqueda",
    "oPaginate": {
        "sFirst":      "Primero",
        "sLast":       "Ultimo",
        "sNext":       "Siguiente",
        "sPrevious":   "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar ascendentemente",
        "sSortDescending": ": Activar para ordenar Descendentemente"
    }
}
