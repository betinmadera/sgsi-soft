/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

function ProyectoFormView() {
    this.activeSelected = null;
    this.amenazaSelected = null;
    this.id = null;
    this.edit = false;
    this.model = {};
    this.valoracionesAmenazas = {};
    this.valoracionesActivos = {};
    this.valoracionesSalvaguardas = {};
    this.valoracionesActivosAmenazas = {};
    this.valoracionAmenazasSalvaguardas = {};

    this.storeSalvaguardas = [];
    this.form = $('#formProject')
    this.lastMethology = 0;
    this.dataValoracionActivos = {
        'requerimientos' : [],
        'activos' : [],
        'valoraciones': []

    };
    this.dataValoracionAmenazas = {
        'requerimientos' : [],
        'amenazas' : [],
        'valoraciones': []

    };
    this.dataValoracionActivosAmenazas = {
        'requerimientos' : []

    };

    this.objectHandsontable = {
        valoracionActivos : null,
        valoracionAmenazas: null,
        valoracionSalvaguardas: null,
        valoracionAmenazasActivo: null,
        valoracionSalvaguardasAmenaza: null


    };



    this.render = function() {
        if(this.edit){
            this.renderPanelTabs();
            this.renderActivos(this.model.activos);
            this.renderAmenazas(this.model.amenazas);
            this.renderFields();
            this.renderMatriz();
            this.renderMatrizAmenazas();
            this.renderActivoAmenazaTree();
            this.renderAmenazaSalvaguardaTree();
            this.renderValoracionesActivoVsAmenaza(this.valoracionesActivosAmenazas);
            this.renderValoracionesAmenazaVsSalvaguarda(this.valoracionAmenazasSalvaguardas);
            this.getSalvaguardas();

        }
    }

    this.add = function() {
        var url = "/riesgos/project/create";

        var arrayActivos = [];
        var arrayAmenazas = [];

        arrayActivos = this.convertToArray(collectionActivo);
        arrayAmenazas = this.convertToArray(collectionAmenaza);

        var objeto = {
            nombre : $('#nombre').val(),
            descripcion : $('#descripcion').val(),
            metodologia : $('#metodologia').val(),
            alcance : $('#alcance').val(),
            objetivo : $('#objetivo').val(),
            'activos' : JSON.stringify(arrayActivos),
           'amenazas' : JSON.stringify(arrayAmenazas)
        };

        if(this.validateForm() && arrayActivos.length > 0 && arrayAmenazas.length > 0 ){
            constantes.peticionGet(url, objeto, this.addSuccess);
        }else{
            console.log('Faltan datos para envio !!');
        }
    }
    this.update = function(){
        var filter = this.filterElementsNews();
        var url = "/riesgos/project/update";
        var arrayActivos = this.convertToArray(filter.activos);
        var arrayAmenazas = this.convertToArray(filter.amenazas);


        var objeto = {
            id_proyecto: this.id,
            nombre : $('#nombre').val(),
            descripcion : $('#descripcion').val(),
            metodologia : $('#metodologia').val(),
            alcance : $('#alcance').val(),
            objetivo : $('#objetivo').val(),
            'activos' : JSON.stringify(arrayActivos),
           'amenazas' : JSON.stringify(arrayAmenazas)
        };

        if(this.validateForm() && collectionActivo.length > 0 && collectionAmenaza.length > 0){
            constantes.peticionGet(url, objeto, this.updateSuccess);
            //Guardado de valoraciones
            this.saveAssessmentsActivos();
            this.saveAssessmentsAmenazas();
            this.saveAssessmentsSalvaguardas();
            this.saveAssessmentsActivosAmenazas();
            this.saveAssessmentsAmenazasSalvaguardas();



        }else{
            constantes.growl.warning("Faltan datos para envío");
        }
    }
    this.remove = function(id,typeCollection){
        var url = "/riesgos/project/removeElement";
        var objeto = {
            id_proyecto: this.id,
            id_element: id,
            type : typeCollection
        }
        if(this.edit){
            constantes.peticionGet(url, objeto,this.RemoveElementSuccess);
        }else{
            buscadores.removeElementArray(id,typeCollection);
        }
    }
    /*
    Guarda valoraiones de los activos
     */
    this.saveAssessmentsActivos = function(){
        var url = "/riesgos/project/assessment/active";
        this.dataValoracionActivos.valoraciones = this.getValoracionesActivos();
        var objeto = {
            proyecto: this.model.id,
            requerimientos: JSON.stringify(this.dataValoracionActivos.requerimientos),
            activos: JSON.stringify(this.dataValoracionActivos.activos),
            valoraciones: JSON.stringify(this.dataValoracionActivos.valoraciones)
        }

        if(this.dataValoracionActivos.valoraciones.length > 0 ){
            constantes.peticionGet(url,objeto,this.saveAssessmentsSuccess)
        }
    }
    /*
    Guarda valoraciones de amenazas
     */
    this.saveAssessmentsAmenazas = function(){
        var url = "/riesgos/project/assessment/threat";
        this.dataValoracionAmenazas.valoraciones = this.getValoracionesAmenazas();
        var objeto = {
            proyecto: this.model.id,
            amenazas: JSON.stringify(this.dataValoracionAmenazas.amenazas),
            valoraciones: JSON.stringify(this.dataValoracionAmenazas.valoraciones)
        }
        if(this.dataValoracionAmenazas.valoraciones.length > 0){
            constantes.peticionGet(url,objeto,this.saveAssessmentsAmenazasSuccess);
        }
    }
    /*
    Guarda valoraciones Activos/Amenazas
     */
    this.saveAssessmentsActivosAmenazas = function(){
        var url = "/riesgos/project/assessment/activeThreat";

        var grid = this.objectHandsontable['valoracionAmenazasActivo'];
        var data = [];

        grid.data.each(function(object){
            if(!grid.isBranch(object.id) && object.$parent > 0){
             data.push(object);
            }
        });

        var objeto = {
            proyecto: this.model.id,
            requerimientos: JSON.stringify(this.dataValoracionActivosAmenazas.requerimientos),
            data: JSON.stringify(data)
        }

        if(data.length > 0){
            constantes.peticionGet(url,objeto,this.saveAssessmentsActivosAmenazasSuccess);
        }
    }
    /*
    Guarda valoraciones Activos/Amenazas Resultados
     */
    this.saveAssessmentsActivosAmenazasResultados = function(){
        var url = "/riesgos/project/assessment/activeThreatResult";

        var objeto = {
            proyecto: this.model.id
        }

        constantes.peticionGet(url,objeto,this.saveAssessmentsActivosAmenazasSuccess);
    }
        /*
    Guarda valoraciones Amenazas/Salvaguardas
     */
    this.saveAssessmentsAmenazasSalvaguardas = function(){
        var url = "/riesgos/project/assessment/threatSafeguarding";

        var grid = this.objectHandsontable['valoracionSalvaguardasAmenaza'];
        var data = [];

        grid.data.each(function(object){
            if(!grid.isBranch(object.id) && object.$parent > 0){
             data.push(object);
            }
        });

        var objeto = {
            proyecto: this.model.id,
            data: JSON.stringify(data)
        }

        if(data.length > 0 ){
            constantes.peticionGet(url,objeto,this.saveAssessmentsAmenazasSalvaguardasSuccess);
        }
    }

    /*
    Guarda valoraciones salvaguardas
     */
    this.saveAssessmentsSalvaguardas = function(){
        var url = "/riesgos/project/assessment/safeguarding";

        var grid = this.objectHandsontable['valoracionSalvaguardas'];

        var data = [];

        grid.data.each(function(object){
            if(!grid.isBranch(object.id) && object.$parent != 0 && typeof (object.valoracion) != "undefined"){
                if(object.valoracion != "n.a." && object.valoracion == ""){
                    object["valoracion"] = "L0";
                }
                data.push(object);
            }
        });
        console.log(data);
        if(data.length > 0){
            var objeto = {
                proyecto: this.model.id,
                data: JSON.stringify(data)
            }
            if(data.length > 0){
                constantes.peticionGet(url,objeto,this.saveAssessmentsSalvaguardasSuccess);
            }
        }
    }

    this.getProyecto = function(data){
        proyecto.model = data['proyecto'];
        proyecto.valoracionesAmenazas = data['ValoracionesAmenazas'];
        proyecto.valoracionesActivos = data['ValoracionesActivos'];
        proyecto.valoracionesSalvaguardas = data['ValoracionesSalvaguardas'];
        proyecto.valoracionesActivosAmenazas = data['ValoracionesActivosAmenazas'];
        proyecto.valoracionAmenazasSalvaguardas = data['ValoracionAmenazasSalvaguardas'];
        proyecto.render();
    }
    this.getSalvaguardas = function(){
        var url = "/riesgos/safeguarding/safeguards/read";
        var objeto = {}
        constantes.peticionGet(url, objeto,proyecto.getSalvaguardasSuccess);
    }
    this.loadAmenazasByMetodologia = function(event){
        var id_metodologia = event.currentTarget.value;

        if(id_metodologia != proyecto.lastMethology){
            if(proyecto.edit){
                console.log("Está editando un proyecto");
            }else{

                var url = "/riesgos/methodology/threats/read";
                var objeto = {
                    id_metodologia : parseInt(id_metodologia)
                }
                constantes.peticionGet(url, objeto,proyecto.loadAmenazasSuccess);
            }
            proyecto.lastMethology = id_metodologia;
        }
    }
    this.renderFields = function(){
        $('#nombre').val(this.model.nombre);
        $('#descripcion').val(this.model.descripcion);
        $('#alcance').val(this.model.alcance);
        $('#objetivo').val(this.model.objetivo);
        $("#metodologia option[value="+ this.model.metodologia.id +"]").attr("selected",true);
        $("#metodologia").trigger("liszt:updated");
    }
    this.renderActivos = function(data){
        collectionActivo = [];
        collectionActivo  = data;
        buscadores.renderTableActivos(data);
    }
    this.renderAmenazas = function(data){
        collectionAmenaza = [];
        collectionAmenaza  = data;
        buscadores.renderTableAmenazas(data);
    }
    this.validateForm = function(){
        var result = false;
        var form = this.form;
        var selectMetodologia = $('#metodologia');

        if(form.valid() && $.trim(selectMetodologia.val()) != ''){
            result = true;
        }
        return result;
    }
    this.loadEvent = function(){
        var _this = this;
        $( "#newProject" ).click(function() {
            if(_this.edit){
                _this.update();
            }else{
                _this.add();
            }
        });

        $( "#metodologia" ).change(this.loadAmenazasByMetodologia);
        //Reglas de validación
        this.validationRules();

    }
    this.loadPanelValoracionActivos = function(){


    }
    this.loadElementsForm = function(){
        var url = "/riesgos/project/read";
        var objeto = {
            id_proyecto : this.id
        }
        if(this.edit){
            constantes.peticionGet(url, objeto, this.getProyecto);
        }
    }
    this.validationRules = function(){
        this.addRule();
        var form = this.form;
        form.validate({
            rules: {
                nombre: "required",
                metodologia : { valueNotEquals: "default" },
                descripcion : "required",
                alcance : "required",
                objetivo : "required"
            },
            messages: {
                nombre: {
                    required: 'Campo obligatorio'
                },
                metodologia : {
                    valueNotEquals: "Seleccione una metodología" },
                descripcion : {
                    required: 'Campo obligatorio'
                },
                alcance : {
                    required: 'Campo obligatorio'
                },
                objetivo : {
                    required: 'Campo obligatorio'
                }
            }
        });
    }
    this.addRule = function(){
        $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg != value;
        }, "Value must not equal arg.");
    }
    this.convertToArray = function(collection){
        var array = [];

        $.each(collection, function( i, object ) {
            array.push(object.id);
        });

        return array;
    }
    this.addSuccess = function(data){
        if(data.response){
            window.location.href = '/riesgos/project';
        }
    }
    this.updateSuccess = function(data){
        if(data.response){
            constantes.growl.success(constantes.codigos.EDICION, "Proyecto");
        }
    }
    this.getSalvaguardasSuccess = function(data){
        console.log("Salvaguardas satisfactorio");
        proyecto.storeSalvaguardas = data;
        //Se deber guiar
        proyecto.renderValoracionSalvaguardasTree();
    }
    this.saveAssessmentsSuccess = function(data){
        console.log("Satisfactorio");
    }
    this.saveAssessmentsAmenazasSuccess = function(data){
        console.log("Satisfactorio");
    }
    this.saveAssessmentsActivosAmenazasSuccess = function(){
        console.log("Satisfactorio");
    }
    this.saveAssessmentsSalvaguardasSuccess = function(){
        console.log("Satisfactorio");
    }

    this.saveAssessmentsAmenazasSalvaguardasSuccess = function(){
        console.log("Satisfactorio");
    }

    this.loadAmenazasSuccess = function(data){
        if(data.length > 0){
            proyecto.renderAmenazas(data);
        }
    }
    this.RemoveElementSuccess = function(data){
        if(data.response){
            proyecto.render();
        }
    }
    this.resetFormView = function () {
        var form = $('#formProject');
        form.each (function(){
            this.reset();
        });
        collectionActivo = [];
        collectionAmenaza = [];
        proyecto.renderActivos([]);
        proyecto.renderAmenazas([]);
    }
    this.filterElementsNews = function(){
        var filter = {
            activos : [],
            amenazas : []
        }
        filter.activos = this.filterElements(collectionActivo);
        filter.amenazas = this.filterElements(collectionAmenaza);

        return filter;

    }
    this.filterElements = function (collection) {
        var resultFilter = [];
        $.each( collection, function( key, object ) {
            if(object['new']){
                resultFilter.push(object);
            }
        });

        return resultFilter;
    }
    this.loadMatriz = function(){
        var _this = this;
        var data = [];
        var activos = this.model['activos'];
        var requerimientos = this.model['metodologia']['requerimientoSeguridad'];
        var ids_requerimientos = [];
        var ids_activos = [];
        var settingsColumns = [];

        var columnas = [];

        columnas.push("");
        settingsColumns.push({
            data : "",
            readOnly : true
        });

        $.each( requerimientos, function( index, object ) {
            ids_requerimientos.push(object.id);
            columnas.push(object.nombre);
            settingsColumns.push({
                data: object.nombre,
                type: "numeric"

            });
        });


        $.each( activos, function( index, activo ) {
            var filas = [];
            filas.push(activo.nombre);
            ids_activos.push(activo.id);
            $.each( requerimientos, function( index, columna ) {
                if(_this.edit){
                    filas.push(_this.valoracionesActivos[activo.id+"-"+columna.id]);
                }else{
                    filas.push(0);
                }
            });
            data.push(filas);
        });
        this.dataValoracionActivos.activos = ids_activos;
        this.dataValoracionActivos.requerimientos = ids_requerimientos;

        return {
            data: data,
            columnas : columnas,
            settingsColumns: settingsColumns

        };

    }

    this.loadMatrizAmenazas = function(){
        var _this = this;
        var data = [];
        var amenazas = this.model['amenazas'];
        var requerimientos = this.model['metodologia']['requerimientoSeguridad'];
        var ids_requerimientos = [];
        var ids_amenazas = [];
        var settingsColumns = [];

        var columnas = [];

        columnas.push("");
        columnas.push("Tipo");
        columnas.push("Nivel Frecuencia");
        settingsColumns.push({
            data : "",
            readOnly : true
        });

        $.each( amenazas, function( index, amenaza ) {
            var filas = [];

            filas.push(amenaza.nombre);
            filas.push(amenaza.tipo);
            ids_amenazas.push(amenaza.id);
            $.each( columnas, function( index, columna ) {
                if($.trim(columna) != ""){
                    if(_this.edit){
                        filas.push(_this.valoracionesAmenazas[amenaza.id]);
                    }else{
                        filas.push("");
                    }

                }
            });
            data.push(filas);
        });


        this.dataValoracionAmenazas.amenazas = ids_amenazas;
        this.dataValoracionAmenazas.requerimientos = ids_requerimientos;

        return {
            data: data,
            columnas : columnas,
            settingsColumns: settingsColumns

        };

    }


    this.renderMatriz = function(){
        var container = document.getElementById('matriz_valoracion_activos');

        var objectoRender = this.loadMatriz();

        var hot = new Handsontable(container, {
            data: objectoRender['data'],
            height: 250,
            colHeaders: true,
            rowHeaders: true,
            stretchH: 'all',
            columns: [
                {type: 'text'},
                {type: 'numeric'},
                {type: 'numeric'},
                {type: 'numeric'},
                {type: 'numeric'},
                {type: 'numeric'},
            ],
            columnSorting: false,
            colHeaders : objectoRender['columnas'],
            contextMenu: false
          });

        this.objectHandsontable['valoracionActivos'] = hot;
    }
    this.renderMatrizAmenazas = function(){
        var container = document.getElementById('matriz_valoracion_amenazas');

        var objectoRender = this.loadMatrizAmenazas();

        var hot = new Handsontable(container, {
            data: objectoRender['data'],
            height: 250,
            colHeaders: true,
            rowHeaders: true,
            stretchH: 'all',
            columns: [
                {type: 'text'},
                {type: 'text'},
                {type: 'text'},
            ],
            columnSorting: false,
            colHeaders : objectoRender['columnas'],
            contextMenu: false
          });

        this.objectHandsontable['valoracionAmenazas'] = hot;
    }
    this.renderActivoAmenazaTree = function(){
        var requerimientos = this.model['metodologia']['requerimientoSeguridad'];
        var ids_requerimientos = [];
        var settingsColumns = [];

        var columnas = [];
        //<a href='#'><span class='_add_amenaza'>+</span></a>
        columnas.push({
                        id:"nombre",
                        editor:"text",
                        header:"",
                        adjust : true,
						//template: "{common.treetable()} #nombre# {common.add_amenaza()}",
                        template: function(obj, common){
                                var _template = common.treetable(obj, common)+" "+obj.nombre+" ";
                                if(obj.$parent == 0){
                                    _template+= "<a href='#'><span class='_add_amenaza'>+</span></a>";
                                }
                                return _template;
                        }
        },
        {
                        id:"frecuencia",
                        editor:"text",
                        header:"Frecuencia",
                        adjust : "header"
        }
        );

        $.each( requerimientos, function( index, object ) {
            ids_requerimientos.push(object.id);
            var objeto = { id: object.nombre, editor:"text", header:object.nombre,	adjust : "header"};
            columnas.push(objeto);
        });
        var grid = webix.ui({
				container:"add_activo_amenaza",
                //width:auto,
				view:"treetable",
				columns:columnas,
                editable:true,
				editaction:"custom",
				navigation:true,
				select:"cell",
				autoheight:true,
				autowidth:true,
				data:proyecto.model.activos,
                onClick:{
                    _add_amenaza:function(e, id){
                        var childrens = [];
                        this.data.eachChild(id,function(obj){
                            childrens.push(obj);
                        });

                        collectionAmenazasActivo = childrens;
                        buscadores.renderTableAmenazasActivo();
                        proyecto.activeSelected = this.getItem(id);
                        $('#addAmenazas').modal('show');
                        //webix.message(this.getItem(id).nombre);
                        return false;
                    }
                }
			});
            webix.UIManager.addHotKey("enter", function(view){
				var pos = view.getSelectedId();
                var item = grid.getSelectedItem();

                if(pos.column != "nombre" && !grid.isBranch(item.id)){
                    view.edit(pos);
                }else{
                    grid.editStop();
                }

			}, grid);



        this.objectHandsontable['valoracionAmenazasActivo'] = grid;
        this.dataValoracionActivosAmenazas['requerimientos'] = ids_requerimientos;
    }
    this.renderAmenazaSalvaguardaTree = function(){
        var settingsColumns = [];

        var columnas = [];
        //<a href='#'><span class='_add_amenaza'>+</span></a>
        columnas.push({
                        id:"nombre",
                        editor:"text",
                        header:"",
                        adjust : true,
						//template: "{common.treetable()} #nombre# {common.add_amenaza()}",
                        template: function(obj, common){
                                var _template = common.treetable(obj, common)+" "+obj.nombre+" ";
                                if(obj.$parent == 0){
                                    _template+= "<a href='#'><span class='_add_salvaguarda'>+</span></a>";
                                }
                                return _template;
                        }
        }
        );

        var grid = webix.ui({
				container:"add_amenaza_salvaguarda",
                //width:auto,
				view:"treetable",
				columns:columnas,
                editable:true,
				editaction:"custom",
				navigation:true,
				select:"cell",
				autoheight:true,
				autowidth:true,
				data:proyecto.model.amenazas,
                onClick:{
                    _add_salvaguarda:function(e, id){
                        var childrens = [];
                        this.data.eachChild(id,function(obj){
                            childrens.push(obj);
                        });

                        collectionSalvaguardasAmenaza = childrens;
                        buscadores.renderTableSalvaguardasAmenaza();
                        proyecto.amenazaSelected = this.getItem(id);
                        $('#addSalvaguardas').modal('show');
                        //webix.message(this.getItem(id).nombre);
                        return false;
                    }
                }
			});
            webix.UIManager.addHotKey("enter", function(view){
				var pos = view.getSelectedId();
                var item = grid.getSelectedItem();

                if(pos.column != "nombre" && !grid.isBranch(item.id)){
                    view.edit(pos);
                }else{
                    grid.editStop();
                }

			}, grid);



        this.objectHandsontable['valoracionSalvaguardasAmenaza'] = grid;
    }
    this.renderValoracionSalvaguardasTree = function(){
        var settingsColumns = [];

        var columnas = [];
        //<a href='#'><span class='_add_amenaza'>+</span></a>
        columnas.push(
            {
                        id:"tipo",
                        header:"Tipo",
                        adjust : true,
                        template:function(obj, common){
                            if (obj.$group) return common.treetable(obj, common) + obj.value;
                            return "";
                        }
						//template: "{common.treetable()} #nombre# {common.add_amenaza()}",
                        /*template: function(obj, common){
                                var _template = common.treetable(obj, common)+" "+obj.nombre+" ";
                                if(obj.$parent == 0){
                                    _template+= "<a href='#'><span class='_add_amenaza'>+</span></a>";
                                }
                                return _template;
                        }*/
        }
            ,{
                        id:"nombre",
                        header:"Salvaguarda",
                        adjust : true
						//template: "{common.treetable()} #nombre# {common.add_amenaza()}",
                        /*template: function(obj, common){
                                var _template = common.treetable(obj, common)+" "+obj.nombre+" ";
                                if(obj.$parent == 0){
                                    _template+= "<a href='#'><span class='_add_amenaza'>+</span></a>";
                                }
                                return _template;
                        }*/
        },
        {
                        id:"valoracion",
                        editor:"text",
                        header:"Valoración",
                        adjust : "header",
                        editor: "select",
                        options: ['','n.a.', 'L0','L1','L2','L3','L4','L5']

        }
        );
        if(proyecto.edit){
            $.each(proyecto.storeSalvaguardas, function(idx, item){
                var valoracion = proyecto.valoracionesSalvaguardas[item.id] != undefined ? proyecto.valoracionesSalvaguardas[item.id] : '';
                item["valoracion"] = valoracion;
            });
        }

        var grid = webix.ui({
				container:"tableTree_valoracion_salvaguardas",
                //width:auto,
				view:"treetable",
				columns:columnas,
                editable:true,
				editaction:"click",
				navigation:true,
				select:"cell",
				autoheight:true,
				autowidth:true,
                ready:function(){

                    this.group({
                        by:"tipo",
                        row:"tipo"
                    });
                },
				data: proyecto.storeSalvaguardas
                /*onClick:{
                    _add_amenaza:function(e, id){
                        var childrens = [];
                        this.data.eachChild(id,function(obj){
                            childrens.push(obj);
                        });

                        collectionAmenazasActivo = childrens;
                        buscadores.renderTableAmenazasActivo();
                        proyecto.activeSelected = this.getItem(id);
                        $('#addAmenazas').modal('show');
                        //webix.message(this.getItem(id).nombre);
                        return false;
                    }
                }*/
			});
            /*webix.UIManager.addHotKey("enter", function(view){
				var pos = view.getSelectedId();
                var item = grid.getSelectedItem();

                if(pos.column != "tipo" && pos.column != "nombre" && !grid.isBranch(item.id)){
                    view.edit(pos);
                }else{
                    grid.editStop();
                }

			}, grid);*/



        this.objectHandsontable['valoracionSalvaguardas'] = grid;
    }
    this.getValoracionesActivos = function(){
        var endRow = this.dataValoracionActivos.activos.length - 1;
        var endColumn = this.dataValoracionActivos.requerimientos.length;
        var dataValoraciones = this.objectHandsontable['valoracionActivos'].getData(0,1,endRow,endColumn);

        return dataValoraciones;
    }
    this.getValoracionesAmenazas = function(){
        var endRow = this.dataValoracionAmenazas.amenazas.length - 1;
        var dataValoraciones = this.objectHandsontable['valoracionAmenazas'].getData(0,2,endRow,2);

        return dataValoraciones;
    }
    this.selectionOfThreats = function(){
        //var activo = this.objectHandsontable['valoracionAmenazasActivo'].getSelectedItem();
        var grid = this.objectHandsontable['valoracionAmenazasActivo'];
        var activo = proyecto.activeSelected;

        $.each(collectionAmenazasActivo, function( index, object ) {
            if(object.$parent != undefined){
                if(!grid.data.exists(object.id) && object.$parent == activo.id){
                    object.id = activo.id+'-'+object.id;
                    grid.data.add(object, -1, activo.id);
                }
            }else{
                object.id = activo.id+'-'+object.id;
                grid.data.add(object, -1, activo.id);
            }
        });

        grid.refresh();


        console.log('Activo',activo);
        console.log('Amenazas',collectionAmenazasActivo);
        $('#addAmenazas').modal('hide');
    }
    this.selectionOfSafeguards = function(){
        //var activo = this.objectHandsontable['valoracionSalvaguardasAmenaza'].getSelectedItem();
        var grid = this.objectHandsontable['valoracionSalvaguardasAmenaza'];
        var amenaza = proyecto.amenazaSelected;

        $.each(collectionSalvaguardasAmenaza, function( index, object ) {
            if(object.$parent != undefined){
                if(!grid.data.exists(object.id) && object.$parent == activo.id){
                    object.id = amenaza.id+'-'+object.id;
                    grid.data.add(object, -1, amenaza.id);
                }
            }else{
                object.id = amenaza.id+'-'+object.id;
                grid.data.add(object, -1, amenaza.id);
            }
        });

        grid.refresh();


        console.log('Amenaza',amenaza);
        console.log('salvaguardas',collectionSalvaguardasAmenaza);
        $('#addSalvaguardas').modal('hide');
    }
    this.renderValoracionesActivoVsAmenaza = function(list){
        var grid = this.objectHandsontable['valoracionAmenazasActivo'];


        $.each(list, function( index, object ) {
            var idActivo= object.idActivo;
            object.id = idActivo+'-'+object.idAmenaza;
            grid.data.add(object, -1, idActivo);
        });

        grid.refresh();

    }
    this.renderValoracionesAmenazaVsSalvaguarda = function(list){
        var grid = this.objectHandsontable['valoracionSalvaguardasAmenaza'];

        $.each(list, function( index, object ) {
            var idAmenaza = object.idAmenaza;
            object.id = idAmenaza+'-'+object.idSalvaguarda;
            grid.data.add(object, -1, idAmenaza);
        });

        grid.refresh();
    }
    this.renderPanelTabs = function(){

        if(this.model.activos.length > 0 && this.model.amenazas.length > 0){
            $(".valoracionActivos").show();
            $(".valoracionAmenazas").show();
        }
        if(!$.isEmptyObject(this.valoracionesActivos) && !$.isEmptyObject(this.valoracionesAmenazas)){
            $(".valoracionSalvaguardas").show();
        }

        if(!$.isEmptyObject(this.valoracionesSalvaguardas)){
            $(".valoracionActivoAmenaza").show();
        }

        if(this.valoracionesActivosAmenazas.length > 0){
            $(".valoracionAmenazaSalvaguarda").show();
        }
    }
}