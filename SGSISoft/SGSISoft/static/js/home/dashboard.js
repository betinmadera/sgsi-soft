/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var dashboardjs = (function ($, console) {
    "use strict"
    var _privado;
    _privado = {
        listarSuccess: function (data) {
            if(data.length>0){
                for (var i = 0; i < data.length; i++) {
                    var description_json = jQuery.parseJSON(data[i].description);
                    $("#notificationsContainer").prepend('<li class="divider"></li><li class="notification"><a href="'+constantes.host+description_json.url+'"><i class="icon-flag"></i>'+data[i].verb+'</a></li>');
                }
                var notificationsLength = $("#notificationsContainer .notification").length;
                $("#notificationsCounter").text(notificationsLength);
            }else{
                $("#notificationsContainer").empty();
                $("#notificationsContainer").prepend('<li id="notificationsEmpty"><i class="icon-flag"></i> No hay notificaciones nuevas</li>');
                $("#notificationsCounter").text(0);
            }
        },
        inicializarGUI: function () {
            _aplicacion.readNotifications();
        }
    };
    var _aplicacion = {
        inicializar: function () {
            _privado.inicializarGUI();

            //Sección para manejar eventos realtime
           /* socket.on('message', function(message) {
                var message_json = jQuery.parseJSON(message);
                if ("mark_as_read" in message_json) {
                    $("#notificationsContainer").empty();
                    $("#notificationsContainer").prepend('<li id="notificationsEmpty"><i class="icon-flag"></i> No hay notificaciones nuevas</li>');
                } else {
                    if($("#notificationsEmpty").length > 0){
                        $("#notificationsEmpty").remove();
                    }

                    $("#notificationsContainer").prepend('<li class="divider"></li><li class="notification"><a href="#"><i class="icon-flag"></i>'+message_json.verb+'</a></li>');
                }
                var notificationsLength = $("#notificationsContainer .notification").length;
                $("#notificationsCounter").text(notificationsLength);

            });
            socket.on('error', function (data) {
                console.log(data || 'error');
            });

            socket.on('connect_failed', function (data) {
                console.log(data || 'connect_failed');
            });*/
        },
        readNotifications: function () {
            var url = "/notifications"
            var objeto = {
                idActividad: 1
            }
            constantes.peticionGet(url, {}, _privado.listarSuccess);
        }
    };
    return _aplicacion;
}(jQuery, console));