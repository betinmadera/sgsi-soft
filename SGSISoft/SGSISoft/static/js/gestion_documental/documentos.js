/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var documentos = (function ($, console) {
    "use strict"
    var _privado = {
        listarSuccess: function (data) {
            var id;
            console.log(data)
            $('#table_document').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 20,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'titulo', "fnRender": function (oObj) {
                        return oObj.aData.titulo
                    }},
                    { mDataProp: 'tipo', "sClass": "center" },
                    { mDataProp: 'version', "sClass": "center"},
                    { mDataProp: 'estado', "sClass": "center", "fnRender": function (data) {

                        return '<span class= "label ' + data.aData.estiloEstado + '">' + data.aData.estado + '</span>'
                    }},
                    { mDataProp: 'inicio', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.inicio);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'fin', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.fin);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'actualizacion', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.actualizacion);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'id', "sClass": "center", "fnRender": function (data) {
                        id = data.aData.id;

                        return '<a href="/documental/activity/' + data.aData.actividadID + '"><span class= "color-icons note_co "></span></a>' 
                        + '<a href="/documental/document/' + data.aData.id + '"><span class= "color-icons bookmark_document_co "></span></a>'
                        //+ '<a href="/documental/observation/document/' + data.aData.id + '"><span class= "color-icons comment_co "></span></a>'
                        + '<a href="../media/' + data.aData.url + '"><span class= "color-icons drive_disk_co "></span></a>'
                        + '<a href="/documental/document/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co"></span></a>' 
                        +'<a href="#" onclick="documentos.removalQuestion(' + data.aData.id + ', \'' + data.aData.titulo + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                        ;
                    }},
                    { mDataProp: 'acciones', "fnRender": function (data) {
                        var salida = '<div class="btn-group pull-right"><button data-toggle="dropdown" class="btn dropdown-toggle"><i  class="icon-cog "></i><span class="caret"></span></button><ul class="dropdown-menu">'

                        $.each(data.aData.acciones, function (i, item) {
                            salida += '<li><a class="btn-modal" data-toggle="modal" href="" ';
                            if (item.valor == 2 || item.valor == 3) {
                                salida += 'onclick="documentos.formObservation(' + id + ',' + item.valor + ')"';
                            } else {
                                salida += 'onclick="documentos.changeDocumentStatus(' + id + ',' + item.valor + ')"';
                            }
                            salida += '><i class="' + item.icon + '"></i> ' + item.nombre + '</a></li>';
                        });

                        salida += '</ul></div>'
                        return salida;
                        //'<a href="/documental/activity/user/' + data.aData.id + '"><span class= "color-icons note_co"></span></a>' + '<a href="/roles/user/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co"></span></a>';

                    }}

                ]
            });
            constantes._modal('hide');
        },
        listarDashboardSuccess: function (data) {
            var id;
            $("#total-documents").text(data.length);
            $('.documents-tbl').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 4,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'titulo', "fnRender": function (data) {
                        data.aData["tituloOriginal"] = data.aData.titulo;
                        return "<a href='#'>"+data.aData.titulo+"</a>";
                    }},
                    { mDataProp: 'estado', "sClass": "center", "fnRender": function (data) {

                        return '<span class= "label ' + data.aData.estiloEstado + '">' + data.aData.estado + '</span>'
                    }},
                    { mDataProp: 'actualizacion', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.actualizacion);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'id', "sClass": "", "fnRender": function (data) {
                        var template = "<div class='btn-group pull-right'> \
                                        <button data-toggle='dropdown' class='btn dropdown-toggle'><i \
                                                class='icon-cog'></i><span class='caret'></span></button><ul class='dropdown-menu'> \
                                            <li><a href='/documental/document/"+data.aData.id+"'><i class='icon-file'></i> Detalle</a></li> \
                                            <!--li><a class='editActividad' id_actividad="+data.aData.id+" href='/documental/document/edit/"+data.aData.id+"'><i class='icon-edit'></i> Editar</a></li> \
                                            <li><a href='#' onclick="+"documentos.removalQuestion(' "+ data.aData.id + "', \''"+data.aData.tituloOriginal + "'\')"+"><i class='icon-remove'></i> Eliminar</a></li-->" +
                                        "</ul></div>";

                        return template;
                    }}
                ]
            });
            constantes._modal('hide');
        },
        eliminarDocumentoSuccess: function (data) {
            constantes.growl.success(_aplicacion.codigos.ELIMINACION, "Documento");
            _aplicacion.read();
        },
        crearObservacionSuccess: function (data) {
            _aplicacion.read()
            console.log(data)
            $('#modal_observacion').modal('hide');
            $('#_documento').val("")
            $('#textoObservacion').val("")
            constantes.growl.success(_aplicacion.codigos.CREACION, "La observación ");
        },
        cambiarEstadoDocumentoSuccess: function (data) {
            _aplicacion.read()
            constantes.growl.success(_aplicacion.codigos.ACTUALIZACION, "Documento");
        },
        failAjax: function (jqxhr, textStatus, error) {
            $('#modal_observacion').modal('hide');

            constantes._message(jqxhr.status)
            //TODO: Crear funcion en constantes.
        },
        inicializarGUI: function () {
            _aplicacion.eventSubmitFormObservation()
            _aplicacion.eventConfirmationDeleteDocument()
            _aplicacion.read()
        }
    };
    var _aplicacion = {
        idDocumento: 0,
        tipo: "",
        inicializar: function () {
            _privado.inicializarGUI();
        },
        read: function () {
            var url = "/documental/document/all"
            var tipo = _aplicacion.tipo
            var objeto = {type: tipo}
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarSuccess);
        },
        readDashboard: function () {
            var url = "/documental/document/all"
            var tipo = "admin"
            var objeto = {type: tipo}
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarDashboardSuccess);
        },
        createObservation: function () {
            var url = "/documental/observation/create";
            var idDocumento = $("#_documento").val()
            var idEstadoDocumento = $("#_estadoDocumento").val()
            var observacion = $("#textoObservacion").val()
            var objeto = {
                observacion: observacion,
                idDocumento: idDocumento,
                idEstadoDocumento: idEstadoDocumento
            }
            constantes.peticionGet(url, objeto, _privado.crearObservacionSuccess);
        },
        delete: function () {
            var url = "/documental/document/delete";
            var idDocumento = _aplicacion.idDocumento
            var objeto = {
                idDocumento: idDocumento
            }
            constantes.peticionGet(url, objeto, _privado.eliminarDocumentoSuccess);

        },
        formObservation: function (idDocumento, idEstadoDocumento) {
            $("#_documento").val(idDocumento);
            $("#_estadoDocumento").val(idEstadoDocumento);
            $('#textoObservacion').val("")
            $('#modal_observacion').modal('show');
        },
        changeDocumentStatus: function (idDocumento, idEstadoDocumento) {
            var url = "/documental/document/change/status";
            var idDocumento = idDocumento;
            var idEstadoDocumento = idEstadoDocumento;
            var objeto = {
                idDocumento: idDocumento,
                idEstadoDocumento: idEstadoDocumento
            }
            constantes.peticionGet(url, objeto, _privado.cambiarEstadoDocumentoSuccess);
        },
        eventSubmitFormObservation: function () {
            $("#bFormObservation").click(function () {
                var form = $("#form_observacion");
                form.validate({
                    rules: {
                        textoObservacion: "required"
                    },
                    messages: {
                        textoObservacion: {
                            required: ''
                        }
                    }
                });
                if (form.valid()) {
                    documentos.createObservation()
                }
            });
        },
        eventConfirmationDeleteDocument: function () {
            $("#bDeleteDocument").click(function () {
                documentos.delete()
            });
        },
        removalQuestion: function (idDocumento, nombreDocumento) {
            _aplicacion.idDocumento = idDocumento;
            $("#nombre_documento").html(nombreDocumento);
            $('#modal_confirmacion').modal("show");
        }
    };
    return _aplicacion;
}(jQuery, console));