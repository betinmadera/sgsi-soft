/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var actividades = (function ($, console) {
    "use strict"
    var _privado, dialog, dialogDocument;
    _privado = {
        listarSuccess: function (data) {
            var id;
            $('#table_activity').dataTable({
                "bDestroy": true,
                "aaData": data,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "iDisplayLength": 20,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'titulo'},
                    { mDataProp: 'tipo', "sClass": "center"},
                    { mDataProp: 'inicio', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.inicio);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'fin', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.fin);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'estado', "sClass": "center" },
                    { mDataProp: 'descripcion' },
                    { mDataProp: 'id', "sClass": "center", "fnRender": function (data) {
                        return '<a href="/documental/activity/' + data.aData.id + '" ><span class= "color-icons application_detail_co text-tip" data-original-title="Detalles"></span></a>' +
                            '<a class="editActividad" id_actividad="' + data.aData.id + '" href="/documental/activity/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co"></span></a>' +
                            '<a href="#" onclick="actividades.removalQuestion(' + data.aData.id + ', \'' + data.aData.titulo + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                        ;
                    }}
                ]
            });
            $(".editActividad").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idActividad = $(this).attr("id_actividad");
                constantes.peticionAjax(url, {}, _privado.cargaFormEditActividad, "GET", "html");
            });
            $(".detailsUser").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idResponsable = $(this).attr("id_user");
                permisos.idResponsable = $(this).attr("id_user");
                permisos.tipoResponsable = "user"
                constantes.peticionAjax(url, {}, _privado.cargaFormDetails, "GET", "html");
            });
            constantes._modal('hide');
        },
        listarDashboardSuccess: function (data) {
            var id;
            $('.activities-tbl').dataTable({
                "bDestroy": true,
                "aaData": data,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "iDisplayLength": 2,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'titulo',"sClass":'',"fnRender": function(data){
                        var template =  "<ul><li>Titulo: <a>"+data.aData.titulo+"</a></li>"+
                                        "<li class='post-by'><i class='icon-small-black user_sc'></i> Encargados: <div>";
                           data.aData.responsables.forEach(function(entry) {
                                template += "<dd><span class='color-icons user_co'></span>"+entry.nombre+" "+entry.apellido+"</dd></li>"
                           });
                        template+= "<div><li class='post-date'><i class='icon-small-black alarm_clock_sc'></i> Fecha:<a href='#'>"+data.aData.inicio+"</a></li></ul>";

                        data.aData["tituloOriginal"] = data.aData.titulo;

                        return template;

                    }},
                    { mDataProp: 'estado', "sClass": "center", "fnRender": function(data){

                        var template = "<span class='label "+data.aData.estiloEstado+"'>"+data.aData.estado+"</span>";

                        return template;


                    }},
                    { mDataProp: 'id', "sClass": "", "fnRender": function (data) {
                        var template = "<div class='btn-group pull-right'> \
                                        <button data-toggle='dropdown' class='btn dropdown-toggle'><i \
                                                class='icon-cog'></i><span class='caret'></span></button><ul class='dropdown-menu'> \
                                            <li><a href='/documental/activity/"+data.aData.id+"'><i class='icon-file'></i> Detalle</a></li> \
                                            <!--li><a class='editActividad' id_actividad="+data.aData.id+" href='/documental/activity/edit/"+data.aData.id+"'><i class='icon-edit'></i> Editar</a></li> \
                                            <li><a href='#' onclick="+"actividades.removalQuestion(' "+ data.aData.id + "', \''"+data.aData.tituloOriginal + "'\')"+"><i class='icon-remove'></i> Eliminar</a></li-->" +
                                        "</ul></div>";

                        return template;
                    }}
                ]
            });
            $(".editActividad").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idActividad = $(this).attr("id_actividad");
                constantes.peticionAjax(url, {}, _privado.cargaFormEditActividad, "GET", "html");
            });
            $(".detailsUser").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idResponsable = $(this).attr("id_user");
                permisos.idResponsable = $(this).attr("id_user");
                permisos.tipoResponsable = "user"
                constantes.peticionAjax(url, {}, _privado.cargaFormDetails, "GET", "html");
            });
            constantes._modal('hide');
        },
        listarDocumentsSuccess: function (data) {
            $('#table_document').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 20,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'titulo', "fnRender": function (oObj) {
                        return oObj.aData.titulo
                    }},
                    { mDataProp: 'tipo', "sClass": "center" },
                    { mDataProp: 'version', "sClass": "center"},
                    { mDataProp: 'estado', "sClass": "center", "fnRender": function (data) {

                        return '<span class= "label ' + data.aData.estiloEstado + '">' + data.aData.estado + '</span>'
                    }},
                    { mDataProp: 'inicio', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.inicio);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'fin', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.fin);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'actualizacion', "sClass": "center", "fnRender": function (data) {
                        var fecha = new Date(data.aData.actualizacion);
                        fecha.setDate(fecha.getDate() + 1)
                        return  dateFormat(fecha, "d-mmm-yy");
                    }},
                    { mDataProp: 'id', "sClass": "center", "fnRender": function (data) {
                        //id = data.aData.id;
                        return '<a href="/documental/activity/' + data.aData.actividadID + '"><span class= "color-icons note_co "></span></a>'
                        + '<a href="/documental/document/' + data.aData.id + '"><span class= "color-icons bookmark_document_co "></span></a>'
                        //+ '<a href="/documental/observation/document/' + data.aData.id + '"><span class= "color-icons comment_co "></span></a>' 
                        + '<a href="../../media/'+ data.aData.url + '"><span class= "color-icons drive_disk_co "></span></a>'
                        + '<a href="/documental/document/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co"></span></a>' 
                        + '<a href="#" onclick="documentos.removalQuestion(' + data.aData.id + ', \'' + data.aData.titulo + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }},
                    { mDataProp: 'acciones', "fnRender": function (data) {
                        var salida = '<div class="btn-group pull-right"><button data-toggle="dropdown" class="btn dropdown-toggle"><i  class="icon-cog "></i><span class="caret"></span></button><ul class="dropdown-menu">'

                        $.each(data.aData.acciones, function (i, item) {
                            salida += '<li><a class="btn-modal" data-toggle="modal" href="" ';
                            if (item.valor == 2 || item.valor == 3) {
                                salida += 'onclick="documentos.formObservation(' + item.id + ',' + item.valor + ')"';
                            } else {
                                salida += 'onclick="documentos.changeDocumentStatus(' + item.id + ',' + item.valor + ')"';
                            }
                            salida += '><i class="' + item.icon + '"></i> ' + item.nombre + '</a></li>';
                        });

                        salida += '</ul></div>'
                        return salida;
                        //'<a href="/documental/activity/user/' + data.aData.id + '"><span class= "color-icons note_co"></span></a>' + '<a href="/roles/user/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co"></span></a>';

                    }}

                ]
            });
            constantes._modal('hide');
        },
        eliminarActividadSuccess: function (data) {
            constantes.growl.success(_aplicacion.codigos.ELIMINACION, "Actividad");
            $("#dialog-confirm").dialog('close');
            _aplicacion.read();
        },
        inicializarGUI: function () {
            _aplicacion.read();
            dialog = $("#window_actividad").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false
            });
            $("#dialog-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: 170,
                modal: true,
                buttons: [
                    {
                        text: "Aceptar",
                        class: 'btn',
                        click: function () {
                            //_aplicacion.delete()
                        }
                    },
                    {
                        text: "Cancelar",
                        class: 'btn',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            $("#bDeleteActivity").click(function () {
                _aplicacion.delete()
            });
            $("#newActividad").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                constantes.peticionAjax(url, {}, _privado.cargaFormActividad, "GET", "html");
            });
        },
        inicializarGUIDetalles: function () {
            _aplicacion.readDocumentsByActivity();

            dialogDocument = $("#window_actividad_documento").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false
            });
            $("#dialog-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: 170,
                modal: true,
                buttons: [
                    {
                        text: "Aceptar",
                        class: 'btn',
                        click: function () {
                            //_aplicacion.delete()
                        }
                    },
                    {
                        text: "Cancelar",
                        class: 'btn',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            $("#newDocumentButton").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                constantes.peticionAjax(url, {}, _privado.cargaFormDocument, "GET", "html");
            });
        },
        cargaFormActividad: function (data) {
            dialog.dialog("option", "title", "Crear Actividad");
            dialog.dialog("option", "width", 600);
            dialog.dialog("option", "height", 600);
            $("#window_actividad #content").html(data);

            $("#id_inicio").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-90:c",
                dateFormat: "dd/mm/yy"
            });
            $("#id_fin").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-90:c",
                dateFormat: "dd/mm/yy"
            });
            $(".chzn-select").chosen();

            $("#saveForm").on("click", function (event) {
                event.preventDefault();
                _aplicacion.add();
            });
            $("#cancelForm").on("click", function (event) {
                event.preventDefault();
                dialog.dialog("close")
            });
            dialog.find("form").on("submit", function (event) {
                event.preventDefault();
                _aplicacion.add();
            });
            dialog.dialog("open");
        },
        cargaFormEditActividad: function (data) {
            dialog.dialog("option", "title", "Editar Actividad");
            dialog.dialog("option", "width", 600);
            dialog.dialog("option", "height", 600);
            $("#window_actividad #content").html(data);

            $("#id_inicio").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-90:c",
                dateFormat: "dd/mm/yy"
            });
            $("#id_fin").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-90:c",
                dateFormat: "dd/mm/yy"
            });
            $(".chzn-select").chosen();

            $("#saveForm").on("click", function (event) {
                event.preventDefault();
                _aplicacion.update();
            });
            $("#cancelForm").on("click", function (event) {
                event.preventDefault();
                dialog.dialog("close")
            });
            dialog.find("form").on("submit", function (event) {
                event.preventDefault();
                _aplicacion.update();
            });
            dialog.dialog("open");
        },
        cargaFormDocument: function (data) {
            dialogDocument.dialog("option", "title", "Crear Documento");
            dialogDocument.dialog("option", "width", 600);
            dialogDocument.dialog("option", "height", 600);
            $("#window_actividad_documento #content").html(data);


            $('#fileupload').fileupload({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: '../document/upload',
                uploadTemplateId: null,
                downloadTemplateId: null,
                //maxFileSize: 5000000,
                maxNumberOfFiles: 1,
                //acceptFileTypes: /^image\/(gif|jpe?g|png)$|^application\/(pdf|msword|vnd.(ms-excel|ms-powerpoint|openxmlformats-officedocument.+|oasis.opendocument.+))$/i,
                uploadTemplate: function (o) {
                    var rows = $();
                    $.each(o.files, function (index, file) {
                        var row = $('<tr class="template-upload fade">' +
                            '<td><span class="preview"></span>' +
                            '<img src="' + constantes.fileTypeImage(file.type) + '"/></td>' +
                            '<td><p class="name"></p>' +
                            '<div class="error"></div>' +
                            '</td>' +
                            '<td><p class="size"></p>' +
                            '</td>' +
                            '<td>' +
                            (!index && !o.options.autoUpload ?
                                '<button class="start" disabled style="display:none">Start</button>' : '') +
                            (!index ? '<button class="btn btn-warning cancel"><i class="glyphicon glyphicon-trash"></i><span>Eliminar</span></button>' : '') +
                            '</td>' +
                            '</tr>');
                        row.find('.name').text(file.name);
                        row.find('.size').text(o.formatFileSize(file.size));
                        if (file.error) {
                            row.find('.error').text(file.error);
                        }
                        rows = rows.add(row);
                    });
                    return rows;
                }
            }).bind('fileuploadadd', function (e, data) {
                $("table tbody.files").empty();
            })
            .bind('fileuploadstart', function (e) {
                $("#uploaded-files").prop('disabled', true);
                $("#buttonFileUpload").prop('disabled', true);
                console.log('Uploads started');
            })
            .bind('fileuploaddone', function (e, data) {
                $("#uploaded-files").prop('disabled', false);
                $("#buttonFileUpload").prop('disabled', false);
                //Aquí debe activarse el modal para la petición ajax
                dialogDocument.dialog("close");
                _aplicacion.readDocumentsByActivity();
            })
            .bind('fileuploadalways', function (e, data) {
                $("#uploaded-files").prop('disabled', false);
                $("#buttonFileUpload").prop('disabled', false);
            });
            $('#fileupload').fileupload(
                'option',
                'redirect'
                //window.location.href.replace(
                //    /\/[^\/]*$/,
                //    '/cors/result.html?%s'
                //)
            );


            $(".chzn-select").chosen();

            $("#saveForm").on("click", function (event) {
                event.preventDefault();
                _aplicacion.add();
            });
            $("#cancelForm").on("click", function (event) {
                event.preventDefault();
                dialog.dialog("close")
            });
            dialogDocument.find("form").on("submit", function (event) {
                event.preventDefault();
                _aplicacion.add();
            });
            dialogDocument.dialog("open");
        },
        cargaFormEditDocument: function (data) {
            dialogDocument.dialog("option", "title", "Editar Documento");
            dialogDocument.dialog("option", "width", 600);
            dialogDocument.dialog("option", "height", 400);
            $("#window_actividad_documento #content").html(data);

            $("#id_inicio").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-90:c",
                dateFormat: "dd/mm/yy"
            });
            $("#id_fin").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-90:c",
                dateFormat: "dd/mm/yy"
            });
            $(".chzn-select").chosen();

            $("#saveForm").on("click", function (event) {
                event.preventDefault();
                if ($('#fileupload').valid()) {
                }
            });
            $("#cancelForm").on("click", function (event) {
                event.preventDefault();
                dialog.dialog("close")
            });

            dialogDocument.find("form").on("submit", function (event) {
                event.preventDefault();
                _aplicacion.update();
            });


            // Initialize the jQuery File Upload widget:

            // Enable iframe cross-domain access via redirect option:
            dialogDocument.dialog("open");
        }
    };
    var _aplicacion = {
        idActividad: 0,
        tipo: "",
        inicializar: function () {
            _privado.inicializarGUI();
        },
        inicializarActividadDetalle: function () {
            _privado.inicializarGUIDetalles()

            // modify jquery ajax to add csrtoken when doing "local" requests
            $('html').ajaxSend(function (event, xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    var csrftoken = constantes.getCookie("csrftoken");
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                }
            });
        },
        read: function () {
            var url = "/documental/activity/all"
            var tipo = _aplicacion.tipo
            var objeto = {type: tipo}
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarSuccess);
        },
        readDashboard: function () {
            var url = "/documental/activity/all"
            var tipo = "admin"
            var objeto = {type: tipo}
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarDashboardSuccess);
        },
        readDocumentsByActivity: function () {
            var url = "/documental/activity/documents/" + _aplicacion.idActividad;
            var tipo = _aplicacion.tipo
            var objeto = {type: tipo}
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarDocumentsSuccess);
        },
        add: function () {
            var data1 = $("#formActividad").serialize();
            $.ajax({
                url: "/documental/activity/new_ajax",
                type: "POST",
                data: data1,
                success: function (data) {
                    _aplicacion.read();
                    constantes.growl.success(constantes.codigos.CREACION, "Actividad");
                    dialog.dialog("close");
                },
                beforeSend: function (xhr) {
                    var csrftoken = constantes.getCookie("csrftoken");
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                },
                error: function (jqxhr, textStatus, error) {
                    try {
                        var errores = $.parseJSON(jqxhr.responseText).errors;
                        $(".id_error_form").remove();
                        $.each(errores, function (key, value) {
                            $("#id_" + key).parent().append("<div class='id_error_form'>" + value + "</div>")
                        });
                    } catch (e) {
                        constantes.growl.error(jqxhr.status);
                    }
                }
            });
        },
        update: function () {
            var data1 = $("#formActividad").serialize();
            $.ajax({
                url: "/documental/activity/edit_ajax/" + _aplicacion.idActividad,
                type: "POST",
                data: data1,
                success: function (data) {
                    _aplicacion.read();
                    constantes.growl.success(constantes.codigos.EDICION, "Actividad");
                    dialog.dialog("close");
                },
                beforeSend: function (xhr) {
                    var csrftoken = constantes.getCookie("csrftoken");
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                },
                error: function (jqxhr, textStatus, error) {
                    try {
                        var errores = $.parseJSON(jqxhr.responseText).errors;
                        $(".id_error_form").remove();
                        $.each(errores, function (key, value) {
                            $("#id_" + key).parent().append("<div class='id_error_form'>" + value + "</div>")
                        });
                    } catch (e) {
                        constantes.growl.error(jqxhr.status);
                    }
                }
            });
        },
        delete: function () {
            var url = "/documental/activity/delete";
            var idActividad = _aplicacion.idActividad
            var objeto = {
                idActividad: idActividad
            }
            constantes.peticionGet(url, objeto, _privado.eliminarActividadSuccess);
        },
        removalQuestion: function (idActividad, nombreActividad) {
            _aplicacion.idActividad = idActividad;
            $("#nombre_actividad").html(nombreActividad);
            $('#modal_confirmacion').modal("show");
        }
    };
    return _aplicacion;
}(jQuery, console));