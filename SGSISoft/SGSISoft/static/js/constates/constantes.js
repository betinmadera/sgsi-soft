/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */
 
var constantes = (function ($, console) {
    "use strict"

    var _staticURL = "/static/";
    var _privado = {
        ERROR_SERVIDOR: "Ha ocurrido un error",

        NO_ENCONTRADO: "El recurso solicitado no se encuentra",
        ERROR_DESCONOCIDO: "Ha ocurrido un error",
        ERROR_YA_EXISTE: "El elemento ya existe",
        ERROR_NO_AUTORIZADO: "No tiene permiso para ejecutar esta accion",
        CREACION: "se ha creado exitosamente",
        EDICION: "se ha modificado exitosamente",
        ELIMINACION: "se ha eliminado exitosamente",
        ACTUALIZADO: "se ha actualizado exitosamente",

        failAjax: function (jqxhr, textStatus, error) {
            _aplicacion.growl.error(jqxhr.status);
        },
        completedAjax: function (jqXHR, textStatus) {
            _aplicacion._modal('hide');
        },
        beforeSendAjax: function (jqXHR, settings) {
            _aplicacion._modal();
        }
    };

    var _fileTypeImages = {
        "application_msword": _staticURL + "img/file-type/word.png",
        "application_vnd_openxmlformats_officedocument_wordprocessingml_document": _staticURL + "img/file-type/word.png",
        "application_vnd_oasis_opendocument_text": _staticURL + "img/file-type/word.png",
        "application_pdf": _staticURL + "img/file-type/pdf.png",
        "application_vnd_oasis_opendocument_spreadsheet": _staticURL + "img/file-type/excel.png",
        "application_vnd_ms_excel": _staticURL + "img/file-type/excel.png",
        "application_vnd_openxmlformats_officedocument_spreadsheetml_sheet": _staticURL + "img/file-type/excel.png",
        "application_vnd_oasis_opendocument_presentation": _staticURL + "img/file-type/power-point.png",
        "application_vnd_ms_powerpoint": _staticURL + "img/file-type/power-point.png",
        "application_vnd_openxmlformats_officedocument_presentationml_slideshow": _staticURL + "img/file-type/power-point.png",
        "application_vnd_openxmlformats_officedocument_presentationml_presentation": _staticURL + "img/file-type/power-point.png"
    };

    var _aplicacion = {
        host: "http://localhost:8000/",
        hostNode: "http://localhost:8004/",

        codigos: {
            NINGUNO: 0,
            ERROR_SERVIDOR: 500,
            ERROR_YA_EXISTE: 409,
            NO_ENCONTRADO: 404,
            NO_AUTORIZADO: 403,
            CREACION: 1,
            EDICION: 2,
            ELIMINACION: 3,
            ACTUALIZACION: 4
        },
        roles: {
            USUARIO: 1,
            LECTOR: 2,
            ESCRITOR:3,
            ADMINISTRADOR: 4
        },

        _message: function (estado, texto) { //TODO: quitar
            switch (estado) {
                case 500:
                    $('#mensaje').html(_privado.ERROR_SERVIDOR)
                    break;
                case 404:
                    $('#mensaje').html(_privado.NO_ENCONTRADO)
                    break;
                case 200:
                    $('#mensaje').html(texto + _privado.CREACION)
                    break;
                case 300:
                    $('#mensaje').html(texto + _privado.EDICION)
                    break;
                case 400:
                    $('#mensaje').html(texto + _privado.ELIMINACION)//TODO: este codigo es badRequest
                    break;
                case 100:
                    $('#mensaje').html(texto + _privado.ACTUALIZADO)
                    break;
                default:
            }
            $('#modal_mensaje').modal("show");
        },
        peticionGet: function (url, objeto, funcionSuccess) {
            //TODO: Pasar el div para actualizar el objeto.
            $.ajax({
                dataType: "json",
                url: url,
                data: objeto,
                success: funcionSuccess,
                beforeSend: _privado.beforeSendAjax,
                error: _privado.failAjax,
                complete: _privado.completedAjax
            })
        },
        peticionAjax: function (url, objeto, functionSuccess, method, dataType) {
            if (method == null || method == undefined) {
                method = 'POST';
            }

            if (dataType == null || dataType == undefined) {
                dataType = 'json';
            }

            $.ajax({
                url: url,
                data: objeto,
                type: method,
                dataType: dataType,
                success: functionSuccess,
                error: _privado.failAjax,
                beforeSend: function (xhr) {
                    _aplicacion._modal();
                    if (method == 'POST') {
                        var csrftoken = _aplicacion.getCookie("csrftoken");
                        xhr.setRequestHeader('X-CSRFToken', csrftoken);
                    }
                },
                complete: function (jqXHR, textStatus) {
                    _aplicacion._modal('hide');
                }
            });
        },
        growl: {
            info: function (message) {
                $.growl({ title: "Informaci&oacute;n", message: message });
            },
            error: function (status) {
                var salida = "";

                switch (status) {
                    case _aplicacion.codigos.ERROR_SERVIDOR:
                        salida = _privado.ERROR_SERVIDOR;
                        break;
                    case _aplicacion.codigos.NO_ENCONTRADO:
                        salida = _privado.NO_ENCONTRADO;
                        break;
                    case _aplicacion.codigos.ERROR_YA_EXISTE:
                        salida = _privado.ERROR_YA_EXISTE;
                        break;
                    case _aplicacion.codigos.NO_AUTORIZADO:
                        salida = _privado.ERROR_NO_AUTORIZADO;
                        break;
                    default:
                        salida = _privado.ERROR_DESCONOCIDO;
                }
                $.growl.error({ message: salida });
            },
            success: function (estado, message) {
                var salida = "";
                switch (estado) {
                    case _aplicacion.codigos.CREACION:
                        salida = message + " " + _privado.CREACION;
                        break;
                    case _aplicacion.codigos.EDICION:
                        salida = message + " " + _privado.EDICION;
                        break;
                    case _aplicacion.codigos.ELIMINACION:
                        salida = message + " " + _privado.ELIMINACION;
                        break;
                    case _aplicacion.codigos.ACTUALIZACION:
                        salida = message + " " + _privado.ACTUALIZADO;
                        break;
                    default:
                        salida = message
                }
                $.growl.notice({ title: "Completado", message: salida });
            },
            warning: function (message) {
                $.growl.warning({ title: "Advertencia", message: message });
            }
        },
        isInRole: function (roleId, listPermission) {
            var exist = false;
            for (var i = 0; i < listPermission.length; i++) {
                if (listPermission[i] >= roleId) {
                    exist = true;
                    break;
                }
            }
            return exist;
        },
        _modal: function (state) {
            var display = 'none';

            if (state == 'show' || state == undefined) {
                display = 'block';
            }
            $('#loading_modal').css('display', display);
        },
        progress: function(objeto, value){
            if(value==true){
                var objectAdd= $('<div class="sgsi-modal"></div>');
                objeto.prepend(objectAdd)
                //Se modifica la posición del objeto
                var position = objeto.position();
                objectAdd.css(position)
                //Se modifica el tamaño del objeto
                objectAdd.height(objeto.height());
                objectAdd.width(objeto.width());
                //Se agrega el objeto

            }else{
                objeto.find(".sgsi-modal").remove()
            }
        },
        _notificationModal: function (state) {
            var display = 'none';

            if (state == 'show' || state == undefined) {
                display = 'block';
            }
            $('#loading_modal').css('display', display);
        },
        fileTypeImage: function (type) {
            var typeClean = type.replace("/", "_");
            var typeClean = typeClean.replace(/\./gi, "_");
            var typeClean = typeClean.replace(/-/gi, "_");
            try {
                if (eval("_fileTypeImages." + typeClean) != undefined) {
                    return eval("_fileTypeImages." + typeClean);
                }
            }
            catch (err) {
                console.log(err);
            }
            return "";
        },
        getCookie: function (name) {
            var cookieValue = null;

            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }

            return cookieValue;
        },
        inicializarArbol: function (div) {
            var id = "";
            if (div != null) {
                id = "#" + div + " ";
            }
            $(id + '.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
            $(id + '.tree li.parent_li > span').on('click', _aplicacion.manejarArbol);
        },
        manejarArbol: function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(":visible")) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
            }
            e.stopPropagation();
        },
        STATIC_URL:_staticURL
    };

    return _aplicacion;
}(jQuery, console));