/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var collectionActivo = [];
var collectionAmenaza = [];
var collectionAmenazasActivo = [];
var collectionSalvaguardasAmenaza = [];

var buscadores = function ($, console) {
    "use strict"
    var _privado;
    _privado = {
        bloodActivos: function(){
            var activosBlood = new Bloodhound({
            datumTokenizer: function (datum) {
                return Bloodhound.tokenizers.whitespace(datum.nombre);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: urlSearchActivos + '?query=%QUERY'
            }
            });
          return activosBlood;
        },
        bloodAmenazas: function(){
            var amenazasBlood = new Bloodhound({
            datumTokenizer: function (datum) {
                return Bloodhound.tokenizers.whitespace(datum.nombre);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: urlSearchAmenazas + '?query=%QUERY'
            }
            });
          return amenazasBlood;
        },
        bloodSalvaguardas: function(id){
            var proyecto_id = id || 0;
            var salvaguardasBlood = new Bloodhound({
            datumTokenizer: function (datum) {
                return Bloodhound.tokenizers.whitespace(datum.nombre);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: urlSearchSalvaguardas + '?proyecto'+proyecto_id+'&query=%QUERY'
            }
            });
          return salvaguardasBlood;
        },
        inicializarGUI: function () {
            console.log('Inicializar')
        },
        search:function(options){
        // Initialize the Bloodhound suggestion engine
            options.blood.initialize();

        // Instantiate the Typeahead UI
        $(options.id).typeahead({
            minLength: 1,
            highlight: true
        }, {
            displayKey: options.displayKey,
            source: options.blood.ttAdapter()
        }).on(
					'typeahead:selected',
                    options.onSelect
        );

        }
    };
    var _aplicacion;
    _aplicacion = {
        urlGeneral: 0,
        blood_activos:null,
        blood_amenazas:null,
        blood_salvaguardas:null,
        inicializar_blood: function () {
            _privado.inicializarGUI();
            _aplicacion.blood_activos = _privado.bloodActivos();
            _aplicacion.blood_amenazas = _privado.bloodAmenazas();
            _aplicacion.blood_salvaguardas = _privado.bloodSalvaguardas();
        },
        initialize_search: function (options,onSelect) {
            _privado.search(options,onSelect);
        },
        onSelectActivos: function(obj, datum){
            $('#typeahead_activos').typeahead('val', "");
            if(!_aplicacion.validateExistence(collectionActivo,datum)){
                if(proyecto.edit){
                    datum['new'] = true;
                }
                collectionActivo.push(datum);
                _aplicacion.renderTableActivos();
            }else{
                constantes.growl.warning("Ya se encuentra agregado");
            }
            $('#typeahead_amenazas').typeahead('val', "");
        },
        onSelectAmenazas: function(obj, datum){
            if(!_aplicacion.validateExistence(collectionAmenaza,datum)){
               if(proyecto.edit){
                    datum['new'] = true;
                }
                collectionAmenaza.push(datum);
                _aplicacion.renderTableAmenazas();
            }else{
                constantes.growl.warning("Ya se encuentra agregado");
            }
            $('#typeahead_amenazas').typeahead('val', "");
        },
        onSelectAddAmenazasActivo: function(obj, datum){
            //Collection para agregar
            if(!_aplicacion.validateExistence(collectionAmenazasActivo,datum)){
               if(proyecto.edit){
                    datum['new'] = true;
                }
                collectionAmenazasActivo.push(datum);
                _aplicacion.renderTableAmenazasActivo();
            }else{
                constantes.growl.warning("Ya se encuentra agregado");
            }
            $('#typeahead_add_amenazas_activo').typeahead('val', "");
        },
        onSelectAddSalvaguardasAmenaza: function(obj, datum){
            //Collection para agregar
            if(!_aplicacion.validateExistence(collectionSalvaguardasAmenaza,datum)){
               if(proyecto.edit){
                    datum['new'] = true;
                }
                collectionSalvaguardasAmenaza.push(datum);
                _aplicacion.renderTableSalvaguardasAmenaza();
            }else{
                constantes.growl.warning("Ya se encuentra agregado");
            }
            $('#typeahead_add_salvaguardas_amenaza').typeahead('val', "");
        },
        renderTableActivos : function(){
            $('#table_active').dataTable({
                "bDestroy": true,
                "aaData": collectionActivo,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'codigo' },
                    { mDataProp: 'nombre' },
                    { mDataProp: 'descripcion' },
                    { mDataProp: 'tipo', "sClass": "center", "fnRender": function (data) {
                        var cadena = 'No Asignado';
                        if(data.aData.tipo != null){
                            cadena = data.aData.tipo;
                        }
                        return  cadena;
                    }},
                    { mDataProp: 'id', "fnRender": function (data) {
                        var tipo = 'activo';
                        return  '<a href="#" onclick="proyecto.remove(' + data.aData.id + ', \'' + tipo + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }}
                ]
            });

        },
        renderTableAmenazas : function(){
            $('#table_amenaza').dataTable({
                "bDestroy": true,
                "aaData": collectionAmenaza,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre' },
                    { mDataProp: 'descripcion' },
                    { mDataProp: 'tipo' },
                    { mDataProp: 'id', "fnRender": function (data) {
                        var tipo = 'amenaza';
                        return  '<a href="#" onclick="buscadores.removeElementArray(' + data.aData.id + ', \'' + tipo + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }}
                ]
            });

        },
        renderTableAmenazasActivo : function(){
            $('#table_add_amenazas_activo').dataTable({
                "bDestroy": true,
                "aaData": collectionAmenazasActivo,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre' },
                    { mDataProp: 'descripcion' },
                    { mDataProp: 'tipo' },
                    { mDataProp: 'id', "fnRender": function (data) {
                        var tipo = 'amenaza_activo';
                        return  '<a href="#" onclick="buscadores.removeElementArray(' + data.aData.id + ', \'' + tipo + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }}
                ]
            });

        },
        renderTableSalvaguardasAmenaza : function(){
            $('#table_add_salvaguardas_amenaza').dataTable({
                "bDestroy": true,
                "aaData": collectionSalvaguardasAmenaza,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre' },
                    { mDataProp: 'descripcion' },
                    { mDataProp: 'tipo' },
                    { mDataProp: 'id', "fnRender": function (data) {
                        var tipo = 'salvaguarda_amenaza';
                        return  '<a href="#" onclick="buscadores.removeElementArray(' + data.aData.id + ', \'' + tipo + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }}
                ]
            });

        },
        validateExistence : function(array, element){
            var resultValid = false
            $.each(array, function( i, object ) {
                if(object.id == element.id){
                    resultValid = true;
                    return false;
                }
            });

            return resultValid;

        },
        removeElementArray : function(id, typeCollection){
            var result = false;
            var element;
            var index;
            var _collection = _aplicacion.getCollectionByType(typeCollection);

            element = _aplicacion.getObjectById(id,_collection)

            index = _collection.indexOf(element);

            if (index > -1) {
                _collection.splice(index, 1);
                result = true;
            }

            _aplicacion.renderTableByType(typeCollection);

            return result;
        },
        getObjectById: function(id, _collection){
            var _object = null;

            $.each(_collection, function( i, object ) {
                if(object.id == id){
                    _object = object;
                    return false;
                }
            });

            return _object;
        },
        getCollectionByType: function(type){
            var _collection = [];
            if(type == 'activo'){
                _collection = collectionActivo;
            }else if(type == 'amenaza'){
                _collection = collectionAmenaza;
            }else if(type == 'amenaza_activo'){
                _collection = collectionAmenazasActivo;
            }else if(type == 'salvaguarda_amenaza'){
                _collection = collectionSalvaguardasAmenaza;
            }else{
                _collection = [];
            }

            return _collection;
        },
        renderTableByType: function(type){
            if(type == 'activo'){
                _aplicacion.renderTableActivos();
            }else if(type == 'amenaza'){
                _aplicacion.renderTableAmenazas();
            }else if(type == 'amenaza_activo'){
                _aplicacion.renderTableAmenazasActivo();
            }else if(type == 'salvaguarda_amenaza'){
                _aplicacion.renderTableSalvaguardasAmenaza();
            }else{
                _aplicacion.renderTableSalvaguardas();
            }
        }

    };
    return _aplicacion;
}(jQuery, console);
