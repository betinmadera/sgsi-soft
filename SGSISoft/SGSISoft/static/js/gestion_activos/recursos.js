/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var recursos = function ($, console) {
    "use strict"
    var _privado;
    _privado = {
        peticionGet: function (url, objeto, funcionSuccess) {
            $.getJSON(url, objeto)
                .done(funcionSuccess)
                .fail(_privado.failAjax);
        },
        listarSuccess: function (data) {
            console.log(data);
            $('#table_resource').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre' },
                    { mDataProp: 'descripcion' },
                    { mDataProp: 'tipo' },
                    { mDataProp: 'activos', "sClass": "center", "fnRender": function (data) {
                        var cadena = '';
                        $.each(data.aData.activos,function(index,object){
                            cadena+='<br>'+object.nombre;
                        })
                        if(cadena==''){
                            cadena = '-';
                        }
                        return  cadena;
                    }},
                    { mDataProp: 'usuarios', "sClass": "center", "fnRender": function (data) {
                        var cadena = '';
                        $.each(data.aData.usuarios,function(index,object){
                            cadena+=object.nombre+' '+object.apellido + '<br>';
                        })
                        if(cadena==''){
                            cadena = '-';
                        }
                        return  cadena;
                    }},
                    { mDataProp: 'id', "fnRender": function (data) {
                        return '<a href="/activos/resource/' + data.aData.id + '"><span class= "color-icons application_detail_co text-tip" title="Permisos"></span></a>' + '<a href="/activos/resource/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>'+'<a href="#" onclick="recursos.removalQuestion(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }}
                ]
            });
        },
        listarCategoriaSuccess: function (data) {
            $('#table_category').dataTable({
                "bDestroy": true,
                "aaData": data,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre
                    }},
                    { mDataProp: 'categoria', "fnRender": function (data) {
                        var salida = "Ninguna"
                        if (data.aData.categoria != null) {
                            salida = ""
                            salida = data.aData.categoria
                        }
                        return salida;
                    }},
                    { mDataProp: 'id', "fnRender": function (data) {
                        return '<a href="/activos/category/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>'+'<a href="#" onclick="activos.removalQuestionCategory(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }}
                ]
            });
        },
        eliminarRecursoSuccess: function (data) {
            constantes._message(400, "Recurso ");
            _aplicacion.read();
        },
         eliminarCategoriaSuccess: function (data) {
            constantes._message(400, "Categoria ");
            _aplicacion.readCategory();
        },
        failAjax: function (jqxhr, textStatus, error) {
            console.log(jqxhr);
            console.log(textStatus);
            console.log(error);
            constantes._message(jqxhr.status)
        },
        inicializarGUI: function () {
            _aplicacion.read()
            _aplicacion.eventConfirmationDeleteActive()
        }
    };
    var _aplicacion;
    _aplicacion = {
        idResponsable: 0,
        idRecurso: 0,
        idCategoria: 0,
        inicializar: function () {
            _privado.inicializarGUI();
        },
        read: function () {
            var url = "/activos/resource/all"
            var objeto = {}
            _privado.peticionGet(url, objeto, _privado.listarSuccess);
        },
        readCategory: function () {
            var url = "/activos/category/all"
            var objeto = {}
            _privado.peticionGet(url, objeto, _privado.listarCategoriaSuccess);
        },
        delete: function () {
            var url = "/activos/resource/delete";
            var idRecurso = _aplicacion.idRecurso
            var objeto = {
                idRecurso: idRecurso
            }
            _privado.peticionGet(url, objeto, _privado.eliminarRecursoSuccess);

        },
        deleteCategory: function () {
            var url = "/activos/category/delete";
            var idCategoria = _aplicacion.idCategoria
            var objeto = {
                idCategoria: idCategoria
            }
            _privado.peticionGet(url, objeto, _privado.eliminarCategoriaSuccess);

        },
        eventConfirmationDeleteActive: function () {
            $("#bDeleteActive").click(function () {
                recursos.delete()
            });
        },
        removalQuestion: function (idRecurso, nombreRecurso) {
            _aplicacion.idRecurso = idRecurso;
            $("#nombre_activo").html(nombreRecurso);
            $('#modal_confirmacion').modal("show");
        },
        eventConfirmationDeleteCategory: function () {
            $("#bDeleteCategory").click(function () {
                recursos.deleteCategory()
            });
        },
        removalQuestionCategory: function (idCategoria, nombreCategoria) {
            _aplicacion.idCategoria = idCategoria;
            $("#nombre_categoria").html(nombreCategoria);
            $('#modal_confirmacion').modal("show");
        }
    };
    return _aplicacion;
}(jQuery, console);