/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var activos = function ($, console) {
    "use strict"
    var _privado;
    _privado = {
        listarSuccess: function (data) {
            $('#table_active').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'codigo' },
                    { mDataProp: 'nombre' },
                    { mDataProp: 'descripcion' },
                    { mDataProp: 'cantidad' },
                    { mDataProp: 'tipo', "sClass": "center", "fnRender": function (data) {
                        var cadena = 'No Asignado';
                        if(data.aData.tipo != null){
                            cadena = data.aData.tipo;
                        }
                        return  cadena;
                    }},
                    { mDataProp: 'dependencias', "sClass": "center", "fnRender": function (data) {
                        var cadena = '';
                        $.each(data.aData.dependencias,function(index,object){
                            cadena+='<br>'+object.nombre;
                        })
                        if(cadena==''){
                            cadena = '-';
                        }
                        return  cadena;
                    }},
                    { mDataProp: 'responsables', "sClass": "center", "fnRender": function (data) {
                        var cadena = '';
                        $.each(data.aData.responsables,function(index,object){
                            cadena+=object.nombre+' '+object.apellido + '<br>';
                        })
                        if(cadena==''){
                            cadena = '-';
                        }
                        return  cadena;
                    }},
                    { mDataProp: 'id', "fnRender": function (data) {
                        return '<a href="/activos/active/' + data.aData.id + '"><span class= "color-icons application_detail_co text-tip" title="Permisos"></span></a>' + '<a href="/activos/active/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>'+'<a href="#" onclick="activos.removalQuestion(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }}
                ]
            });
        },
        listarCategoriaSuccess: function (data) {
            $('#table_category').dataTable({
                "bDestroy": true,
                "aaData": data,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre
                    }},
                    { mDataProp: 'categoria', "fnRender": function (data) {
                        var salida = "Ninguna"
                        if (data.aData.categoria != null) {
                            salida = ""
                            salida = data.aData.categoria
                        }
                        return salida;
                    }},
                    { mDataProp: 'id', "fnRender": function (data) {
                        return '<a href="/activos/category/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>'+'<a href="#" onclick="activos.removalQuestionCategory(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                    }}
                ]
            });
        },
        eliminarActivoSuccess: function (data) {
            constantes.growl.success(_aplicacion.codigos.ELIMINACION, "Activo");
            _aplicacion.read();
        },
        eliminarCategoriaSuccess: function (data) {
            constantes.growl.success(_aplicacion.codigos.ELIMINACION, "Categoria");
            _aplicacion.readCategory();
        },
        inicializarGUI: function () {
            _aplicacion.read()
            _aplicacion.eventConfirmationDeleteActive()
        }
    };
    var _aplicacion;
    _aplicacion = {
        idResponsable: 0,
        idActivo: 0,
        idCategoria: 0,
        inicializar: function () {
            _privado.inicializarGUI();
        },
        read: function () {
            var url = "/activos/active/all"
            var objeto = {}
            constantes.peticionGet(url, objeto, _privado.listarSuccess);
        },
        readCategory: function () {
            var url = "/activos/category/all"
            var objeto = {}
            constantes.peticionGet(url, objeto, _privado.listarCategoriaSuccess);
        },
        delete: function () {
            var url = "/activos/active/delete";
            var idActivo = _aplicacion.idActivo
            var objeto = {
                idActivo: idActivo
            }
            constantes.peticionGet(url, objeto, _privado.eliminarActivoSuccess);

        },
        deleteCategory: function () {
            var url = "/activos/category/delete";
            var idCategoria = _aplicacion.idCategoria
            var objeto = {
                idCategoria: idCategoria
            }
            constantes.peticionGet(url, objeto, _privado.eliminarCategoriaSuccess);

        },
        eventConfirmationDeleteActive: function () {
            $("#bDeleteActive").click(function () {
                activos.delete()
            });
        },
        removalQuestion: function (idActivo, nombreActivo) {
            _aplicacion.idActivo = idActivo;
            $("#nombre_activo").html(nombreActivo);
            $('#modal_confirmacion').modal("show");
        },
        eventConfirmationDeleteCategory: function () {
            $("#bDeleteCategory").click(function () {
                activos.deleteCategory()
            });
        },
        removalQuestionCategory: function (idCategoria, nombreCategoria) {
            _aplicacion.idCategoria = idCategoria;
            $("#nombre_categoria").html(nombreCategoria);
            $('#modal_confirmacion').modal("show");
        }
    };
    return _aplicacion;
}(jQuery, console);