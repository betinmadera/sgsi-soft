/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var grupos;

grupos = function ($, console) {
    "use strict"
    var _privado,
        dialog;
    _privado = {
        listarSuccess: function (data) {
            $('#table_group').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 20,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre
                    }},
/*                    { mDataProp: 'miembros', "fnRender": function (data) {
                        var salida = "";
                        $.each(data.aData.miembros, function (i, item) {
                            salida += '<a href="">' + item.nombre + " " + item.apellido + '</a>' + '<br/>'
                        });
                        return salida;
                    }},*/
                    { mDataProp: 'id', "fnRender": function (data) {
                        var s = "";
                        if (constantes.isInRole(constantes.roles.LECTOR, _aplicacion.permisos)) {
                            s += '<a class="detailsGroup" id_group="' + data.aData.id + '"  href="/roles/group/' + data.aData.id + '"><span class= "color-icons application_key_co text-tip" title="Permisos"></span></a>'
                        }
                        if (constantes.isInRole(constantes.roles.ESCRITOR, _aplicacion.permisos)) {
                            s += '<a class="editGroup" id_group="' + data.aData.id + '" href="/roles/group/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>'
                        }
                        if (constantes.isInRole(constantes.roles.ADMINISTRADOR, _aplicacion.permisos)) {
                            s += '<a href="#" onclick="grupos.removalQuestion(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                        }
                        return s;
                    }}
                ]
            });

            $(".editGroup").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idGrupo = $(this).attr("id_group");
                constantes.peticionAjax(url, {}, _privado.cargaFormEditGroup, "GET", "html");

            });
            $(".detailsGroup").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idGrupo = $(this).attr("id_group");
                _aplicacion.idResponsable = $(this).attr("id_group");
                permisos.idResponsable = $(this).attr("id_group");
                permisos.tipoResponsable = "group";
                constantes.peticionAjax(url, {}, _privado.cargaFormDetails, "GET", "html");
            });

            constantes._modal('hide');
        },
        listarUsersSuccess: function (data) {
            $('#table_usersGroup').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 5,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre + " " + oObj.aData.apellido
                    }},
                    { mDataProp: 'email' },
                    { mDataProp: 'id', "fnRender": function (data) {
                        return '<a class="removeUser" style="cursor:pointer" id_user="'+ data.aData.id +'"><span class= "color-icons delete_co text-tip" title="Permisos"></span></a>';
                    }}
                ]
            });
            $("#table_usersGroup .removeUser").on("click", function (e) {
                e.preventDefault();
                var personId = $(this).attr("id_user");
                var url = "/roles/group/users-remove";
                constantes.peticionAjax(url, {idPersona: personId, idGrupo: _aplicacion.idGrupo}, function (data) {
                    constantes.growl.success(constantes.codigos.NINGUNO, "Persona removida con exito");
                    _aplicacion.readNoUsersByGroup();
                    _aplicacion.readUsersByGroup();
                }, "GET");
            });
            constantes._modal('hide');
        },
        listarNoUsersSuccess: function (data) {
            $('#table_usersNoGroup').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 5,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre + " " + oObj.aData.apellido
                    }},
                    { mDataProp: 'email' },
                    { mDataProp: 'id', "fnRender": function (data) {
                        return '<a class="addUser" style="cursor:pointer"  id_user="'+ data.aData.id + '"><span class= "color-icons add_co text-tip" title="Permisos"></span></a>';
                    }}
                ]
            });
            $("#table_usersNoGroup .addUser").on("click", function (e) {
                e.preventDefault();
                var personId = $(this).attr("id_user");
                 var url = "/roles/group/users-add";
                constantes.peticionAjax(url, {idPersona: personId, idGrupo: _aplicacion.idGrupo}, function (data) {
                    constantes.growl.success(constantes.codigos.NINGUNO, "Persona agregada con exito");
                    _aplicacion.readUsersByGroup();
                    _aplicacion.readNoUsersByGroup();
                }, "GET");
            });
            constantes._modal('hide');
        },
        eliminarGrupoSuccess: function (data) {
            constantes.growl.success(constantes.codigos.ELIMINACION, "Grupo");
            $("#dialog-confirm").dialog('close')
            _aplicacion.read();
        },
        inicializarGUI: function () {
            _aplicacion.read();

            dialog = $("#window_grupo").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false
            });

            $("#dialog-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: 170,
                modal: true,
                buttons: [
                    {
                        text: "Aceptar",
                        class: 'btn',
                        click: function () {
                            _aplicacion.delete()
                        }
                    },
                    {
                        text: "Cancelar",
                        class: 'btn',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });

            $("#newGrupo").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                constantes.peticionAjax(url, {}, _privado.cargaFormGroup, "GET", "html");
            });
            if (!constantes.isInRole(constantes.roles.ESCRITOR, _aplicacion.permisos)) {
                $("#newGrupo").hide();
            }
        },
        cargaFormGroup: function (data) {
            dialog.dialog("option", "title", "Crear Grupo");
            dialog.dialog("option", "width", 560);
            dialog.dialog("option", "height", 270);
            $("#window_grupo #content").html(data);

            $("#saveFormGroup").on("click", function (event) {
                event.preventDefault();
                _aplicacion.add();
            });
            $("#cancelFormGroup").on("click", function (event) {
                event.preventDefault();
                dialog.dialog("close")
            });
            dialog.find("form").on("submit", function (event) {
                event.preventDefault();
                _aplicacion.add();
            });
            dialog.dialog("open");
        },
        cargaFormEditGroup: function (data) {
            dialog.dialog("option", "title", "Editar Grupo");
            dialog.dialog("option", "width", 560);
            dialog.dialog("option", "height", 270);
            $("#window_grupo #content").html(data);

            $("#saveFormGroup").on("click", function (event) {
                event.preventDefault();
                _aplicacion.update();
            });
            $("#cancelFormGroup").on("click", function (event) {
                event.preventDefault();
                dialog.dialog("close")
            });
            dialog.find("form").on("submit", function (event) {
                event.preventDefault();
                _aplicacion.update();
            });
            dialog.dialog("open");
        },
        cargaFormDetails: function (data) {
            dialog.dialog("option", "title", "Detalles Grupo");
            dialog.dialog("option", "width", 800);
            dialog.dialog("option", "height", 550);
            $("#window_grupo #content").html(data);
            $("#tabs").tabs();
            _aplicacion.readUsersByGroup();
            _aplicacion.readNoUsersByGroup();
            permisos.inicializar(_aplicacion.permisos);
            if (!constantes.isInRole(constantes.roles.ADMINISTRADOR, _aplicacion.permisos)) {
                $("#window_grupo #permisosAddSection").hide();
            }
            dialog.dialog("open")
        }
    };
    var _aplicacion;
    _aplicacion = {
        idResponsable: 0,
        idGrupo: 0,
        permisos: null,
        inicializar: function (permissions) {
            _aplicacion.permisos = permissions;
            _privado.inicializarGUI();
        },
        read: function () {
            var url = "/roles/group/all";
            var objeto = {};
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarSuccess);
        },
        readNoUsersByGroup: function () {
            var url = "/roles/group/no-users";
            var objeto = {idResponsable: _aplicacion.idResponsable};
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarNoUsersSuccess);
        },
        readUsersByGroup: function () {
            var url = "/roles/group/users";
            var objeto = {idResponsable: _aplicacion.idResponsable};
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarUsersSuccess);
        },
        add: function () {
            var data1 = $("#formGroup").serialize();
            $.ajax({
                url: "/roles/group/new_ajax",
                type: "POST",
                data: data1,
                success: function (data) {
                    _aplicacion.read();
                    constantes.growl.success(constantes.codigos.CREACION, "Grupo");
                    dialog.dialog("close");
                },
                beforeSend: function (xhr) {
                    var csrftoken = constantes.getCookie("csrftoken");
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                },
                error: function (jqxhr, textStatus, error) {
                    try {
                        var errores = $.parseJSON(jqxhr.responseText).errors;
                        $(".id_error_form").remove();
                        $.each(errores, function (key, value) {
                            $("#id_" + key).parent().append("<div class='id_error_form'>" + value + "</div>")
                        });
                    } catch (e) {
                        constantes.growl.error(jqxhr.status);
                    }
                }
            });
        },
        update: function () {
            var data1 = $("#formGroup").serialize();
            $.ajax({
                url: "/roles/group/edit_ajax/" + _aplicacion.idGrupo,
                type: "POST",
                data: data1,
                success: function (data) {
                    _aplicacion.read();
                    constantes.growl.success(constantes.codigos.EDICION, "Grupo");
                    dialog.dialog("close");
                },
                beforeSend: function (xhr) {
                    var csrftoken = constantes.getCookie("csrftoken");
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                },
                error: function (jqxhr, textStatus, error) {
                    try {
                        var errores = $.parseJSON(jqxhr.responseText).errors;
                        $(".id_error_form").remove();
                        $.each(errores, function (key, value) {
                            $("#id_" + key).parent().append("<div class='id_error_form'>" + value + "</div>")
                        });
                    } catch (e) {
                        constantes.growl.error(jqxhr.status);
                    }
                }
            });
        },
        delete: function () {
            var url = "/roles/group/delete";
            var idGrupo = _aplicacion.idGrupo
            var objeto = {
                idGrupo: idGrupo
            }
            constantes.peticionGet(url, objeto, _privado.eliminarGrupoSuccess);
        },
        removalQuestion: function (idGrupo, nombreGrupo) {
            _aplicacion.idGrupo = idGrupo;
            $("#dialog-confirm #nombre_grupo").html(nombreGrupo);
            $('#dialog-confirm').dialog("open");
        }
    };
    return _aplicacion;
}(jQuery, console);