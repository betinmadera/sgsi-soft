/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var usuarios;

usuarios = function ($, console) {
    "use strict"
    var _privado,
        dialog;
    _privado = {
        listarSuccess: function (data) {
            $('#table_user').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre + " " + oObj.aData.apellido
                    }},
                    { mDataProp: 'cargoNombre' },
                    { mDataProp: 'email' },
                    { mDataProp: 'id', "fnRender": function (data) {
                        var s = "";
                        if (constantes.isInRole(constantes.roles.LECTOR, _aplicacion.permisos)) {
                            s += '<a class="detailsUser" id_user="' + data.aData.id + '" href="/roles/user/' + data.aData.id + '"><span class= "color-icons application_key_co text-tip" title="Permisos"></span></a>'
                        }
                        if (constantes.isInRole(constantes.roles.ESCRITOR, _aplicacion.permisos)) {
                            s += '<a class="editUser" id_user="' + data.aData.id + '" href="/roles/user/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>'
                        }
                        if (constantes.isInRole(constantes.roles.ADMINISTRADOR, _aplicacion.permisos)) {
                            s += '<a href="#" onclick="usuarios.removalQuestion(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                        }
                        return s;
                    }}
                ]
            });
            $(".editUser").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idUsuario = $(this).attr("id_user");
                constantes.peticionAjax(url, {}, _privado.cargaFormEditUser, "GET", "html");

            });
            $(".detailsUser").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idResponsable = $(this).attr("id_user");
                permisos.idResponsable = $(this).attr("id_user");
                permisos.tipoResponsable = "user"
                constantes.peticionAjax(url, {}, _privado.cargaFormDetails, "GET", "html");
            });
            constantes._modal('hide');
        },
        listarDashboardSuccess: function (data) {
            $("#users-total").text(data.length);
            $('.user-tbl').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 3,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    {mDataProp:"usuarioNombre","sClass":"center","fnRender":function(oObj){
                        return "<span class='user-thumb'><img src='"+constantes.STATIC_URL+"img/user-thumb.png' width='40' \
                                        height='40' alt='Usuario'></span>";
                    }},
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre + " " + oObj.aData.apellido+"<span class='user-position'>"+oObj.aData.cargoNombre+"</span>"
                    }},
                    { mDataProp: 'email' }
/*                    { mDataProp: 'id', "fnRender": function (data) {
                        debugger;
                        var s = "";
                        if (constantes.isInRole(constantes.roles.LECTOR, _aplicacion.permisos)) {
                            s += '<a class="detailsUser" id_user="' + data.aData.id + '" href="/roles/user/' + data.aData.id + '"><span class= "color-icons application_key_co text-tip" title="Permisos"></span></a>'
                        }
                        if (constantes.isInRole(constantes.roles.ESCRITOR, _aplicacion.permisos)) {
                            s += '<a class="editUser" id_user="' + data.aData.id + '" href="/roles/user/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>'
                        }
                        if (constantes.isInRole(constantes.roles.ADMINISTRADOR, _aplicacion.permisos)) {
                            s += '<a href="#" onclick="usuarios.removalQuestion(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                        }
                        return s;
                    }}*/
                ]
            });
            $(".editUser").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idUsuario = $(this).attr("id_user");
                constantes.peticionAjax(url, {}, _privado.cargaFormEditUser, "GET", "html");

            });
            $(".detailsUser").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                _aplicacion.idResponsable = $(this).attr("id_user");
                permisos.idResponsable = $(this).attr("id_user");
                permisos.tipoResponsable = "user"
                constantes.peticionAjax(url, {}, _privado.cargaFormDetails, "GET", "html");
            });
            constantes._modal('hide');
        },
        listarGroupsSuccess: function (data) {
            $('#table_group').dataTable({
                "bDestroy": true,
                "aaData": data,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 20,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre
                    }}
                    /*                    { mDataProp: 'miembros', "fnRender": function (data) {
                     var salida = "";
                     $.each(data.aData.miembros, function (i, item) {
                     salida += '<a href="">' + item.nombre + " " + item.apellido + '</a>' + '<br/>'
                     });
                     return salida;
                     }},
                     { mDataProp: 'id', "fnRender": function (data) {

                     return '<a class="detailsGroup" id_group="' + data.aData.id + '"  href="/roles/group/' + data.aData.id + '"><span class= "color-icons application_key_co text-tip" title="Permisos"></span></a>' +
                     '<a class="editGroup" id_group="' + data.aData.id + '" href="/roles/group/edit/' + data.aData.id + '"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>' +
                     '<a href="#" onclick="grupos.removalQuestion(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                     }}*/
                ]
            });
            /*
             $(".editGroup").on("click", function (e) {
             e.preventDefault();
             var url = $(this).attr("href");
             _aplicacion.idUsuario = $(this).attr("id_group");
             constantes.peticionAjax(url, {}, _privado.cargaFormEditGroup, "GET", "html");

             });
             $(".detailsGroup").on("click", function (e) {
             e.preventDefault();
             var url = $(this).attr("href");
             _aplicacion.idResponsable = $(this).attr("id_group");
             permisos.idResponsable = $(this).attr("id_group");
             permisos.tipoResponsable = "group";
             constantes.peticionAjax(url, {}, _privado.cargaFormDetails, "GET", "html");
             });*/

            constantes._modal('hide');
        },
        eliminarUsuarioSuccess: function (data) {
            constantes.growl.success(constantes.codigos.ELIMINACION, "Usuario");
            $("#dialog-confirm").dialog('close');
            _aplicacion.read();
        },
        inicializarGUI: function () {
            _aplicacion.read();
            dialog = $("#window_usuario").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false
            });

            $("#dialog-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: 170,
                modal: true,
                buttons: [
                    {
                        text: "Aceptar",
                        class: 'btn',
                        click: function () {
                            _aplicacion.delete()
                        }
                    },
                    {
                        text: "Cancelar",
                        class: 'btn',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });


            $("#bDeleteUser").click(function (e) {
                _aplicacion.delete()
            });

            $("#newUsuario").on("click", function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                constantes.peticionAjax(url, {}, _privado.cargaFormUser, "GET", "html");
            });
            if (!constantes.isInRole(constantes.roles.ESCRITOR, _aplicacion.permisos)) {
                $("#newUsuario").hide();
            }
        },
        cargaFormUser: function (data) {
            dialog.dialog("option", "title", "Crear Usuario");
            dialog.dialog("option", "width", 560);
            dialog.dialog("option", "height", 550);
            $("#window_usuario #content").html(data);

            $("#id_fechaNacimiento").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-90:c",
                dateFormat: "dd/mm/yy"
            });

            $("#saveFormUser").on("click", function (event) {
                event.preventDefault();
                _aplicacion.add();
            });
            $("#cancelFormUser").on("click", function (event) {
                event.preventDefault();
                dialog.dialog("close")
            });
            dialog.find("form").on("submit", function (event) {
                event.preventDefault();
                _aplicacion.add();
            });
            dialog.dialog("open");
        },
        cargaFormEditUser: function (data) {
            dialog.dialog("option", "title", "Editar Usuario");
            dialog.dialog("option", "width", 560);
            dialog.dialog("option", "height", 550);
            $("#window_usuario #content").html(data);

            $("#id_fechaNacimiento").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-90:c",
                dateFormat: "dd/mm/yy"
            });

            $("#saveFormUser").on("click", function (event) {
                event.preventDefault();
                _aplicacion.update();
            });
            $("#cancelFormUser").on("click", function (event) {
                event.preventDefault();
                dialog.dialog("close")
            });

            dialog.find("form").on("submit", function (event) {
                event.preventDefault();
                _aplicacion.update();
            });
            dialog.dialog("open");
        },
        cargaFormDetails: function (data) {
            dialog.dialog("option", "title", "Detalles Usuario");
            dialog.dialog("option", "width", 800);
            dialog.dialog("option", "height", 600);
            $("#window_usuario #content").empty()
            $("#window_usuario #content").html(data);
            $("#tabs").tabs();

            _aplicacion.readGroupsByUser();
            permisos.inicializar(_aplicacion.permisos);
            if (!constantes.isInRole(constantes.roles.ADMINISTRADOR, _aplicacion.permisos)) {
                $("#window_usuario #permisosAddSection").hide();
            }
            dialog.dialog("open")
        }
    };
    var _aplicacion;
    _aplicacion = {
        idResponsable: 0,
        idUsuario: 0,
        permisos: null,
        inicializar: function (permissions) {
            _aplicacion.permisos = permissions;
            _privado.inicializarGUI();
        },
        read: function () {
            var url = "/roles/user/all";
            var objeto = {};
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarSuccess);
        },
        readDashboard: function () {
            var url = "/roles/user/all";
            var objeto = {};
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarDashboardSuccess);
        },
        readGroupsByUser: function () {
            var url = "/roles/user/groups"
            var objeto = {idResponsable: _aplicacion.idResponsable}
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarGroupsSuccess);
        },
        add: function () {
            var data1 = $("#formUser").serialize();
            $.ajax({
                url: "/roles/user/new_ajax",
                type: "POST",
                data: data1,
                success: function (data) {
                    _aplicacion.read();
                    constantes.growl.success(constantes.codigos.CREACION, "Usuario");
                    constantes.progress($("#window_usuario"), false);
                    dialog.dialog("close");
                },
                beforeSend: function (xhr) {
                    constantes.progress($("#window_usuario"), true);
                    var csrftoken = constantes.getCookie("csrftoken");
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                },
                error: function (jqxhr, textStatus, error) {
                    try {
                        var errores = $.parseJSON(jqxhr.responseText).errors;
                        $(".id_error_form").remove();
                        $.each(errores, function (key, value) {
                            $("#id_" + key).parent().append("<div class='id_error_form'>" + value + "</div>")
                        });
                    } catch (e) {
                        constantes.growl.error(jqxhr.status);
                    }
                    constantes.progress($("#window_usuario"), false);
                }
            });
        },
        update: function () {
            var data1 = $("#formUser").serialize();
            $.ajax({
                url: "/roles/user/edit_ajax/" + _aplicacion.idUsuario,
                type: "POST",
                data: data1,
                success: function (data) {
                    _aplicacion.read();
                    constantes.growl.success(constantes.codigos.EDICION, "Grupo");
                    constantes.progress($("#window_usuario"), false);
                    dialog.dialog("close");
                },
                beforeSend: function (xhr) {
                    constantes.progress($("#window_usuario"), true);
                    var csrftoken = constantes.getCookie("csrftoken");
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
                },
                error: function (jqxhr, textStatus, error) {
                    try {
                        var errores = $.parseJSON(jqxhr.responseText).errors;
                        $(".id_error_form").remove();
                        $.each(errores, function (key, value) {
                            $("#id_" + key).parent().append("<div class='id_error_form'>" + value + "</div>")
                        });
                    } catch (e) {
                        constantes.growl.error(jqxhr.status);
                    }
                    constantes.progress($("#window_usuario"), false);
                }
            });
        },
        delete: function () {
            var url = "/roles/user/delete";
            var idUsuario = _aplicacion.idUsuario
            var objeto = {
                idUsuario: idUsuario
            }
            constantes.peticionGet(url, objeto, _privado.eliminarUsuarioSuccess);
        },
        removalQuestion: function (idUsuario, nombreUsuario) {
            _aplicacion.idUsuario = idUsuario;
            $("#dialog-confirm #nombre_usuario").html(nombreUsuario);
            $('#dialog-confirm').dialog("open");
        }
    };
    return _aplicacion;
}(jQuery, console);