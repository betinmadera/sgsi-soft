/**
 * Autor: Andres Betin, Jhonny Madera y Raul Martelo
 * Copyright: Copyright 2016, Universidad de Cartagena
 * Credits: Andres Betin, Jhonny Madera, Raul Martelo,
 *          Grupo de investigacion GIMATICA
 *          Universidad de Cartagena
 * License: GPL
 * Version: 1.0.0
 * Status: Production
 */

var permisos;
permisos = (function ($, console) {
    "use strict"
    var _privado = {
        agregarPermisoSuccess: function (data) {
            _aplicacion.read()
        },
        eliminarPermisoSuccess: function (data) {
            constantes.growl.success(constantes.codigos.ELIMINACION, "Permiso");
            $('#modal_confirmacion').dialog("close");
            _aplicacion.read();
        },
        editarPermisoSuccess: function (data) {
            $('#modal_edicion').dialog("close");
            _aplicacion.read();
            constantes.growl.success(constantes.codigos.EDICION, "Permiso");
        },
        listarSuccess: function (data) {
            $('#table_permissions').dataTable({
                "bDestroy": true,
                "aaData": data,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 5,
                "oLanguage": jQuery.labels,
                "sDom": '<"table_top clearfix"f<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>',
                "aoColumns": [
                    { mDataProp: 'nombre', "fnRender": function (oObj) {
                        return oObj.aData.nombre
                    }},
                    { mDataProp: 'rol'},
                    { mDataProp: 'id', "fnRender": function (data) {
                        var s = "";
                        if (constantes.isInRole(constantes.roles.ADMINISTRADOR, _aplicacion.permisos)) {
                            s += '<a href="#" onclick="permisos.formUpdatePermission(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons page_white_edit_co text-tip" title="Editar"></span></a>' +
                            '<a href="#" onclick="permisos.removalQuestion(' + data.aData.id + ', \'' + data.aData.nombre + '\')"><span class= "color-icons  page_white_delete_co text-tip" title="Eliminar"></span></a>';
                        }
                        return s;
                    }}
                ]
            });
            constantes._modal('hide');
        }
    };
    var _aplicacion;
    _aplicacion = {
        idPermiso: 0,
        tipoResponsable: "",
        idResponsable: 0,
        permisos: null,
        inicializar: function (permissions) {
            _aplicacion.permisos = permissions;
            _aplicacion.read();
            constantes.inicializarArbol();
            
            $("#modal_edicion").dialog({
                autoOpen: false,
                resizable: false,
                height: 250,
                width: 400,
                modal: true,
                draggable: false
            });
            
            $("#modal_confirmacion").dialog({
                autoOpen: false,
                resizable: false,
                height: 200,
                modal: true,
                buttons: [
                    {
                        text: "Aceptar",
                        class: 'btn btn-success',
                        click: function () {
                            _aplicacion.delete();
                        }
                    },
                    {
                        text: "Cancelar",
                        class: 'btn btn-warning',
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            
            $("#bUpdatePermission").click(function (e) {
                e.preventDefault();
                var form = $("#form_update_permission");
                if (form.valid()) {
                    permisos.update()
                }
                return false;
            });
            $("#cancelUpdatePermission").click(function (e) {
                e.preventDefault();
                $("#modal_edicion").dialog("close");
            });
        },
        add: function () {
            var url = "/roles/permission/create"
            var permiso = $('input[name=permisos]:checked').attr('value')
            var rol = $("#roles").val()
            if (permiso == null || rol == null) {
                //Está nulo
            } else {
                var objeto = {
                    idPermiso: permiso,
                    tipo: _aplicacion.tipoResponsable,
                    rol: rol,
                    idResponsable: _aplicacion.idResponsable
                }
                constantes.peticionGet(url, objeto, _privado.agregarPermisoSuccess);
            }
        },
        read: function () {
            var url = "/roles/permission/all"
            var tipo = _aplicacion.tipoResponsable
            var idResponsable = _aplicacion.idResponsable
            var objeto = {
                tipo: tipo,
                idResponsable: idResponsable
            }
            constantes._modal();
            constantes.peticionGet(url, objeto, _privado.listarSuccess);
        },
        delete: function () {
            var url = "/roles/permission/delete";
            var idPermiso = _aplicacion.idPermiso
            var objeto = {
                idPermiso: idPermiso,
                tipo: _aplicacion.tipoResponsable
            }
            constantes.peticionGet(url, objeto, _privado.eliminarPermisoSuccess);

        },
        update: function () {
            var url = "/roles/permission/update";
            var idPermiso = _aplicacion.idPermiso
            var tipo = _aplicacion.tipoResponsable
            var idRol = $("#rolesEditar").val()

            var objeto = {
                idPermiso: idPermiso,
                tipo: _aplicacion.tipoResponsable,
                idRol: idRol
            }
            constantes.peticionGet(url, objeto, _privado.editarPermisoSuccess);

        }, removalQuestion: function (idPermiso, nombrePermiso) {
            _aplicacion.idPermiso = idPermiso;
            $("#modal_confirmacion #nombre_permiso").html(nombrePermiso);
            $('#modal_confirmacion').dialog("open");
        },
        formUpdatePermission: function (idPermiso, nombrePermiso) {
            _aplicacion.idPermiso = idPermiso;
            $('#nombrePermisoEditar').html(nombrePermiso)
            $('#modal_edicion').dialog("open");
        }
    };
    return _aplicacion;
}(jQuery, console));