# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.contrib.auth.models import User
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.dispatch import receiver
from email_confirm_la.signals import post_email_confirm
from django.core.urlresolvers import reverse

#region Enumeraciones
from util.Util import EnumElemento


class Rol:
    USUARIO=EnumElemento("Usuario", 1, peso=1)
    LECTOR=EnumElemento("Lector", 2, peso=2)
    ESCRITOR=EnumElemento("Escritor", 3, peso=3)
    ADMINISTRADOR=EnumElemento("Administrador", 4, peso=4)

    @staticmethod
    def listaEnumerada():
        return [(value.getValor(), value.getNombre()) for key, value in Rol.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def listaElementos():
        return [value for key, value in Rol.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def get(id):
        valord= [value for key, value in Rol.__dict__.iteritems() if isinstance(value, EnumElemento) and value.getValor()==id][0]
        return valord
#endregion

#region Borrables
class Permiso(MPTTModel):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    parent = TreeForeignKey("self", blank=True, null=True, related_name='children', db_index=True)
    funcion = models.CharField(max_length=150, blank=True, null=True)
    modulo = models.CharField(max_length=150)
    path = models.CharField(default=funcion, max_length=200, auto_created=True, unique=True)
    urlPath = models.CharField(max_length=150, blank=True, null=True)
    tipoPermiso = models.CharField(max_length=150)
    imagen = models.TextField()

# region Auditoria
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
#endregion

    def __unicode__(self):
        return self.nombre

    class MPTTMeta:
        order_insertion_by = ['nombre']

class Departamento(models.Model):
    nombre = models.CharField(max_length=50)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class Cargo(models.Model):
    nombre = models.CharField(max_length=50)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre
# endregion

# region NoBorrables
class Persona(models.Model):
    identificacion = models.CharField(max_length=20, unique=True)
    nombre = models.CharField(max_length=20)
    apellido = models.CharField(max_length=20)
    email = models.EmailField(unique=True)
    fechaNacimiento = models.DateField()
    usuario = models.ForeignKey(User)
    cargo = models.ForeignKey(Cargo)
    departamento = models.ForeignKey(Departamento)
    foto = models.TextField(blank=True)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre + " " + self.apellido

class Grupo(models.Model):# Grupo no es NoBorrable. Los grupos se pueden eliminar. No mantienen relaciones.
    nombre = models.CharField(max_length=50, unique=True)
    personas = models.ManyToManyField(Persona, blank=True)

# region Auditoria
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre
# endregion

# region Relaciones
class PersonaPermiso(models.Model):
    rol = models.IntegerField(choices=Rol.listaEnumerada())
    usuario = models.ForeignKey(Persona)
    permiso = models.ForeignKey(Permiso)

# region Auditoria
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return "{0:s} {1:s}".format(self.usuario.usuario.username, self.permiso.nombre)

class GrupoPermiso(models.Model):
    rol = models.IntegerField(choices=Rol.listaEnumerada())
    grupo = models.ForeignKey(Grupo)
    permiso = models.ForeignKey(Permiso)

# region Auditoria
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return "{0:s} {1:s}".format(self.grupo.nombre, self.permiso.nombre)
# endregion

#region utilitarias
class MenuItem:
    """Clase para representar un item del menu"""
    nombre = ""
    descripcion = ""
    funcion = ""
    modulo = ""
    path = ""
    tipoPermiso = ""
    imagen = ""
    urlPath = ""

    def __init__(self, permiso):
        self.nombre = permiso.nombre
        self.descripcion = permiso.descripcion
        self.funcion = permiso.funcion
        self.modulo = permiso.modulo
        self.path = permiso.path
        self.tipoPermiso = permiso.tipoPermiso
        self.imagen = permiso.imagen
        self.urlPath = reverse(permiso.urlPath) if permiso.urlPath is not None and permiso.urlPath != '' else ''

    def __eq__(self, other):
        if isinstance(other, MenuItem):
            return self.nombre == other.nombre
        return NotImplemented

class MenuGroup(MenuItem):
    """Clase para agrupar las aplicaciones"""
    name= ""
    menuItems = list()

    def __eq__(self, other):
        if isinstance(other, MenuGroup):
            return self.nombre == other.nombre
        return NotImplemented

#endregion

@receiver(post_email_confirm)
def post_email_confirm_callback(sender, confirmation, **kwargs):
    model_instace = confirmation.content_object
    email = confirmation.email

    model_instace.is_active = True
    model_instace.save()