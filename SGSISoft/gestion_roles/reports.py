__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.models import Persona
from model_report.report import reports, ReportAdmin


class PersonaReport(ReportAdmin):
    title = 'Reporte de Personal'
    model = Persona
    fields = [
        'nombre','apellido', 'email', 'cargo'
    ]
    list_order_by = ('nombre',)
    list_group_by = ("cargo", 'nombre')

    type = 'report'

reports.register('persona-report', PersonaReport)
