__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.models import PersonaPermiso
from util.Sesiones import SesionesUtil
from util.Util import Util

class PersonaPermisoCrud:
    def __init__(self):pass

    def create(self, personaPermiso):
        personaPermiso.setCreateUser(SesionesUtil.actualUser.username)
        personaPermiso.save()

# region Read
    def readAll(self):
        return self.whereList()

    def readId(self, id):
        return self.whereObject(pk=id)

    def readRango(self, inicio, fin):
        return self.whereList()[inicio:fin]

    def whereObject(self, *args, **kwargs):
        queryset = Util._get_queryset(PersonaPermiso)
        try:
            personaPermiso =  queryset.get(*args, **kwargs)
            return personaPermiso
        except queryset.model.DoesNotExist:
            return None

    def whereList(self, *args, **kwargs):
        queryset = Util._get_queryset(PersonaPermiso)
        obj_list = list(queryset.filter(*args, **kwargs))
        if not obj_list:
            obj_list=list()

        return obj_list

# endregion

    def update(self, personaPermiso):
        personaPermiso.setUpdateUser(SesionesUtil.actualUser.username)
        personaPermiso.save()

    def delete(self, personaPermiso):
        personaPermiso.delete()