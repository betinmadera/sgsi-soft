__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.DTO import DepartamentoDTO
from gestion_roles.crud import CargoCrud


class CargoServicios:

    def __init__(self):
        self.cargoCrud= CargoCrud()

# region Servicios Basicos
    def get(self, id):
        return self.cargoCrud.readId(id)

    def getAll(self):
        return self.cargoCrud.readAll()

    def getDTO(self, cargo):
        cargoDTO= DepartamentoDTO(cargo)
        return cargoDTO.toJSON()

    def insert(self, cargo):
        return self.cargoCrud.create(cargo)

    def update(self, cargo):
        return self.cargoCrud.update(cargo)

    def delete(self, cargo):
        return self.cargoCrud.delete(cargo)
# endregion







