__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.DTO import PersonaPermisoDTO
from gestion_roles.crud import GrupoPermisoCrud

class GrupoPermisoServicios:
    def __init__(self):
        self.grupoPermisoCrud = GrupoPermisoCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.grupoPermisoCrud.readId(id)

    def getAll(self):
        return self.grupoPermisoCrud.readAll()

    def getDTO(self, grupoPermiso):
        grupoPermisoDTO = PersonaPermisoDTO(grupoPermiso)
        return grupoPermisoDTO.toJSON()

    def insert(self, grupoPermiso):
        return self.grupoPermisoCrud.create(grupoPermiso)

    def update(self, grupoPermiso):
        return self.grupoPermisoCrud.update(grupoPermiso)

    def delete(self, grupoPermiso):
        return self.grupoPermisoCrud.delete(grupoPermiso)

    # endregion

    def getPermisosForGrupo(self, grupo):
        permisos = self.grupoPermisoCrud.whereList(grupo=grupo)
        return permisos

    def validarExistenciaPermiso(self, permisoGrupo):
        permiso = self.grupoPermisoCrud.whereObject(rol=permisoGrupo.rol, grupo=permisoGrupo.grupo.id,permiso=permisoGrupo.permiso.id)
        return permiso







