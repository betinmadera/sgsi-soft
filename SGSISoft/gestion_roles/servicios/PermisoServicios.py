# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.DTO import PermisoDTO
from gestion_roles.crud import GrupoCrud, GrupoPermisoCrud, PermisoCrud, PersonaCrud, PersonaPermisoCrud
from gestion_roles.models import Permiso, PersonaPermiso, GrupoPermiso, MenuGroup, MenuItem

class PermisoServicios:

    def __init__(self):
        self.permisoCrud= PermisoCrud()
        self.grupoPermisoCrud= GrupoPermisoCrud()
        self.personaPermisoCrud= PersonaPermisoCrud()
        self.grupoCrud=GrupoCrud()
        self.personaCrud= PersonaCrud()

# region Servicios Basicos
    def get(self, id):
        return self.permisoCrud.readId(id)

    def getAll(self):
        return self.permisoCrud.readAll()

    def getDTO(self, permiso):
        permisoDTO= PermisoDTO(permiso)
        return permisoDTO.toJSON()

    def insert(self, permiso):
        return self.permisoCrud.create(permiso)

    def update(self, permiso):
        return self.permisoCrud.update(permiso)

    def delete(self, permiso):
        return self.permisoCrud.delete(permiso)
# endregion

    def listar(self):
        permisos = Permiso.objects.all()
        return  permisos

    def getPermisoByUserAndUrl(self, user, path):
        """
        Método que verifica si un usuario tiene relacionado un permiso ya sea por grupo o por usuario. Realiza una
        búsqueda hacia arriba buscando en los padres aplanando la autorización.
        :param userId: Usuario en sesión.
        :param path: url del método a ingresar
        :return: Permiso asociado.
        """
        permiso= self.permisoCrud.whereObject(path=path)
        permisoGrupo = permiso
        usuario= self.personaCrud.whereObject(usuario=user)

        perm = list()
        perm.extend(self.personaPermisoCrud.whereList(permiso=permiso, usuario=usuario))

        #Se busca en los padres.
        while permiso.parent is not None:
            permiso=permiso.parent
            perm.extend(self.personaPermisoCrud.whereList(permiso=permiso, usuario=usuario))

        #Continua buscando en los grupos.
        grupos= self.grupoCrud.whereList(personas=usuario)
        perm.extend(self.grupoPermisoCrud.whereList(grupo__in=grupos, permiso=permisoGrupo))

        while permisoGrupo.parent is not None:
            permisoGrupo=permisoGrupo.parent
            perm.extend(self.grupoPermisoCrud.whereList(grupo__in=grupos, permiso=permisoGrupo))

        return list(set([item.rol for item in perm]))

    def getMenuByUser(self, user):
        """
        Método que obtiene un menú a partir de la persona en sesión.
        :param userId: Usuario en sesión.
        :return: Árbol de permisos.
        """
        perm = list()
        if user.is_superuser:
            perm = Permiso.objects.filter(tipoPermiso = 'Padre')
        else:
            usuario= self.personaCrud.whereObject(usuario=user)
            #busca los permisos del usuario
            perm.extend(Permiso.objects.filter(id__in = PersonaPermiso.objects.filter(usuario=usuario).values('permiso')))

            #Continua buscando en los grupos.
            grupos= self.grupoCrud.whereList(personas=usuario)
            perm.extend(Permiso.objects.filter(id__in = GrupoPermiso.objects.filter(grupo__in=grupos).values('permiso')))


        #Se arma el menu
        listMenuGroups = list()
        for permiso in perm:
            #Si el permiso tiene padre es un MenuItem
            if permiso.parent is not None:
                menuG = MenuGroup(permiso.parent)
                if menuG not in listMenuGroups:
                    listMenuGroups.append(menuG)

                menuItem = MenuItem(permiso)
                if(menuItem not in menuG.menuItems):
                    menuG.menuItems.append(menuItem)

            #Si el permiso no tiene padre es un MenuGroup
            else:
                menuG = MenuGroup(permiso)
                menuG.menuItems = [MenuItem(item) for item in permiso.get_children()]
                if menuG not in listMenuGroups:
                    listMenuGroups.append(menuG)
                else:
                    menug2 = [obj for obj in listMenuGroups if menuG == obj][0]
                    menug2.menuItems = menuG.menuItems

        return listMenuGroups