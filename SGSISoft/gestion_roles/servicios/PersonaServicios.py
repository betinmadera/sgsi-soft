# -*- encoding: utf-8 -*-
# -*- decoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from email_confirm_la.models import EmailConfirmation

from gestion_roles.DTO import PersonaDTO
from gestion_roles.crud import PersonaCrud


class PersonaServicios:
    def __init__(self):
        self.personaCrud = PersonaCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.personaCrud.readId(id)

    def getAll(self):
        return self.personaCrud.readAll()

    def getDTO(self, persona):
        personaDTO = PersonaDTO(persona)
        return personaDTO.toJSON()

    def insert(self, persona):
        return self.personaCrud.create(persona)

    def update(self, persona):
        return self.personaCrud.update(persona)

    def delete(self, persona):
        return self.personaCrud.delete(persona)

    # endregion

    def getPersonaByUser(self, usuario):
        persona = self.personaCrud.whereObject(usuario=usuario, isDisable=False)
        return persona

    def deleteWithUser(self, persona):
        #Se eliminan las verificaciones para esa cuenta
        verificaciones = EmailConfirmation.objects.get_queryset_for_object(persona.usuario)
        print(verificaciones)
        verificaciones.delete()
        persona.usuario.delete() #TODO desactivar persona. No permitir eliminación de usuarios porque se va en cascada
        #return self.personaCrud.delete(persona) TODO:Revisar eliminacion







