__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from .CargoServicios import CargoServicios
from .DepartamentoServicios import DepartamentoServicios
from .GrupoPermisoServicios import GrupoPermisoServicios
from .GrupoServicios import GrupoServicios
from .PermisoServicios import PermisoServicios
from .PersonaPermisoServicios import PersonaPermisoServicios
from .PersonaServicios import PersonaServicios