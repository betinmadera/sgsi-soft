__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.contrib.auth.models import Permission
from django.core.exceptions import ObjectDoesNotExist
from gestion_roles.DTO import DepartamentoDTO, PersonaPermisoDTO
from gestion_roles.crud import PersonaPermisoCrud

class PersonaPermisoServicios:

    def __init__(self):
        self.personaPermisoCrud= PersonaPermisoCrud()

# region Servicios Basicos
    def get(self, id):
        return self.personaPermisoCrud.readId(id)

    def getAll(self):
        return self.personaPermisoCrud.readAll()

    def getDTO(self, personaPermiso):
        personaPermisoDTO= PersonaPermisoDTO(personaPermiso)
        return personaPermisoDTO.toJSON()

    def insert(self, personaPermiso):
        return self.personaPermisoCrud.create(personaPermiso)

    def update(self, personaPermiso):
        return self.personaPermisoCrud.update(personaPermiso)

    def delete(self, personaPermiso):
        return self.personaPermisoCrud.delete(personaPermiso)
# endregion

    def getPermisosForPersona(self,persona):
        permisos = self.personaPermisoCrud.whereList(usuario=persona)
        return permisos

    def validarExistenciaPermiso(self, permisoPersona):
        permiso = self.personaPermisoCrud.whereObject(rol=permisoPersona.rol, usuario=permisoPersona.usuario.id,permiso=permisoPersona.permiso.id)
        return permiso

    def validarExistenciaPermisoMaster(self, usuario, permisoMaster):
        try:
            permiso = usuario.user_permissions.get(pk=permisoMaster.id)
            return permiso
        except ObjectDoesNotExist:
            return None