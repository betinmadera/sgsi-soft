__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.DTO import DepartamentoDTO
from gestion_roles.crud import DepartamentoCrud

class DepartamentoServicios:

    def __init__(self):
        self.departamentoCrud= DepartamentoCrud()

# region Servicios Basicos
    def get(self, id):
        return self.departamentoCrud.readId(id)

    def getAll(self):
        return self.departamentoCrud.readAll()

    def getDTO(self, departamento):
        departamentoDTO= DepartamentoDTO(departamento)
        return departamentoDTO.toJSON()

    def insert(self, departamento):
        return self.departamentoCrud.create(departamento)

    def update(self, departamento):
        return self.departamentoCrud.update(departamento)

    def delete(self, departamento):
        return self.departamentoCrud.delete(departamento)
# endregion







