__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.DTO import GrupoDTO
from gestion_roles.crud import GrupoCrud

class GrupoServicios:
    def __init__(self):
        self.grupoCrud = GrupoCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.grupoCrud.readId(id)

    def getAll(self):
        return self.grupoCrud.readAll()

    def getDTO(self, grupo):
        grupoDTO = GrupoDTO(grupo)
        return grupoDTO.toJSON()

    def insert(self, grupo):
        return self.grupoCrud.create(grupo)

    def update(self, grupo):
        return self.grupoCrud.update(grupo)

    def delete(self, grupo):
        return self.grupoCrud.delete(grupo)

    # endregion

    def getGrupos(self, persona):
        return self.grupoCrud.whereList(personas=persona)


