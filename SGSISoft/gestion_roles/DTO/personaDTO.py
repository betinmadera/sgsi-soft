__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class PersonaDTO(DTO):
    def __init__(self, persona):
        self.id= persona.id
        self.identificacion=persona.identificacion
        self.nombre= persona.nombre
        self.apellido= persona.apellido
        self.email= persona.email
        self.fechaNacimiento= persona.fechaNacimiento.isoformat()
        self.usuarioNombre= persona.usuario.username
        self.usuarioId= persona.usuario.id
        self.cargoNombre= persona.cargo.nombre
        self.cargoId= persona.cargo.id

    def toJSON(self):
        return self.__dict__
