__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class CargoDTO(DTO):
    def __init__(self, cargo):
        self.id= cargo.id
        self.nombre= cargo.nombre
        self.codigo= cargo.codigo
        self.departamentoId= cargo.departamento.id
        self.departamento= cargo.departamento.nombre