# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.models import Rol
from home.DTO import DTO

class GrupoPermisoDTO(DTO):
    def __init__(self, grupoPermiso):
        self.id= grupoPermiso.id
        self.nombre = grupoPermiso.permiso.nombre
        self.rolId= grupoPermiso.rol
        self.rol = Rol.get(grupoPermiso.rol).getNombre()
        self.grupoId= grupoPermiso.grupo.id
        self.grupo= grupoPermiso.grupo.nombre
        self.permisoId= grupoPermiso.permiso.id
