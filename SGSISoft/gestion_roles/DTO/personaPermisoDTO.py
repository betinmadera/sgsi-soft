# -*- encoding: utf-8 -*-
# -*- decoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.models import Rol
from home.DTO import DTO

class PersonaPermisoDTO(DTO):
    def __init__(self, personaPermiso):
        self.id= personaPermiso.id
        self.rolId= personaPermiso.rol
        self.rol = Rol.get(personaPermiso.rol).getNombre()
        self.usuarioId= personaPermiso.usuario.id
        self.usuarioUsername= personaPermiso.usuario.usuario.username
        self.usuario= "{0:s} {1:s}".format(personaPermiso.usuario.nombre,personaPermiso.usuario.apellido) #TODO: corregir error utf-8
        self.permisoId= personaPermiso.permiso.id
        self.nombre = personaPermiso.permiso.nombre