__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class PermisoDTO(DTO):
    def __init__(self, permiso):
        self.id= permiso.id
        self.nombre= permiso.nombre
        self.descripcion= permiso.descripcion
        self.parent= permiso.parent.nombre
        self.parentId= permiso.parent.id
        self.funcion= permiso.funcion
        self.modulo= permiso.modulo
        self.path= permiso.path
        self.tipoPermisoId= permiso.tipoPermiso.getValor()
        self.tipoPermiso=str(permiso.tipoPermiso.getNombre())
        self.imagen= permiso.imagen
    def toJSON(self):
        return self.__dict__


