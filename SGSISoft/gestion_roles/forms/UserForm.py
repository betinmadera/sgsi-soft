# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.contrib.auth.models import User
from django.forms.models import ModelForm

class UserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        super(UserForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = ['is_staff']

    def save(self, commit=True):

        if not self.edit:
            user = super(UserForm, self).save(commit=False)
            user.save()
        else:
            user = super(UserForm, self).save(commit=False)
            user.is_staff = True
            user.save()
