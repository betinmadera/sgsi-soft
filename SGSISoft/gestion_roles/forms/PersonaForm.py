# -*- encoding: utf-8 -*-
# -*- decoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from util.Constantes import Constantes
from django import forms
from django.contrib.auth.models import User
from django.forms.models import ModelForm
from email_confirm_la.models import EmailConfirmation
from gestion_roles.models import Persona
from gestion_roles.servicios import PersonaServicios
from util.Util import Util

class PersonaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        super(PersonaForm, self).__init__(*args, **kwargs)

        self.personaServicio= PersonaServicios()

    class Meta:
        model = Persona
        exclude = ['usuario']
        atributos = {'data-placeholder': "Seleccionar cargo"} #, 'class': "chzn-select"

        widgets = {
            'cargo': forms.Select(attrs=atributos),
        }
    def save(self, commit=True):
        if not self.edit:
            persona = super(PersonaForm, self).save(commit=False)

            nUsuario = User()
            nUsuario.username = persona.email
            nUsuario.first_name = persona.nombre
            nUsuario.last_name = persona.apellido
            nUsuario.email = persona.email
            nUsuario.is_active = False
            contrase = Util.contrasena_aleatoria() #persona.identificacion#
            nUsuario.set_password(contrase)
            nUsuario.save()
            persona.usuario = nUsuario

            persona.edad = Util.getEdad(persona.fechaNacimiento)

            self.personaServicio.insert(persona)

            user_template_context = {
                'password': contrase,
                'title': Constantes.TITULO_CORREO_CUENTA_CREACION,
                'firstname': nUsuario.first_name,
                'lastname' : nUsuario.last_name
            }

            email_confirmation = EmailConfirmation.objects.set_email_for_object(
                email=nUsuario.email,
                content_object=nUsuario,
                template_context=user_template_context
            )

            print email_confirmation


        else:
            persona = super(PersonaForm, self).save(commit=False)
            persona.edad = Util.getEdad(persona.fechaNacimiento)

            nUsuario = persona.usuario

            nUsuario.first_name = persona.nombre
            nUsuario.last_name = persona.apellido
            nUsuario.email = persona.email

            self.personaServicio.update(persona)