# -*- encoding: utf-8 -*-
# -*- decoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from util.Constantes import Constantes

from django import forms

class ContrasenaForm(forms.Form):

    contrasena_actual = forms.CharField(max_length=20, widget=forms.PasswordInput)
    contrasena = forms.CharField(max_length=20, widget=forms.PasswordInput, label='Ingrese nueva contraseña')
    contrasena2 = forms.CharField(max_length=20, widget=forms.PasswordInput, label='Repita la contraseña')

    def __init__(self, *args, **kwargs):
        self.usuario = kwargs.pop('usuario', None)
        super(ContrasenaForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data

        contra_act = cleaned_data.get("contrasena_actual")
        contra = cleaned_data.get("contrasena")
        contra2 = cleaned_data.get("contrasena2")

        if not contra == contra2:
            raise forms.ValidationError(Constantes.ERROR_CONTRASENAS_NO_COINCIDEN)

        try:
            usu2 = User.objects.get(username=self.usuario.username)
            if not self.usuario.check_password(contra_act):
                raise forms.ValidationError(Constantes.ERROR_CONTRASENA_INCORRECTA)
        except ObjectDoesNotExist:
            raise forms.ValidationError(Constantes.ERROR_USUARIO_NO_EXISTE)

        return cleaned_data

    def save(self, request, commit=True):
        usuario = self.usuario
        contrasena_f = self.cleaned_data['contrasena']
        usuario.set_password(contrasena_f)
        usuario.save()
        #TODO: enviar correo con confirmacion de contraseña y notificar al usuario en growl