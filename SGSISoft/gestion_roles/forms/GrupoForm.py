# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django import forms
from django.forms.models import ModelForm
from gestion_roles.models import Grupo
from gestion_roles.servicios import GrupoServicios

class GrupoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        super(GrupoForm, self).__init__(*args, **kwargs)
        self.grupoServicio= GrupoServicios()

    class Meta:
        model = Grupo
        personas = {'class': 'chzn-select', 'multiple': '', 'tabindex': '15', 'style': 'width:300px'}
        widgets = {
            'personas': forms.SelectMultiple(attrs=personas),
        }

    def save(self, commit=True):
        if not self.edit:
            grupo = super(GrupoForm, self).save(commit=False)
            self.grupoServicio.insert(grupo)
            self.save_m2m()
        else:
            grupo = super(GrupoForm, self).save(commit=False)
            self.save_m2m()
            self.grupoServicio.update(grupo)