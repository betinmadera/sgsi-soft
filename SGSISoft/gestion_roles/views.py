# -*- encoding: utf-8 -*-
# -*- decoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

# Create your views here.
import json
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http import HttpResponse, Http404
from django.template.loader import render_to_string
from email_confirm_la.models import EmailConfirmation
from gestion_roles.DTO import GrupoDTO, GrupoPermisoDTO, PersonaDTO, PersonaPermisoDTO
from gestion_roles.forms import GrupoForm, PersonaForm, ContrasenaForm
from gestion_roles.models import Permiso, Rol, GrupoPermiso, Grupo, Persona, PersonaPermiso
from gestion_roles.servicios import GrupoPermisoServicios, GrupoServicios, PermisoServicios, PersonaPermisoServicios, PersonaServicios
from home.models import Log
from util.PermisosUtil import permisos_req, permisos_req_rol
from django.contrib import admin
from util.Sesiones import SesionesUtil
from util.Util import *

personaServicios = PersonaServicios()
grupoServicios = GrupoServicios()
permisoServicios = PermisoServicios()
grupoPermisoServicios = GrupoPermisoServicios()
personaPermisoServicios = PersonaPermisoServicios()

# region User Render
@permisos_req_rol(Rol.LECTOR)
def userRender(request, id, **kwargs):
    if request.is_ajax():
        permisos = permisoServicios.getAll()
        roles = Rol.listaElementos()
        persona = personaServicios.get(id)

        classes = {key.__name__.lower(): key.__module__[:key.__module__.index(".")] for key, value in admin.site._registry.iteritems()}
        contentType_list = [ContentType.objects.get(model=key) for key in classes] #app_label=classes[key],
        permisosMaestras = []
        for elem in contentType_list:
            permisosMaestras.append({'key': elem, 'value':Permission.objects.filter(content_type= elem)})

        #permisosMaestras = Permission.objects.filter(content_type__in=contentType_list)
        permisosMaestrasUsuario = persona.usuario.user_permissions.all()

        return Util.pagina(request, "usuario_detalle.html", {'persona': persona,'nodes': permisos, 'permisosMaestras': permisosMaestras, 'permisosMaestrasUsuario': permisosMaestrasUsuario, 'roles': roles, 'id': id})
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ESCRITOR)
def userNewPost(request):
    if request.is_ajax():
        if request.method == 'POST':
            formulario = PersonaForm(request.POST)
            if formulario.is_valid():
                formulario.save()
                return HttpResponse(json.dumps({"success": True}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"success": False, "response":"Formulario Invalido", 'errors': formulario.errors}), content_type="application/json", status=500)
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ESCRITOR)
def userNewRender(request):
    if request.is_ajax():
        formulario = PersonaForm(initial={'fechaNacimiento': Util.fechaActualFormateada()})
        rendered = render_to_string('usuario_form.html', {'formulario': formulario})
        return HttpResponse(rendered, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ESCRITOR)
def userEditPost(request, id):
    if request.is_ajax():
        if request.method == 'POST':
            persona = personaServicios.get(id)
            personaForm = PersonaForm(request.POST, edit=True,instance=persona)
            if personaForm.is_valid():
                personaForm.save()
                return HttpResponse(json.dumps({"success": True}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"success": False, "response":"Formulario Invalido", 'errors': personaForm.errors}), content_type="application/json", status=500)
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ESCRITOR)
def userEditRender(request, id):
    if request.is_ajax():
        persona = personaServicios.get(id)
        personaForm = PersonaForm(instance=persona)
        #personaForm.fields['identificacion'].widget.attrs['readonly'] = True
        personaForm.fields['email'].widget.attrs['readonly'] = True
        rendered = render_to_string('usuario_form.html', {'formulario': personaForm, 'now': Util.fechaActualFormateada(), 'editId':id})
        return HttpResponse(rendered, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req
def userAllRender(request, **kwargs):
    return Util.pagina(request, "usuario_listar.html", {'tipo': 'user'})

@login_required
def userProfileRender(request):
    persona = personaServicios.getPersonaByUser(request.user)
    actividadReciente = Log.objects.filter(usuario= request.user)[0:5]
    return Util.pagina(request, "usuario_perfil.html", {'persona': persona, 'actividad': actividadReciente})

# endregion

# region User Object

@permisos_req_rol(Rol.USUARIO)
def userRead(request):
    if request.is_ajax():
        usuarios = personaServicios.getAll()
        respuesta = [PersonaDTO(usuario).toJSON() for usuario in usuarios]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ADMINISTRADOR)
def userDelete(request):
    if request.is_ajax():
        idUsuario = request.GET['idUsuario']
        usuario = personaServicios.get(idUsuario)
        personaServicios.deleteWithUser(usuario)
        respuesta = []
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.LECTOR)
def userGroupsRead(request):
    id = int(request.GET['idResponsable'])
    if request.is_ajax():
        persona = personaServicios.get(id)
        grupos = grupoServicios.getGrupos(persona)

        respuesta = [GrupoDTO(grupo).toJSON() for grupo in grupos]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)


# endregion

# region Group Render
@permisos_req_rol(Rol.ESCRITOR)
def groupNewPost(request):
    if request.is_ajax():
        if request.method == 'POST':
            formulario = GrupoForm(request.POST)
            if formulario.is_valid():
                formulario.save()
                return HttpResponse(json.dumps({"success": True}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"success": False, "response":"Formulario Invalido", 'errors': formulario.errors}), content_type="application/json", status=500)
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ESCRITOR)
def groupNewRender(request):
    if request.is_ajax():
        formulario = GrupoForm()
        rendered = render_to_string('grupo_form.html', {'formulario': formulario})
        return HttpResponse(rendered, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.LECTOR)
def groupRender(request, id):
    if request.is_ajax():
        permisos = permisoServicios.getAll()
        roles = Rol.listaElementos()
        grupo = grupoServicios.get(id)
        return Util.pagina(request, "grupo_detalle.html", {'grupo': grupo ,'nodes': permisos, 'roles': roles, 'id': id})
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ESCRITOR)
def groupEditPost(request, id):
    if request.is_ajax():
        if request.method == 'POST':
            grupo = grupoServicios.get(id)
            formulario = GrupoForm(request.POST, edit=True, instance=grupo)
            if formulario.is_valid():
                formulario.save()
                return HttpResponse(json.dumps({"success": True}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"success": False, "response":"Formulario Invalido", 'errors': formulario.errors}), content_type="application/json", status=500)
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ESCRITOR)
def groupEditRender(request, id):
    if request.is_ajax():
        grupo = grupoServicios.get(id)
        formulario = GrupoForm(instance=grupo)
        rendered = render_to_string('grupo_form.html', {'formulario': formulario, 'now': Util.fechaActualFormateada(), 'editId':id})
        return HttpResponse(rendered, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req
def groupAllRender(request):
    return Util.pagina(request, 'grupo_listar.html', {})


# endregion

# region Group Object
@permisos_req_rol(Rol.USUARIO)
def groupRead(request):
    if request.is_ajax():
        grupos = grupoServicios.getAll()
        respuesta = [GrupoDTO(grupo).toJSON() for grupo in grupos]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ADMINISTRADOR)
def groupDelete(request):
    if request.is_ajax():
        idGrupo = request.GET['idGrupo']
        grupo = grupoServicios.get(idGrupo)
        grupoServicios.delete(grupo)
        respuesta = []
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.LECTOR)
def groupUsersRead(request):
    id = int(request.GET['idResponsable'])
    if request.is_ajax():
        grupo = grupoServicios.get(id)
        miembros = grupo.personas.all()
        respuesta = [PersonaDTO(miembro).toJSON() for miembro in miembros]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.LECTOR)
def groupNoUsersRead(request):
    id = int(request.GET['idResponsable'])
    if request.is_ajax():
        grupo = grupoServicios.get(id)
        noMiembros = Persona.objects.all().exclude(id__in=grupo.personas.all())
        respuesta = [PersonaDTO(miembro).toJSON() for miembro in noMiembros]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ADMINISTRADOR)
def groupAddUser(request):
    idPersona = int(request.GET['idPersona'])
    idGrupo = int(request.GET['idGrupo'])
    if request.is_ajax():
        grupo = grupoServicios.get(idGrupo)
        persona = personaServicios.get(idPersona)
        grupo.personas.add(persona)
        grupo.save()
        return HttpResponse(json.dumps({"success": True}), content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ADMINISTRADOR)
def groupRemoveUser(request):
    idPersona = int(request.GET['idPersona'])
    idGrupo = int(request.GET['idGrupo'])
    if request.is_ajax():
        grupo = grupoServicios.get(idGrupo)
        persona = personaServicios.get(idPersona)
        grupo.personas.remove(persona)
        grupo.save()
        return HttpResponse(json.dumps({"success": True}), content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion


# region permission Object
@permisos_req_rol(Rol.ADMINISTRADOR)
def permissionCreate(request):
    if request.is_ajax():
        permiso = permisoServicios.get(int(request.GET['idPermiso']))
        rol = int(request.GET['rol'])
        tipo = request.GET['tipo']
        id = int(request.GET['idResponsable'])
        if tipo == "group":
            responsable = grupoServicios.get(id)
            grupoPermiso = GrupoPermiso(rol=rol, grupo=responsable, permiso=permiso)
            objPermiso = grupoPermisoServicios.validarExistenciaPermiso(grupoPermiso)
            if objPermiso is None:
                grupoPermisoServicios.insert(grupoPermiso)
                response_data = []
                serializado = json.dumps(response_data)
            else:
                return HttpResponse(status=409, content_type="application/json")

        else:
            responsable = personaServicios.get(id)
            personaPermiso = PersonaPermiso(rol=rol, usuario=responsable, permiso=permiso)
            objPermiso = personaPermisoServicios.validarExistenciaPermiso(personaPermiso)
            if objPermiso is None:
                personaPermisoServicios.insert(personaPermiso)
                response_data = []
                serializado = json.dumps(response_data)
            else:
                return HttpResponse(status=409, content_type="application/json")

        return HttpResponse(serializado, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.LECTOR)
def permissionRead(request):
    if request.is_ajax():
        tipo = request.GET['tipo']
        idResponsable = int(request.GET['idResponsable'])
        if tipo == "group":
            grupo = grupoServicios.get(idResponsable)
            permisos = grupoPermisoServicios.getPermisosForGrupo(grupo)
            respuesta = [GrupoPermisoDTO(permiso).toJSON() for permiso in permisos]
            serializado = json.dumps(respuesta)
            return HttpResponse(serializado, content_type="application/json")

        elif tipo == "user":
            persona = personaServicios.get(idResponsable)
            permisos = personaPermisoServicios.getPermisosForPersona(persona)
            respuesta = [PersonaPermisoDTO(permiso).toJSON() for permiso in permisos]
            serializado = json.dumps(respuesta)
            return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ADMINISTRADOR)
def permissionDelete(request):
    if request.is_ajax():
        idPermiso = request.GET['idPermiso']
        tipo = request.GET['tipo']
        if tipo == "group":
            permiso = grupoPermisoServicios.get(idPermiso)
            grupoPermisoServicios.delete(permiso)
            respuesta = []
            serializado = json.dumps(respuesta)
            return HttpResponse(serializado, content_type="application/json")

        elif tipo == "user":
            permiso = personaPermisoServicios.get(idPermiso)
            personaPermisoServicios.delete(permiso)
            respuesta = []
            serializado = json.dumps(respuesta)
            return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

@permisos_req_rol(Rol.ADMINISTRADOR)
def permissionUpdate(request):
    if request.is_ajax():
        idPermiso = request.GET['idPermiso']
        tipo = request.GET['tipo']
        idRolUpdate = int(request.GET['idRol'])
        if tipo == "group":
            permiso = grupoPermisoServicios.get(idPermiso)
            permiso.rol = idRolUpdate
            grupoPermiso = GrupoPermiso(rol=idRolUpdate, grupo=permiso.grupo, permiso=permiso.permiso)
            objPermiso = grupoPermisoServicios.validarExistenciaPermiso(grupoPermiso)
            if objPermiso is None:
                grupoPermisoServicios.update(permiso)
                response_data = []
                serializado = json.dumps(response_data)
            else:
                return HttpResponse(status=500, content_type="application/json")
        elif tipo == "user":
            permiso = personaPermisoServicios.get(idPermiso)
            permiso.rol = idRolUpdate
            personaPermiso = PersonaPermiso(rol=idRolUpdate, usuario=permiso.usuario, permiso=permiso.permiso)
            objPermiso = personaPermisoServicios.validarExistenciaPermiso(personaPermiso)
            if objPermiso is None:
                personaPermisoServicios.update(permiso)
                response_data = []
                serializado = json.dumps(response_data)
            else:
                return HttpResponse(status=500, content_type="application/json")

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion

# region permissionMaster Object
def permissionMasterRead(request):
    if request.is_ajax():
        usuario = User.objects.get(pk=int(request.GET['usuario']))
        serializado = json.dumps([{'id': obj.id, 'name': obj.name} for obj in usuario.user_permissions.all()])
        return HttpResponse(serializado, content_type="application/ json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def permissionMasterSetAdmin(request):
    if request.is_ajax():
        usuario = User.objects.get(pk=int(request.GET['usuario']))
        usuario.is_staff = request.GET['isStaff'] == 'true' #La unica forma de comparar si es true
        usuario.save()
        return HttpResponse(json.dumps({"success": True, "response": usuario.is_staff}), content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def permissionMasterCreate(request):
    if request.is_ajax():
        permiso = Permission.objects.get(pk= int(request.GET['idPermiso']))
        usuario = User.objects.get(pk=int(request.GET['usuario']))
        if(personaPermisoServicios.validarExistenciaPermisoMaster(usuario, permiso) is None):
            usuario.user_permissions.add(permiso)
        else:
            return HttpResponse(status=409, content_type="application/json")
        return HttpResponse(json.dumps({"success": True}), content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def permissionMasterDelete(request):
    if request.is_ajax():
        permiso = Permission.objects.get(pk= int(request.GET['idPermiso']))
        usuario2 = User.objects.get(pk=int(request.GET['usuario']))
        if(personaPermisoServicios.validarExistenciaPermisoMaster(usuario2, permiso) is not None):
            usuario2.user_permissions.remove(permiso)
        else:
            return HttpResponse(status=404, content_type="application/json")
        return HttpResponse(json.dumps({"success": True}), content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)
# endregion

def setPassword(request):
    if request.method == 'POST':
        formulario = ContrasenaForm(request.POST, usuario=request.user)
        if (formulario.is_valid()):
            formulario.save(request)
            return Util.redireccionar('inicio')
    else:
        formulario = ContrasenaForm()

    return Util.pagina(request, 'restablecer_contrasena.html', {'formulario': formulario})

def confirm_email(request, confirmation_key):
    try:
        email_confirmation = EmailConfirmation.objects.get(confirmation_key=confirmation_key)
    except EmailConfirmation.DoesNotExist:
        raise Http404
    email_confirmation.confirm()

    context = {
        'email_confirmation': email_confirmation,
    }
    return Util.redireccionar('inicio')

@receiver(post_save, sender=Persona)
def on_persona_post_save(sender, **kwargs):
    """
    This method is called when a usuario is saved. crea el log de Usuario
    :param sender: Persona almacenada.
    :param kwargs: variables adicionales de notificacion. Funcionan como metadata.
    :return:
    """
    persona = kwargs['instance']
    log = Log(usuario=SesionesUtil.actualUser,
          titulo='creacion de usuario',
          descripcion= "{0:s} ha creado usuario {1:s}".format(SesionesUtil.actualUser.username, persona.usuario.username))

    if not kwargs.get('created', False):
        log.titulo='actualizacion de usuario'
        log.descripcion = "{0:s} ha actualizado usuario {1:s}".format(SesionesUtil.actualUser.username, persona.usuario.username)

    log.save()

@receiver(post_save, sender=Grupo)
def on_grupo_post_save(sender, **kwargs):
    """
    This method is called when a Grupo is saved. crea el log de Grupo
    :param sender: Grupo almacenado.
    :param kwargs: variables adicionales de notificacion. Funcionan como metadata.
    :return:
    """
    grupo = kwargs['instance']
    log = Log(usuario=SesionesUtil.actualUser,
          titulo='creacion de Grupo',
          descripcion= "{0:s} ha creado grupo {1:s}".format(SesionesUtil.actualUser.username, grupo.nombre))

    if not kwargs.get('created', False):
        log.titulo='actualizacion de Grupo'
        log.descripcion= "{0:s} ha actualizado grupo {1:s}".format(SesionesUtil.actualUser.username, grupo.nombre)

    log.save()