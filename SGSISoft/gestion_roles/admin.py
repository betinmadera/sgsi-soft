# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.models import *

from django.contrib import admin

admin.site.register(Departamento)
admin.site.register(Cargo)
admin.site.register(Persona)
#admin.site.register(Grupo)
admin.site.register(Permiso)
#admin.site.register(PersonaPermiso)
#admin.site.register(GrupoPermiso)



