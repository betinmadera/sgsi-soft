# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_roles.views import *
from django.conf.urls import patterns, url
#url(r'^$', index, name='roles.index'),
urlpatterns = patterns('',
    url(r'^user$', userAllRender, name='lUsuario'),#Mostrar: pagina administracion usuarios
    url(r'^user/all$', userRead, name='userRead'),#listar: usuarios
    url(r'^user/new$', userNewRender, name='nUsuario'),#Mostrar: formulario para CREAR usuario
    url(r'^user/new_ajax$', userNewPost, name='nUsuario_ajax'),#ajax: Ajax para CREAR usuario.
    url(r'^user/edit/([\d]+)$', userEditRender, name='eUsuario'),#Mostrar: formulario para EDITAR usuario
    url(r'^user/edit_ajax/([\d]+)$', userEditPost, name='eUsuario_ajax'),#ajax: ajax para EDITAR usuario
    url(r'^user/delete$', userDelete, name='userDelete'),#Eliminar: usuario
    url(r'^user/groups$', userGroupsRead, name='userGroupsRead'),#Listar: Grupos al que pertenece el usuario
    url(r'^user/([\d]+)$', userRender, name='vUsuario'),#Detalle: de informacion del usuario determinado
    url(r'^userprofile$', userProfileRender, name='userProfile'),#Detalle: de informacion del usuario determinado

    url(r'^group$', groupAllRender, name='lGrupo'),#Mostrar: pagina para listar grupo
    url(r'^group/all$', groupRead, name='groupRead'),#Listar: grupos
    url(r'^group/new$', groupNewRender, name='nGrupo'),#Mostrar pagina para CREAR grupo
    url(r'^group/new_ajax$', groupNewPost, name='nGrupo_ajax'),#ajax: Ajax para crear usuario.
    url(r'^group/edit/([\d]+)$', groupEditRender, name='eGrupo'),#Mostrar: pagina para EDITAR grupo
    url(r'^group/edit_ajax/([\d]+)$', groupEditPost, name='eGrupo_ajax'),#ajax: ajax para EDITAR grupo
    url(r'^group/delete$', groupDelete, name='groupDelete'),#Eliminar: grupo
    url(r'^group/users$', groupUsersRead, name='groupUsersRead'),#Listar: usuarios de un grupo
    url(r'^group/no-users$', groupNoUsersRead, name='groupNoUsersRead'),#Listar: usuarios de un grupo
    url(r'^group/users-add$', groupAddUser, name='groupUsersAdd'),#Agregar: Agregar usuarios a un grupo
    url(r'^group/users-remove$', groupRemoveUser, name='groupUsersRemove'),#Remover: remover usuarios de un grupo
    url(r'^group/([\d]+)$', groupRender, name='vGrupo'),#Detalle: informacion de grupo

    #url(r'^permission/([\w]+)/([\d]+)$', permissionRender, name='aPermiso'),#Mostrar pagina de administracion de permisos grupo o usuario determinado TODO: quitar
    url(r'^permission/all$', permissionRead, name='permissionRead'),#Listar permisos
    url(r'^permission/create$', permissionCreate, name='nPermiso'),#Crear permiso
    url(r'^permission/delete$', permissionDelete, name='dPermiso'),#Eliminar permiso
    url(r'^permission/update$', permissionUpdate, name='uPermiso'),#Actualizar permiso

    url(r'^permissionmaster/all$', permissionMasterRead, name='permissionReadMaster'),#Listar permisos
    url(r'^permissionmaster/setadmin$', permissionMasterSetAdmin, name='permissionSetAdminMaster'),#Modificar si el usuario es Administrador
    url(r'^permissionmaster/create$', permissionMasterCreate, name='nPermisoMaster'),#Crear permiso
    url(r'^permissionmaster/delete$', permissionMasterDelete, name='dPermisoMaster'),#Eliminar permiso

    url(r'^setpassword$', setPassword, name='user_setpassword'),
    url(r'^key/(?P<confirmation_key>\w+)/$', confirm_email, name='confirm_email'),
)