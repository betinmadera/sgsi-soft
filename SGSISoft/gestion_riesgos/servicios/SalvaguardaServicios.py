__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.DTO.salvaguardaDTO import SalvaguardaDTO
from gestion_riesgos.crud.SalvaguardaCrud import SalvaguardaCrud


class SalvaguardaServicios:
    def __init__(self):
        self.salvaguardaCrud = SalvaguardaCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.salvaguardaCrud.readId(id)

    def getAll(self):
        return self.salvaguardaCrud.readAll()

    def getDTO(self, salvaguarda):
        salvaguardaDTO = SalvaguardaDTO(salvaguarda)
        return salvaguardaDTO.toJSON()

    def insert(self, salvaguarda):
        return self.salvaguardaCrud.create(salvaguarda)

    def update(self, salvaguarda):
        return self.salvaguardaCrud.update(salvaguarda)

    def delete(self, salvaguarda):
        return self.salvaguardaCrud.delete(salvaguarda)
    def whereLike(self,query):
        return self.salvaguardaCrud.whereList(nombre__icontains = query)

    # endregion