__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_activos.servicios.ActivoServicios import ActivoServicios
from gestion_riesgos.crud.RiesgoEfectivoCrud import RiesgoEfectivoCrud
from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_riesgos.DTO.riesgoEfectivoDTO import RiesgoEfectivoDTO

class RiesgoEfectivoServicios:
    def __init__(self):
        self.riesgoEfectivoCrud = RiesgoEfectivoCrud()
        self.activoServicios = ActivoServicios()
        self.proyectoServicios = ProyectoServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.riesgoEfectivoCrud.readId(id)

    def getAll(self):
        return self.riesgoEfectivoCrud.readAll()

    def getDTO(self, riesgoEfectivo):
        riesgoEfectivoDTO = RiesgoEfectivoDTO(riesgoEfectivo,self.proyectoServicios,self.activoServicios)
        return riesgoEfectivoDTO.toJSON()

    def insert(self, riesgoEfectivo):
        return self.riesgoEfectivoCrud.create(riesgoEfectivo)

    def update(self, riesgoEfectivo):
        return self.riesgoEfectivoCrud.update(riesgoEfectivo)

    def delete(self, riesgoEfectivo):
        return self.riesgoEfectivoCrud.delete(riesgoEfectivo)

    def whereLike(self,query):
        return self.riesgoEfectivoCrud.whereList(nombre__icontains = query)




    # endregion
