__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.MetodologiaServicios import MetodologiaServicios
from gestion_activos.servicios.ActivoServicios import ActivoServicios
from gestion_riesgos.crud.ProyectoCrud import ProyectoCrud
from gestion_riesgos.DTO.proyectoDTO import ProyectoDTO
from gestion_riesgos.servicios.SalvaguardaServicios import SalvaguardaServicios
from gestion_riesgos.servicios.AmenazaServicios import AmenazaServicios

class ProyectoServicios:
    def __init__(self):
        self.activoServicios = ActivoServicios()
        self.metodologiaServicios = MetodologiaServicios()
        self.amenazaServicios = AmenazaServicios()
        self.proyectoCrud = ProyectoCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.proyectoCrud.readId(id)

    def getAll(self):
        return self.proyectoCrud.readAll()

    def getDTO(self, proyecto):
        proyectoDTO = ProyectoDTO(proyecto,self.activoServicios,self.metodologiaServicios,self.amenazaServicios)
        return proyectoDTO.toJSON()

    def insert(self, proyecto):
        return self.proyectoCrud.create(proyecto)

    def update(self, proyecto):
        return self.proyectoCrud.update(proyecto)

    def delete(self, proyecto):
        return self.proyectoCrud.delete(proyecto)

    def addActiveToProject(self,proyecto,list):
        for activo in list:
           proyecto.activos.add(activo)

    def addThreatToProject(self,proyecto,list):
        for amenaza in list:
           proyecto.amenazas.add(amenaza)

    def addSafeguardingToProject(self,proyecto,list):
        for salvaguarda in list:
           proyecto.salvaguardas.add(salvaguarda)

    def removeActiveProject(self,proyecto,id_element):
        proyecto.activos.remove(id_element)

    def removeThreatProject(self,proyecto,id_element):
        proyecto.amenazas.remove(id_element)

    def removeSafeguardingProject(self,proyecto,id_element):
        proyecto.salvaguardas.remove(id_element)

    def convertListObjectsActive(self,list_id):
        activos = []
        for id_activo in list_id:
            activo = self.activoServicios.get(id_activo)
            activos.append(activo)

        return activos

    def convertListObjectsThreat(self,list_id):
        amenazas = []
        for id_amenaza in list_id:
            amenaza = self.amenazaServicios.get(id_amenaza)
            amenazas.append(amenaza)

        return amenazas

    def convertListObjectsSafeguarding(self,list_id):
        salvaguardas = []
        for id_salvaguarda in list_id:
            salvaguarda = self.salvaguardaServicios.get(id_salvaguarda)
            salvaguardas.append(salvaguarda)

        return salvaguardas

    # endregion

