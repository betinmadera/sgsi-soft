__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_activos.servicios.ActivoServicios import ActivoServicios
from gestion_riesgos.crud.ResultadoValoracionActivoAmenazaCrud import ResultadoValoracionActivoAmenazaCrud
from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_riesgos.servicios.AmenazaServicios import AmenazaServicios
from gestion_riesgos.servicios.RequerimientoSeguridadServicios import RequerimientoSeguridadServicios
from gestion_riesgos.DTO.resultadoValoracionActivoAmenazaDTO import ResultadoValoracionActivoAmenazaDTO
from gestion_riesgos.servicios.ValoracionActivoAmenazaDetalleServicios import ValoracionActivoAmenazaDetalleServicios
from gestion_riesgos.servicios.ValoracionActivoAmenazaServicios import ValoracionActivoAmenazaServicios
from gestion_riesgos.models import ResultadoValoracionActivoAmenaza
from gestion_riesgos.servicios.ValoracionActivoServicios import ValoracionActivoServicios
from gestion_riesgos.servicios.ValoracionActivoDetalleServicios import ValoracionActivoDetalleServicios
from gestion_riesgos.servicios.RiesgoIntrinsecoServicios import RiesgoIntrinsecoServicios
from gestion_riesgos.servicios.RiesgoEfectivoServicios import RiesgoEfectivoServicios
from gestion_riesgos.models import RiesgoIntrinseco
from gestion_riesgos.models import RiesgoEfectivo

class ResultadoValoracionActivoAmenazaServicios:
    def __init__(self):
        self.valoracionActivoAmenazaServicios = ValoracionActivoAmenazaServicios()
        self.valoracionActivoAmenazaDetalleServicios = ValoracionActivoAmenazaDetalleServicios()
        self.resultadoValoracionActivoAmenazaCrud = ResultadoValoracionActivoAmenazaCrud()
        self.activoServicios = ActivoServicios()
        self.proyectoServicios = ProyectoServicios()
        self.requerimientoSeguridadServicios = RequerimientoSeguridadServicios()
        self.amenazaServicios = AmenazaServicios()
        self.valoracionActivoServicios = ValoracionActivoServicios()
        self.valoracionActivoDetalleServicios = ValoracionActivoDetalleServicios()
        self.riesgoIntrinsecoServicios = RiesgoIntrinsecoServicios()
        self.riesgoEfectivoServicios = RiesgoEfectivoServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.resultadoValoracionActivoAmenazaCrud.readId(id)

    def getAll(self):
        return self.resultadovaloracionActivoAmenazaCrud.readAll()

    def getDTO(self, resultadoValoracionActivoAmenaza):
        resultadoValoracionActivoAmenazaDTO = ResultadoValoracionActivoAmenazaDTO(resultadoValoracionActivoAmenaza,self.proyectoServicios,self.activoServicios, self.amenazaServicios, self.requerimientoSeguridadServicios)
        return resultadoValoracionActivoAmenazaDTO.toJSON()

    def insert(self, resultadoValoraciondActivoAmenaza):
        return self.resultadoValoracionActivoAmenazaDetalle.create(resultadoValoraciondActivoAmenaza)

    def update(self, resultadoValoracionActivoAmenaza):
        return self.resultadoValoracionActivoAmenazaCrud.update(resultadoValoracionActivoAmenaza)

    def delete(self, resultadoValoracionActivoAmenaza):
        return self.resultadoValoracionActivoAmenazaCrud.delete(resultadoValoracionActivoAmenaza)
    def whereLike(self,query):
        return self.resultadoValoracionActivoAmenazaCrud.whereList(nombre__icontains = query)

    def getResultsByProject(self, proyecto):

        return self.resultadoValoracionActivoAmenazaCrud.whereList(proyecto = proyecto)

    def insertResults(self, proyecto):
        valoracionActivoAmenaza = self.valoracionActivoAmenazaServicios.getActiveThreatAssessmentByProject(proyecto)

        activos = proyecto.activos.all()

        requerimientos = proyecto.metodologia.requerimientoSeguridad.all()

        amenazas = proyecto.amenazas.all()

        for activo in activos:
            valoracionActivo = self.valoracionActivoServicios.getActiveAssessmentByProject(proyecto)
            for amenaza in amenazas:
                #Obtener valoracion de activo por requerimieto
                for requerimiento in requerimientos:
                    valoracionActivoAmenazaDetalle = self.valoracionActivoAmenazaDetalleServicios.getAssessmentActiveThreatByFilter(valoracionActivoAmenaza = valoracionActivoAmenaza, activo = activo, requerimientoSeguridad=requerimiento,amenaza=amenaza)
                    valoracionActivoDetalle = self.valoracionActivoDetalleServicios.getAssessmentByFilter(activo=activo,valoracionActivo=valoracionActivo,requerimientoSeguridad = requerimiento)
                    if valoracionActivoAmenazaDetalle is not None and valoracionActivoDetalle is not None:
                        resultadoValActivoAmenaza = ResultadoValoracionActivoAmenaza()
                        resultadoValActivoAmenaza.proyecto = proyecto
                        resultadoValActivoAmenaza.activo = activo
                        resultadoValActivoAmenaza.amenaza = amenaza
                        resultadoValActivoAmenaza.requerimientoSeguridad = requerimiento

                        degradacion = valoracionActivoAmenazaDetalle.degradacion

                        if degradacion > 0:
                            degradacion = float(degradacion)/100

                        #Hallar el impacto degradacion * valorActivo-Requerimiento
                        resultadoValActivoAmenaza.valor = float(degradacion * float(valoracionActivoDetalle.nivel.valor))

                        #se crea resultado de la valoracion
                        self.resultadoValoracionActivoAmenazaCrud.create(resultadoValActivoAmenaza)

    def calculatingSumImpacts(self, proyecto):

        # valoracionActivoAmenaza = self.valoracionActivoAmenazaServicios.getActiveThreatAssessmentByProject(proyecto)
        #
        activos = proyecto.activos.all()
        #
        # requerimientos = proyecto.metodologia.requerimientoSeguridad.all()
        #
        amenazas = proyecto.amenazas.all()
        riesgos = []
        for activo in activos:
            riesgo = 0.0
            for amenaza in amenazas:
                impacto = self.resultadoValoracionActivoAmenazaCrud.groupBy("proyecto","activo","amenaza",proyecto = proyecto, activo = activo, amenaza  = amenaza)
                valoracionActivoAmenaza = self.valoracionActivoAmenazaServicios.getActiveThreatAssessmentByProject(proyecto)
                valoracionActivoAmenazaDetalle = self.valoracionActivoAmenazaDetalleServicios.getAssessmentActiveThreatByFilter(False, valoracionActivoAmenaza = valoracionActivoAmenaza, activo = activo,amenaza=amenaza)

                if impacto is not None and valoracionActivoAmenazaDetalle:
                    frecuencia = valoracionActivoAmenazaDetalle[0].nivelFrecuencia.valor
                    if frecuencia > 0:
                        frecuencia = float(frecuencia)/100
                    if frecuencia is not None:
                        riesgo += impacto[0].get('total') * float(frecuencia)

        #Insertar RiesgoIntrinseco
            riesgos.append({"activo": activo, "riesgo":riesgo})

            riesgoIntrinseco = RiesgoIntrinseco()

            riesgoIntrinseco.proyecto = proyecto
            riesgoIntrinseco.activo = activo
            riesgoIntrinseco.valor = riesgo

            self.riesgoIntrinsecoServicios.insert(riesgoIntrinseco)



    # endregion
