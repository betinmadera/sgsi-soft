__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.NivelServicios import NivelServicios
from gestion_riesgos.DTO.valoracionSalvaguardaDetalleDTO import ValoracionSalvaguardaDetalleDTO
from gestion_riesgos.crud.ValoracionSalvaguardaDetalleCrud import ValoracionSalvaguardaDetalleCrud
from gestion_riesgos.servicios.SalvaguardaServicios import SalvaguardaServicios
from gestion_riesgos.models import ValoracionSalvaguardaDetalle
from gestion_riesgos.servicios.ValoracionSalvaguardaServicios import ValoracionSalvaguardaServicios

from StringIO import StringIO
import json

class ValoracionSalvaguardaDetalleServicios:
    def __init__(self):
        self.valoracionSalvaguardaServicios = ValoracionSalvaguardaServicios()
        self.valoracionSalvaguardaDetalleCrud = ValoracionSalvaguardaDetalleCrud()
        self.salvaguardaServicios = SalvaguardaServicios()
        self.nivelServicios = NivelServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionSalvaguardaDetalleCrud.readId(id)

    def getAll(self):
        return self.valoracionSalvaguardaDetalleCrud.readAll()

    def getDTO(self, valoracionSalvaguardaDetalle):
        valoracionSalvaguardaDetalleDTO = ValoracionSalvaguardaDetalleDTO(valoracionSalvaguardaDetalle,self.salvaguardaServicios,self.nivelServicios)
        return valoracionSalvaguardaDetalleDTO.toJSON()

    def insert(self, valoraciondSalvaguardaDetalle):
        return self.valoracionSalvaguardaDetalleCrud.create(valoraciondSalvaguardaDetalle)

    def update(self, valoracionSalvaguardaDetalle):
        return self.valoracionSalvaguardaDetalleCrud.update(valoracionSalvaguardaDetalle)

    def delete(self, valoracionSalvaguardaDetalle):
        return self.valoracionSalvaguardaDetalleCrud.delete(valoracionSalvaguardaDetalle)
    def whereLike(self,query,proyecto):
        return self.valoracionSalvaguardaDetalleCrud.whereList(salvaguarda__nombre__icontains = query, valoracionSalvaguarda__proyecto = proyecto)

    def insertAssessmentSafeguarding(self,escalaValoracion,valoracionSalvaguarda,data):

        for objectSalvaguarda in data:
            print  objectSalvaguarda
            valoracionSalvaguardaDetalle = ValoracionSalvaguardaDetalle()
            valoracionSalvaguardaDetalle.valoracionSalvaguarda = valoracionSalvaguarda
            valoracionSalvaguardaDetalle.salvaguarda = self.salvaguardaServicios.get(objectSalvaguarda.get('id'))
            valoracionSalvaguardaDetalle.nivel = self.nivelServicios.getLevelByScaleAssessmentAndName(escalaValoracion,objectSalvaguarda.get('valoracion'))

            self.insert(valoracionSalvaguardaDetalle)

    def getEvaluationValues(self,valoracionSalvaguarda):


        return self.valoracionSalvaguardaDetalleCrud.whereList(valoracionSalvaguarda = valoracionSalvaguarda)

    def valuesDictionary(self,list):
        diccionario = {}

        for valoracion in list:
            diccionario[valoracion.salvaguarda.id] = valoracion.nivel.nombre

        return diccionario

    # endregion
