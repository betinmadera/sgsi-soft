__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.crud.RequerimientoSeguridadCrud import RequerimientoSeguridadCrud
from gestion_riesgos.DTO.requerimientoSeguridadDTO import RequerimientoSeguridadDTO

class RequerimientoSeguridadServicios:
    def __init__(self):
        #self.personaServicios = PersonaServicios()
        self.requerimientoSeguridadCrud = RequerimientoSeguridadCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.requerimientoSeguridadCrud.readId(id)

    def getAll(self):
        return self.requerimientoSeguridadCrud.readAll()

    def getDTO(self, requerimientoSeguridad):
        requerimientoSeguridadDTO = RequerimientoSeguridadDTO(requerimientoSeguridad)
        return requerimientoSeguridadDTO.toJSON()

    def insert(self, requerimientoSeguridad):
        return self.requerimientoSeguridadCrud.create(requerimientoSeguridad)

    def update(self, requerimientoSeguridad):
        return self.requerimientoSeguridadCrud.update(requerimientoSeguridad)

    def delete(self, requerimientoSeguridad):
        return self.requerimientoSeguridadCrud.delete(requerimientoSeguridad)

    def convertListObjectsRequirement(self,list_id):
        requerimientos = []
        for id_requerimiento in list_id:
            requerimiento = self.get(id_requerimiento)
            requerimientos.append(requerimiento)

        return requerimientos

    # endregion