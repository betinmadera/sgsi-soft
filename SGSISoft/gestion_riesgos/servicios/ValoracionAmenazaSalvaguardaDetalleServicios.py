__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.ValoracionAmenazaSalvaguardaServicios import ValoracionAmenazaSalvaguardaServicios
from gestion_riesgos.DTO.valoracionAmenazaSalvaguardaDetalleDTO import ValoracionAmenazaSalvaguardaDetalleDTO
from gestion_riesgos.crud.ValoracionAmenazaSalvaguardaDetalleCrud import ValoracionAmenazaSalvaguardaDetalleCrud
from gestion_riesgos.models import ValoracionAmenazaSalvaguardaDetalle
from gestion_riesgos.servicios.AmenazaServicios import AmenazaServicios
from gestion_riesgos.servicios.SalvaguardaServicios import SalvaguardaServicios

class ValoracionAmenazaSalvaguardaDetalleServicios:
    def __init__(self):
        self.valoracionAmenazaSalvaguardaDetalleCrud = ValoracionAmenazaSalvaguardaDetalleCrud()
        self.valoracionAmenazaSalvaguardaServicios = ValoracionAmenazaSalvaguardaServicios()
        self.amenazaServicios = AmenazaServicios()
        self.salvaguardaServicios = SalvaguardaServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionAmenazaSalvaguardaDetalleCrud.readId(id)

    def getAll(self):
        return self.valoracionAmenazaSalvaguardaDetalleCrud.readAll()

    def getDTO(self, valoracionAmenazaSalvaguardaDetalle):
        valoracionAmenazaSalvaguardaDetalleDTO = ValoracionAmenazaSalvaguardaDetalleDTO(valoracionAmenazaSalvaguardaDetalle,self.valoracionAmenazaSalvaguardaServicios,self.amenazaServicios,self.salvaguardaServicios)
        return valoracionAmenazaSalvaguardaDetalleDTO.toJSON()

    def insert(self, valoraciondAmenazaSalvaguardaDetalle):
        return self.valoracionAmenazaSalvaguardaDetalleCrud.create(valoraciondAmenazaSalvaguardaDetalle)

    def update(self, valoracionAmenazaSalvaguardaDetalle):
        return self.valoracionAmenazaSalvaguardaDetalleCrud.update(valoracionAmenazaSalvaguardaDetalle)

    def delete(self, valoracionAmenazaSalvaguardaDetalle):
        return self.valoracionAmenazaSalvaguardaDetalleCrud.delete(valoracionAmenazaSalvaguardaDetalle)
    def whereLike(self,query):
        return self.valoracionAmenazaSalvaguardaDetalleCrud.whereList(nombre__icontains = query)

    def insertAssessmentThreatSafeguarding(self,valoracionAmenazaSalvaguarda,data):
        for object in data:
            valoracionAmenazaSalvaguardaDetalle = ValoracionAmenazaSalvaguardaDetalle()

            list_id = object.get('id').split('-')

            id_amenaza = list_id[0]
            id_salvaguarda = list_id[1]

            amenaza = self.amenazaServicios.get(id_amenaza)
            salvaguarda = self.salvaguardaServicios.get(id_salvaguarda)

            valoracionAmenazaSalvaguardaDetalle.valoracionAmenazaSalvaguarda = valoracionAmenazaSalvaguarda
            valoracionAmenazaSalvaguardaDetalle.amenaza = amenaza
            valoracionAmenazaSalvaguardaDetalle.salvaguarda  = salvaguarda

            self.insert(valoracionAmenazaSalvaguardaDetalle)

    def getThreatSafeguardingAssessment(self,valoracionAmenazaSalvaguarda):

        return self.valoracionAmenazaSalvaguardaDetalleCrud.whereList(valoracionAmenazaSalvaguarda = valoracionAmenazaSalvaguarda)

    def valuesDictionary(self,list):
        listaValoraciones = []
        _valoracion = {}
        salvaguardaActual = 0

        size = len(list) - 1

        for i, valoracion in enumerate(list):
            if salvaguardaActual == 0: salvaguardaActual =  valoracion.salvaguarda.id

            if salvaguardaActual != valoracion.salvaguarda.id:
                _valoracion = {}
                salvaguardaActual = valoracion.salvaguarda.id
                listaValoraciones.append(_valoracion)
                _valoracion["idSalvaguarda"] = valoracion.salvaguarda.id
                _valoracion["idAmenaza"] = valoracion.amenaza.id
                _valoracion["nombre"] = valoracion.salvaguarda.nombre
            else:
                _valoracion["idSalvaguarda"] = valoracion.salvaguarda.id
                _valoracion["idAmenaza"] = valoracion.amenaza.id
                _valoracion["nombre"] = valoracion.salvaguarda.nombre

            if i == size :
                listaValoraciones.append(_valoracion)

        return listaValoraciones
    # endregion
