__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_riesgos.crud.ValoracionAmenazaCrud import ValoracionAmenazaCrud
from gestion_riesgos.DTO.valoracionAmenazaDTO import ValoracionAmenazaDTO

class ValoracionAmenazaServicios:
    def __init__(self):
        self.valoracionAmenazaCrud = ValoracionAmenazaCrud()
        self.proyectoServicios = ProyectoServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionAmenazaCrud.readId(id)

    def getAll(self):
        return self.valoracionAmenazaCrud.readAll()

    def getDTO(self, valoracionAmenaza):
        valoracionAmenazaDTO = ValoracionAmenazaDTO(valoracionAmenaza,self.proyectoServicios)
        return valoracionAmenazaDTO.toJSON()

    def insert(self, valoracionAmenaza):
        return self.valoracionAmenazaCrud.create(valoracionAmenaza)

    def update(self, valoracionAmenaza):
        return self.valoracionAmenazaCrud.update(valoracionAmenaza)

    def delete(self, valoracionAmenaza):
        return self.valoracionAmenazaCrud.delete(valoracionAmenaza)

    def whereLike(self,query):
        return self.valoracionAmenazaCrud.whereList(nombre__icontains = query)

    def getThreatAssessmentByProject(self,proyecto):

        return self.valoracionAmenazaCrud.whereObject(proyecto = proyecto)

    def stateByProject(self,proyecto):
        valoracionProyecto = self.valoracionAmenazaCrud.whereObject(proyecto=proyecto)

        estado = True if valoracionProyecto != None else False

        return estado

    # endregion
