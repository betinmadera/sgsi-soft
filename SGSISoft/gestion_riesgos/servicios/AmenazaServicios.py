__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.DTO.AmenazaDTO import AmenazaDTO
from gestion_riesgos.crud.AmenazaCrud import AmenazaCrud

class AmenazaServicios:
    def __init__(self):
        self.amenazaCrud = AmenazaCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.amenazaCrud.readId(id)

    def getAll(self):
        return self.amenazaCrud.readAll()

    def getDTO(self, amenaza):
        amenazaDTO = AmenazaDTO(amenaza)
        return amenazaDTO.toJSON()

    def insert(self, amenaza):
        return self.amenazaCrud.create(amenaza)

    def update(self, amenaza):
        return self.amenazaCrud.update(amenaza)

    def delete(self, amenaza):
        return self.amenazaCrud.delete(amenaza)
    def whereLike(self,query):
        return self.amenazaCrud.whereList(nombre__icontains = query)
    def convertListObjectsThreat(self,list_id):
        amenazas = []
        for id_amenaza in list_id:
            amenaza = self.get(id_amenaza)
            amenazas.append(amenaza)

        return amenazas

    # endregion