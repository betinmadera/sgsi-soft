__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_activos.servicios.ActivoServicios import ActivoServicios
from gestion_riesgos.crud.RiesgoIntrinsecoCrud import RiesgoIntrinsecoCrud
from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_riesgos.DTO.riesgoIntrinsecoDTO import RiesgoIntrinsecoDTO

class RiesgoIntrinsecoServicios:
    def __init__(self):
        self.riesgoIntrinsecoCrud = RiesgoIntrinsecoCrud()
        self.activoServicios = ActivoServicios()
        self.proyectoServicios = ProyectoServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.riesgoIntrinsecoCrud.readId(id)

    def getAll(self):
        return self.riesgoIntrinsecoCrud.readAll()

    def getDTO(self, riesgoIntrinseco):
        riesgoIntrinsecoDTO = RiesgoIntrinsecoDTO(riesgoIntrinseco,self.proyectoServicios,self.activoServicios)
        return riesgoIntrinsecoDTO.toJSON()

    def insert(self, riesgoIntrinseco):
        return self.riesgoIntrinsecoCrud.create(riesgoIntrinseco)

    def update(self, riesgoIntrinseco):
        return self.riesgoIntrinsecoCrud.update(riesgoIntrinseco)

    def delete(self, riesgoIntrinseco):
        return self.riesgoIntrinsecoCrud.delete(riesgoIntrinseco)

    def whereLike(self,query):
        return self.riesgoIntrinsecoCrud.whereList(nombre__icontains = query)




    # endregion
