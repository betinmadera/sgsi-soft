__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.crud.NivelCrud import NivelCrud
from gestion_riesgos.servicios.EscalaValoracionServicios import EscalaValoracionServicios
from gestion_riesgos.DTO.nivelDTO import NivelDTO

class NivelServicios:
    def __init__(self):
        self.nivelCrud = NivelCrud()
        self.escalaValoracionServicios = EscalaValoracionServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.nivelCrud.readId(id)

    def getAll(self):
        return self.nivelCrud.readAll()

    def getDTO(self, nivel):
        nivelDTO = NivelDTO(nivel,self.escalaValoracionServicios)
        return nivelDTO.toJSON()

    def insert(self, nivel):
        return self.nivelCrud.create(nivel)

    def update(self, nivel):
        return self.nivelCrud.update(nivel)

    def delete(self, nivel):
        return self.nivelCrud.delete(nivel)
    def whereLike(self,query):
        return self.nivelCrud.whereList(nombre__icontains = query)
    def getLevelsByScaleAssessment(self,escala):

        niveles = self.nivelCrud.whereList(escalaValoracion = escala)

        return niveles
    def getLevelByScaleAssessmentAndValue(self,escala,value):
        niveles = self.getLevelsByScaleAssessment(escala)
        objectNivel = []

        for nivel in niveles:
            if int(value) >= nivel.minimo and int(value) <= nivel.maximo:
                objectNivel = nivel
                break

        return objectNivel

    def getLevelByScaleAssessmentAndName(self,escala,name):
        nivel = self.nivelCrud.whereObject(escalaValoracion = escala, nombre = name)

        return nivel

    # endregion