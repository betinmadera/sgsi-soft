__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.RequerimientoSeguridadServicios import RequerimientoSeguridadServicios
from gestion_activos.servicios.ActivoServicios import ActivoServicios
from gestion_riesgos.servicios.NivelServicios import NivelServicios
from gestion_riesgos.servicios.ValoracionActivoAmenazaServicios import ValoracionActivoAmenazaServicios
from gestion_riesgos.DTO.valoracionActivoAmenazaDetalleDTO import ValoracionActivoAmenazaDetalleDTO
from gestion_riesgos.crud.ValoracionActivoAmenazaDetalleCrud import ValoracionActivoAmenazaDetalleCrud
from gestion_riesgos.models import ValoracionActivoAmenazaDetalle
from gestion_riesgos.servicios.AmenazaServicios import AmenazaServicios

class ValoracionActivoAmenazaDetalleServicios:
    def __init__(self):
        self.valoracionActivoAmenazaDetalleCrud = ValoracionActivoAmenazaDetalleCrud()
        self.valoracionActivoAmenazaServicios = ValoracionActivoAmenazaServicios()
        self.activoServicios = ActivoServicios()
        self.amenazaServicios = AmenazaServicios()
        self.requerimientoServicios = RequerimientoSeguridadServicios()
        self.nivelServicios = NivelServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionActivoAmenazaDetalleCrud.readId(id)

    def getAll(self):
        return self.valoracionActivoAmenazaDetalleCrud.readAll()

    def getDTO(self, valoracionActivoAmenazaDetalle):
        valoracionActivoAmenazaDetalleDTO = ValoracionActivoAmenazaDetalleDTO(valoracionActivoAmenazaDetalle,self.requerimientoServicios,self.activoServicios,self.nivelServicios)
        return valoracionActivoAmenazaDetalleDTO.toJSON()

    def insert(self, valoraciondActivoAmenazaDetalle):
        return self.valoracionActivoAmenazaDetalleCrud.create(valoraciondActivoAmenazaDetalle)

    def update(self, valoracionActivoAmenazaDetalle):
        return self.valoracionActivoAmenazaDetalleCrud.update(valoracionActivoAmenazaDetalle)

    def delete(self, valoracionActivoAmenazaDetalle):
        return self.valoracionActivoAmenazaDetalleCrud.delete(valoracionActivoAmenazaDetalle)
    def whereLike(self,query):
        return self.valoracionActivoAmenazaDetalleCrud.whereList(nombre__icontains = query)

    def insertAssessmentActiveThreat(self,valoracionActivoAmenaza,escalaValoracion,requerimientos,data):
        for object in data:
            for requerimiento in requerimientos:
                degradacion = 0
                valorFrecuencia = 'MB'
                if object.get(requerimiento.nombre) != None:
                    degradacion = int(object.get(requerimiento.nombre))
                if object.get('Frecuencia') != None:
                    valorFrecuencia = object.get('Frecuencia')


                nivelFrecuencia = self.nivelServicios.getLevelByScaleAssessmentAndName(escalaValoracion['amenaza'],valorFrecuencia)

                valoracionActivoAmenazaDetalle = ValoracionActivoAmenazaDetalle()

                list_id = object.get('id').split('-')

                id_activo = list_id[0]
                id_amenaza = list_id[1]

                activo = self.activoServicios.get(id_activo)
                amenaza = self.amenazaServicios.get(id_amenaza)

                valoracionActivoAmenazaDetalle.valoracionActivoAmenaza = valoracionActivoAmenaza
                valoracionActivoAmenazaDetalle.activo = activo
                valoracionActivoAmenazaDetalle.amenaza = amenaza
                valoracionActivoAmenazaDetalle.requerimientoSeguridad = requerimiento
                valoracionActivoAmenazaDetalle.nivelFrecuencia = nivelFrecuencia
                valoracionActivoAmenazaDetalle.degradacion = degradacion

                self.insert(valoracionActivoAmenazaDetalle)

    def getAssessmentActiveThreat(self,valoracionActivoAmenaza):

        return self.valoracionActivoAmenazaDetalleCrud.whereList(valoracionActivoAmenaza = valoracionActivoAmenaza)

    def getAssessmentActiveThreatGroupByActive(self, valoracionActivoAmenaza):

        return self.valoracionActivoAmenazaDetalleCrud.groupBy("activo",valoracionActivoAmenaza = valoracionActivoAmenaza)

    def getAssessmentActiveThreatByFilter(self,object = True, **kwargs):

        if object:
            result = self.valoracionActivoAmenazaDetalleCrud.whereObject(**kwargs)
        else:
            result = self.valoracionActivoAmenazaDetalleCrud.whereList(**kwargs)

        return result

    def valuesDictionary(self,list):
        listaValoraciones = []
        _valoracion = {}
        amenazaActual = 0

        size = len(list) - 1

        for i,valoracion in enumerate(list):
            if amenazaActual == 0: amenazaActual =  valoracion.amenaza.id

            if amenazaActual != valoracion.amenaza.id:
                _valoracion = {}
                amenazaActual = valoracion.amenaza.id
                listaValoraciones.append(_valoracion)
                _valoracion["idAmenaza"] = valoracion.amenaza.id
                _valoracion["idActivo"] = valoracion.activo.id
                _valoracion[valoracion.requerimientoSeguridad.nombre] = valoracion.degradacion
                _valoracion["frecuencia"] = valoracion.nivelFrecuencia.nombre
                _valoracion["nombre"] = valoracion.amenaza.nombre
            else:
                _valoracion["idAmenaza"] = valoracion.amenaza.id
                _valoracion["idActivo"] = valoracion.activo.id
                _valoracion[valoracion.requerimientoSeguridad.nombre] = valoracion.degradacion
                _valoracion["frecuencia"] = valoracion.nivelFrecuencia.nombre
                _valoracion["nombre"] = valoracion.amenaza.nombre

            if i == size :
                listaValoraciones.append(_valoracion)



        return listaValoraciones





    # endregion
