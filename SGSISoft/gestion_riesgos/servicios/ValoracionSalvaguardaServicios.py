__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_riesgos.crud.ValoracionSalvaguardaCrud import ValoracionSalvaguardaCrud
from gestion_riesgos.DTO.valoracionSalvaguardaDTO import ValoracionSalvaguardaDTO

class ValoracionSalvaguardaServicios:
    def __init__(self):
        self.valoracionSalvaguardaCrud = ValoracionSalvaguardaCrud()
        self.proyectoServicios = ProyectoServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionSalvaguardaCrud.readId(id)

    def getAll(self):
        return self.valoracionSalvaguardaCrud.readAll()

    def getDTO(self, valoracionSalvaguarda):
        valoracionSalvaguardaDTO = ValoracionSalvaguardaDTO(valoracionSalvaguarda,self.proyectoServicios)
        return valoracionSalvaguardaDTO.toJSON()

    def insert(self, valoracionSalvaguarda):
        return self.valoracionSalvaguardaCrud.create(valoracionSalvaguarda)

    def update(self, valoracionSalvaguarda):
        return self.valoracionSalvaguardaCrud.update(valoracionSalvaguarda)

    def delete(self, valoracionSalvaguarda):
        return self.valoracionSalvaguardaCrud.delete(valoracionSalvaguarda)

    def whereLike(self,query):
        return self.valoracionSalvaguardaCrud.whereList(nombre__icontains = query)

    def getSafeguardingAssessmentByProject(self,proyecto):

        return self.valoracionSalvaguardaCrud.whereObject(proyecto = proyecto)

    def stateByProject(self,proyecto):
        valoracionProyecto = self.valoracionSalvaguardaCrud.whereObject(proyecto=proyecto)

        estado = True if valoracionProyecto != None else False

        return estado

    # endregion
