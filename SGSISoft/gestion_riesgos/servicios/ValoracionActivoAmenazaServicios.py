__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_riesgos.DTO.valoracionActivoAmenazaDTO import ValoracionActivoAmenazaDTO
from gestion_riesgos.crud.ValoracionActivoAmenazaCrud import ValoracionActivoAmenazaCrud

class ValoracionActivoAmenazaServicios:
    def __init__(self):
        self.valoracionActivoAmenazaCrud = ValoracionActivoAmenazaCrud()
        self.proyectoServicios = ProyectoServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionActivoAmenazaCrud.readId(id)

    def getAll(self):
        return self.valoracionActivoAmenazaCrud.readAll()

    def getDTO(self, valoracionActivoAmenaza):
        valoracionActivoAmenazaDTO = ValoracionActivoAmenazaDTO(valoracionActivoAmenaza,self.proyectoServicios)
        return valoracionActivoAmenazaDTO.toJSON()

    def insert(self, valoracionActivoAmenaza):
        return self.valoracionActivoAmenazaCrud.create(valoracionActivoAmenaza)

    def update(self, valoracionActivoAmenaza):
        return self.valoracionActivoAmenazaCrud.update(valoracionActivoAmenaza)

    def delete(self, valoracionActivoAmenaza):
        return self.valoracionActivoAmenazaCrud.delete(valoracionActivoAmenaza)
    def whereLike(self,query):
        return self.valoracionActivoAmenazaCrud.whereList(nombre__icontains = query)

    def getActiveThreatAssessmentByProject(self,proyecto):

        return self.valoracionActivoAmenazaCrud.whereObject(proyecto = proyecto)

    def stateByProject(self,proyecto):
        valoracionProyecto = self.valoracionActivoAmenazaCrud.whereObject(proyecto=proyecto)

        estado = True if valoracionProyecto != None else False

        return estado

    # endregion
