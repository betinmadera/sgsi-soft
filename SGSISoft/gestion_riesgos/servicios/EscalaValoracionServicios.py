__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.crud.EscalaValoracionCrud import EscalaValoracionCrud
from gestion_riesgos.DTO.escalaValoracionDTO import EscalaValoracionDTO

class EscalaValoracionServicios:
    def __init__(self):
        self.escalaValoracionCrud = EscalaValoracionCrud

    # region Servicios Basicos
    def get(self, id):
        return self.escalaValoracionCrud.readId(id)

    def getAll(self):
        return self.escalaValoracionCrud.readAll()

    def getDTO(self, escalaValoracion):
        escalaValoracionDTO = EscalaValoracionDTO(escalaValoracion)
        return escalaValoracionDTO.toJSON()

    def insert(self, escalaValoracion):
        return self.escalaValoracionCrud.create(escalaValoracion)

    def update(self, escalaValoracion):
        return self.escalaValoracionCrud.update(escalaValoracion)

    def delete(self, escalaValoracion):
        return self.escalaValoracionCrud.delete(escalaValoracion)

    # endregion