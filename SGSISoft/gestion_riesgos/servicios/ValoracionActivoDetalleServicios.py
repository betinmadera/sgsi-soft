__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.DTO.valoracionActivoDetalleDTO import ValoracionActivoDetalleDTO
from gestion_riesgos.servicios.ValoracionActivoServicios import ValoracionActivoServicios
from gestion_riesgos.servicios.RequerimientoSeguridadServicios import RequerimientoSeguridadServicios
from gestion_activos.servicios.ActivoServicios import ActivoServicios
from gestion_riesgos.crud.ValoracionActivoDetalleCrud import ValoracionActivoDetalleCrud
from gestion_riesgos.models import ValoracionActivosDetalle
from gestion_riesgos.servicios.NivelServicios import NivelServicios

class ValoracionActivoDetalleServicios:
    def __init__(self):
        self.valoracionActivoDetalleCrud = ValoracionActivoDetalleCrud()
        self.valoracionActivoServicios = ValoracionActivoServicios()
        self.activoServicios = ActivoServicios()
        self.requerimientoServicios = RequerimientoSeguridadServicios()
        self.nivelServicios = NivelServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionActivoDetalleCrud.readId(id)

    def getAll(self):
        return self.valoracionActivoDetalleCrud.readAll()

    def getDTO(self, valoracionActivoDetalle):
        valoracionActivoDetalleDTO = ValoracionActivoDetalleDTO(valoracionActivoDetalle,self.requerimientoServicios,self.activoServicios,self.nivelServicios)
        return valoracionActivoDetalleDTO.toJSON()

    def insert(self, valoraciondActivoDetalle):
        return self.valoracionActivoDetalleCrud.create(valoraciondActivoDetalle)

    def update(self, valoracionActivoDetalle):
        return self.valoracionActivoDetalleCrud.update(valoracionActivoDetalle)

    def delete(self, valoracionActivoDetalle):
        return self.valoracionActivoDetalleCrud.delete(valoracionActivoDetalle)
    def whereLike(self,query):
        return self.valoracionActivoDetalleCrud.whereList(nombre__icontains = query)
    def insertListOfListAssessment(self,escalaValoracion,valoracionActivo,requerimientos,activos,valoraciones):
        fila = 0
        for activo in activos:
            columna = 0
            for requerimiento in requerimientos:
                valor = valoraciones[fila][columna]
                valoracionActivoDetalle = ValoracionActivosDetalle()
                valoracionActivoDetalle.valoracionActivo = valoracionActivo
                valoracionActivoDetalle.requerimientoSeguridad = requerimiento
                valoracionActivoDetalle.activo = activo
                valoracionActivoDetalle.valor = valor
                valoracionActivoDetalle.nivel = self.nivelServicios.getLevelByScaleAssessmentAndValue(escalaValoracion,valor)
                self.insert(valoracionActivoDetalle)

                columna+=1
            fila+=1

    def getEvaluationValues(self,valoracionActivo):

        valoraciones = self.valoracionActivoDetalleCrud.whereList(valoracionActivo = valoracionActivo)

        matrizValores = {}

        for valoracion in valoraciones:
            id = str(valoracion.activo.id) +"-"+ str(valoracion.requerimientoSeguridad.id)
            matrizValores[id] = valoracion.valor

        return matrizValores

    def getAssessmentByFilter(self,**kwargs):

        return self.valoracionActivoDetalleCrud.whereObject(**kwargs)





    # endregion
