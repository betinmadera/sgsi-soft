__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.NivelServicios import NivelServicios
from gestion_riesgos.DTO.valoracionAmenazaDetalleDTO import ValoracionAmenazaDetalleDTO
from gestion_riesgos.crud.ValoracionAmenazaDetalleCrud import ValoracionAmenazaDetalleCrud
from gestion_riesgos.servicios.AmenazaServicios import AmenazaServicios
from gestion_riesgos.models import ValoracionAmenazaDetalle
from gestion_riesgos.servicios.ValoracionAmenazaServicios import ValoracionAmenazaServicios
from StringIO import StringIO
import json


class ValoracionAmenazaDetalleServicios:
    def __init__(self):
        self.valoracionAmenazaServicios = ValoracionAmenazaServicios()
        self.valoracionAmenazaDetalleCrud = ValoracionAmenazaDetalleCrud()
        self.amenazaServicios = AmenazaServicios()
        self.nivelServicios = NivelServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionAmenazaDetalleCrud.readId(id)

    def getAll(self):
        return self.valoracionAmenazaDetalleCrud.readAll()

    def getDTO(self, valoracionAmenazaDetalle):
        valoracionAmenazaDetalleDTO = ValoracionAmenazaDetalleDTO(valoracionAmenazaDetalle,self.amenazaServicios,self.nivelServicios)
        return valoracionAmenazaDetalleDTO.toJSON()

    def insert(self, valoraciondAmenazaDetalle):
        return self.valoracionAmenazaDetalleCrud.create(valoraciondAmenazaDetalle)

    def update(self, valoracionAmenazaDetalle):
        return self.valoracionAmenazaDetalleCrud.update(valoracionAmenazaDetalle)

    def delete(self, valoracionAmenazaDetalle):
        return self.valoracionAmenazaDetalleCrud.delete(valoracionAmenazaDetalle)
    def whereLike(self,query):
        return self.valoracionAmenazaDetalleCrud.whereList(nombre__icontains = query)

    def insertListOfListAssessment(self,escalaValoracion,valoracionAmenaza,amenazas,valoraciones):
        fila = 0
        for amenaza in amenazas:
            valor = valoraciones[fila][0]
            valoracionAmenazaDetalle = ValoracionAmenazaDetalle()
            valoracionAmenazaDetalle.valoracionAmenaza = valoracionAmenaza
            valoracionAmenazaDetalle.amenaza = amenaza
            nivel = self.nivelServicios.getLevelByScaleAssessmentAndName(escalaValoracion,valor)
            valoracionAmenazaDetalle.nivel = nivel
            valoracionAmenazaDetalle.valor = nivel.valor
            self.insert(valoracionAmenazaDetalle)
            fila+=1

    def getEvaluationValues(self,valoracionAmenaza):

        valoraciones = self.valoracionAmenazaDetalleCrud.whereList(valoracionAmenaza = valoracionAmenaza)
        matrizValores = {}

        for valoracion in valoraciones:
            matrizValores[valoracion.amenaza.id] = valoracion.nivel.nombre

        return matrizValores

        #Trabajar con estos valores


    # endregion
