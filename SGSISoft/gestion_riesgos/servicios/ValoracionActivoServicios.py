__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.crud.ValoracionActivoCrud import ValoracionActivoCrud
from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_riesgos.DTO.valoracionActivoDTO import ValoracionActivoDTO

class ValoracionActivoServicios:
    def __init__(self):
        self.valoracionActivoCrud = ValoracionActivoCrud()
        self.proyectoServicios = ProyectoServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionActivoCrud.readId(id)

    def getAll(self):
        return self.valoracionActivoCrud.readAll()

    def getDTO(self, valoracionActivo):
        valoracionActivoDTO = ValoracionActivoDTO(valoracionActivo,self.proyectoServicios)
        return valoracionActivoDTO.toJSON()

    def insert(self, valoracionActivo):
        return self.valoracionActivoCrud.create(valoracionActivo)

    def update(self, valoracionActivo):
        return self.valoracionActivoCrud.update(valoracionActivo)

    def delete(self, valoracionActivo):
        return self.valoracionActivoCrud.delete(valoracionActivo)
    def whereLike(self,query):
        return self.valoracionActivoCrud.whereList(nombre__icontains = query)

    def getActiveAssessmentByProject(self,proyecto):

        return self.valoracionActivoCrud.whereObject(proyecto = proyecto)

    def stateByProject(self,proyecto):
        valoracionProyecto = self.valoracionActivoCrud.whereObject(proyecto=proyecto)

        estado = True if valoracionProyecto != None else False

        return estado

    # endregion
