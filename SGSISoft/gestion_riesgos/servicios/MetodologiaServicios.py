__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.crud.MetodologiaCrud import MetodologiaCrud
from gestion_riesgos.servicios.RequerimientoSeguridadServicios import RequerimientoSeguridadServicios
from gestion_riesgos.DTO.metodologiaDTO import MetodologiaDTO
from gestion_riesgos.servicios.EscalaValoracionServicios import EscalaValoracionServicios
from gestion_roles.servicios.PersonaServicios import PersonaServicios
from gestion_riesgos.servicios.SalvaguardaServicios import SalvaguardaServicios
from gestion_riesgos.servicios.AmenazaServicios import AmenazaServicios

class MetodologiaServicios:
    def __init__(self):
        self.requerimientoSeguridadServicios = RequerimientoSeguridadServicios()
        self.escalaValoracionServicios = EscalaValoracionServicios()
        self.personaServicios = PersonaServicios()
        self.metodologiaCrud = MetodologiaCrud()
        self.salvaguardaServicios = SalvaguardaServicios()
        self.amenazaServicios = AmenazaServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.metodologiaCrud.readId(id)

    def getAll(self):
        return self.metodologiaCrud.readAll()

    def getDTO(self, metodologia):
        metodologiaDTO = MetodologiaDTO(metodologia,self.requerimientoSeguridadServicios,self.escalaValoracionServicios,self.personaServicios,self.amenazaServicios,self.salvaguardaServicios)
        return metodologiaDTO.toJSON()

    def insert(self, metodologia):
        return self.metodologiaCrud.create(metodologia)

    def update(self, metodologia):
        return self.metodologiaCrud.update(metodologia)

    def delete(self, metodologia):
        return self.metodologiaCrud.delete(metodologia)

    # endregion

