__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_riesgos.DTO.valoracionAmenazaSalvaguardaDTO import ValoracionAmenazaSalvaguardaDTO
from gestion_riesgos.crud.ValoracionAmenazaSalvaguardaCrud import ValoracionAmenazaSalvaguardaCrud

class ValoracionAmenazaSalvaguardaServicios:
    def __init__(self):
        self.valoracionAmenazaSalvaguardaCrud = ValoracionAmenazaSalvaguardaCrud()
        self.proyectoServicios = ProyectoServicios()

    # region Servicios Basicos
    def get(self, id):
        return self.valoracionAmenazaSalvaguardaCrud.readId(id)

    def getAll(self):
        return self.valoracionAmenazaSalvaguardaCrud.readAll()

    def getDTO(self, valoracionAmenazaSalvaguarda):
        valoracionAmenazaSalvaguardaDTO = ValoracionAmenazaSalvaguardaDTO(valoracionAmenazaSalvaguarda,self.proyectoServicios)
        return valoracionAmenazaSalvaguardaDTO.toJSON()

    def insert(self, valoracionAmenazaSalvaguarda):
        return self.valoracionAmenazaSalvaguardaCrud.create(valoracionAmenazaSalvaguarda)

    def update(self, valoracionAmenazaSalvaguarda):
        return self.valoracionAmenazaSalvaguardaCrud.update(valoracionAmenazaSalvaguarda)

    def delete(self, valoracionAmenazaSalvaguarda):
        return self.valoracionAmenazaSalvaguardaCrud.delete(valoracionAmenazaSalvaguarda)
    def whereLike(self,query):
        return self.valoracionAmenazaSalvaguardaCrud.whereList(nombre__icontains = query)

    def getThreatSafeguardAssessmentByProject(self,proyecto):

        return self.valoracionAmenazaSalvaguardaCrud.whereObject(proyecto = proyecto)

    def stateByProject(self,proyecto):
        valoracionProyecto = self.valoracionAmenazaSalvaguardaCrud.whereObject(proyecto=proyecto)

        estado = True if valoracionProyecto != None else False

        return estado

    # endregion
