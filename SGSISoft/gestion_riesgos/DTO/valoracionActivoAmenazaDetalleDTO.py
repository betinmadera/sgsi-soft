__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ValoracionActivoAmenazaDetalleDTO(DTO):
    def __init__(self, valoracionActivoAmenazaDetalle,valoracionActivoAmenazaServicios,requerimientoSeguridadServicios, activoServicios,nivelServicios):
        self.id = valoracionActivoAmenazaDetalle.id
        self.valoracionActivoAmenaza = valoracionActivoAmenazaServicios.getDTO(valoracionActivoAmenazaDetalle.valoracionActivoAmenaza)
        self.activo = activoServicios.getDTO(valoracionActivoAmenazaDetalle.activo)
        self.requerimientoSeguridad = requerimientoSeguridadServicios.getDTO(valoracionActivoAmenazaDetalle.requerimientoSeguridad)
        self.degradacion = valoracionActivoAmenazaDetalle.degradacion
        self.nivelFrecuencia =  nivelServicios.getDTO(valoracionActivoAmenazaDetalle.nivelFrecuencia)
