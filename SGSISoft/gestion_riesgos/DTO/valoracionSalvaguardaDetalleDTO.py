__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ValoracionSalvaguardaDetalleDTO(DTO):
    def __init__(self, valoracionSalvaguardaDetalle,valoracionSalvaguardaServicios, salvaguardaServicios, nivelServicios):
        self.id = valoracionSalvaguardaDetalle.id
        self.valoracionSalvaguarda = valoracionSalvaguardaServicios.getDTO(valoracionSalvaguardaDetalle.valoracionSalvaguarda)
        self.salvaguarda = salvaguardaServicios.getDTO(valoracionSalvaguardaDetalle.salvaguarda)
        self.nivel = nivelServicios.getDTO(valoracionSalvaguardaDetalle.nivel)

