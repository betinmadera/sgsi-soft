# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class EscalaValoracionDTO(DTO):
    def __init__(self, escalaValoracion):
        self.id= escalaValoracion.id
        self.nombre= escalaValoracion.nombre
        self.descripcion = escalaValoracion.descripcion