__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ValoracionAmenazaDetalleDTO(DTO):
    def __init__(self, valoracionAmenazaDetalle,valoracionAmenazaServicios, amenazaServicios, nivelServicios):
        self.id = valoracionAmenazaDetalle.id
        self.valoracionAmenaza = valoracionAmenazaServicios.getDTO(valoracionAmenazaDetalle.valoracionAmenaza)
        self.amenaza = amenazaServicios.getDTO(valoracionAmenazaDetalle.amenaza)
        self.nivel = nivelServicios.getDTO(valoracionAmenazaDetalle.nivel)
        self.valor = valoracionAmenazaDetalle.valor
