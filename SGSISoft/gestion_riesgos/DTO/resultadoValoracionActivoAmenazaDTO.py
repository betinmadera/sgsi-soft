__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ResultadoValoracionActivoAmenazaDTO(DTO):
    def __init__(self, resultadoValoracionActivoAmenaza,proyectoServicios, activoServicios, amenazaServicios, requerimientoSeguridadServicios):
        self.id = resultadoValoracionActivoAmenaza.id
        self.proyecto = proyectoServicios.getDTO(resultadoValoracionActivoAmenaza.proyecto)
        self.activo = activoServicios.getDTO(resultadoValoracionActivoAmenaza.activo)
        self.requerimientoSeguridad = requerimientoSeguridadServicios.getDTO(resultadoValoracionActivoAmenaza.requerimientoSeguridad)
        self.amenaza = amenazaServicios.getDTO(resultadoValoracionActivoAmenaza.amenaza)
