__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ValoracionActivoDetalleDTO(DTO):
    def __init__(self, valoracionActvoDetalle,valoracionActivoServicios,requerimientoSeguridadServicios, activoServicios,nivelServicios):
        self.id = valoracionActvoDetalle.id
        self.valoracionActivo = valoracionActivoServicios.getDTO(valoracionActvoDetalle.valoracionActivo)
        self.activo = activoServicios.getDTO(valoracionActvoDetalle.activo)
        self.requerimientoSeguridad = requerimientoSeguridadServicios.getDTO(valoracionActvoDetalle.requerimientoSeguridad)
        self.valor = valoracionActvoDetalle.valor
        self.nivel =  nivelServicios.getDTO(valoracionActvoDetalle.nivel)
