# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class MetodologiaDTO(DTO):
    def __init__(self, metodologia, requerimientoServicios, escalaValoracionServicios,personaServicios,amenazaServicios, salvaguardaServicios):
        self.id= metodologia.id
        self.nombre= metodologia.nombre
        self.descripcion= metodologia.descripcion
        self.autor = personaServicios.getDTO(metodologia.autor)
        self.fechaCreacion= metodologia.fechaCreacion.isoformat()
        self.fechaActualizacion = metodologia.fechaActualizacion.isoformat()
        self.requerimientoSeguridad = [requerimientoServicios.getDTO(r) for r in metodologia.requerimientoSeguridad.all()]
        self.escalaValoracion = [escalaValoracionServicios.getDTO(r) for r in metodologia.escalaValoracion.all()]
        self.amenazas = [amenazaServicios.getDTO(r) for r in metodologia.amenazas.all()]
        self.salvaguardas = [salvaguardaServicios.getDTO(r) for r in metodologia.salvaguardas.all()]
