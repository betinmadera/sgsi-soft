__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class RiesgoEfectivoDTO(DTO):
    def __init__(self, riesgoEfectivo,proyectoServicios, activoServicios):
        self.id = riesgoEfectivo.id
        self.proyecto = proyectoServicios.getDTO(riesgoEfectivo.proyecto)
        self.activo = activoServicios.getDTO(riesgoEfectivo.activo)
        self.valor = riesgoEfectivo.valor
