# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ProyectoDTO(DTO):
    def __init__(self, proyecto, activoServicios, metodologiaServicios,amenazaServicios):
        self.id= proyecto.id
        self.nombre= proyecto.nombre
        self.descripcion= proyecto.descripcion
        self.alcance= proyecto.alcance
        self.objetivo= proyecto.objetivo
        self.metodologia = metodologiaServicios.getDTO(proyecto.metodologia)
        self.fechaCreacion = proyecto.fechaCreacion.isoformat()
        self.activos = [activoServicios.getDTO(r) for r in proyecto.activos.all()]
        self.amenazas = [amenazaServicios.getDTO(r) for r in proyecto.amenazas.all()]