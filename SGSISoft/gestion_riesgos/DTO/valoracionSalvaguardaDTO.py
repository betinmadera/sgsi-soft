__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ValoracionSalvaguardaDTO(DTO):
    def __init__(self, valoracionSalvaguarda,proyectoServicios):
        self.id= valoracionSalvaguarda.id
        self.proyecto = proyectoServicios.getDTO(valoracionSalvaguarda.proyecto)