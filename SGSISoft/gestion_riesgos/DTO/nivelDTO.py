# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class NivelDTO(DTO):
    def __init__(self, nivel,escalaValoracionServicios):
        self.id= nivel.id
        self.nombre= nivel.nombre
        self.descripcion= nivel.descripcion
        self.posicion = nivel.posicion
        self.maximo = nivel.maximo
        self.minimo = nivel.minimo
        self.valor = nivel.valor
        self.escalaValoracion = escalaValoracionServicios.getDTO(nivel.escalaValoracion)