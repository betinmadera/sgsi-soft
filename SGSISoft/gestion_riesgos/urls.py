# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.conf.urls import patterns, url
from gestion_riesgos.views import *


urlpatterns = patterns('',
    url(r'^methodology/([\d]+)$', methodologyRender, name='vMetodologia'),#Detalle: pagina para informacion de activo
    url(r'^methodology/new$', methodologyNewRender, name='nMetodologia'),#Mostrar: pagina para crear activo
    url(r'^methodology$', methodologyAllRender, name='lMetodologia'),#Mostrar: pagina para listar activos
    url(r'^methodology/edit/([\d]+)$', methodologyEditRender, name='eMetodologia'),#Mostrar: pagina para EDITAR activos
    url(r'^methodology/all$', methodologyRead, name='methodologyRead'),#Listar: activos
    url(r'^methodology/delete$', methodologyDelete, name='methodologyDelete'),#Eliminar activos
    url(r'^methodology/threats/read$', methodologyThreatsRead, name='methodologyThreatsRead'),#Mostrar: pagina para EDITAR activos

    url(r'^project/form/new$', projectNewRender, name='nProject'),#Mostrar: pagina para EDITAR activos
    url(r'^project/form/edit/([\d]+)$', projectEditRender, name='eProject'),#Mostrar: pagina para EDITAR activos
    url(r'^project$', projectRender, name='lProject'),#Mostrar: pagina para EDITAR activos
    url(r'^project/create$', projectCreate, name='projectCreate'),#Mostrar: pagina para EDITAR activos
    url(r'^project/update$', projectUpdate, name='projectUpdate'),#Mostrar: pagina para EDITAR activos
    url(r'^project/removeElement$', projectRemoveElement, name='projectRemoveElement'),#Mostrar: pagina para EDITAR activos
    url(r'^project/read$', projectRead, name='projectRead'),#Mostrar: pagina para EDITAR activos

    url(r'^safeguarding/safeguards/read$', getSafeguards, name='getSafeguards'),#Listar: salavaguardas
    url(r'^safeguarding/search$', safeguardingSearch, name='safeguardingSearch'),#Listar: activos
    url(r'^threat/search$', threatSearch, name='threatSearch'),#Listar: activos

    url(r'^project/assessment/active$', activeAssessment, name='activeAssessment'),#Valoracion de activos
    url(r'^project/assessment/threat$', threatAssessment, name='threatAssessment'),#Valoracion de activos
    url(r'^project/assessment/activeThreat$', activeThreatAssessment, name='activeThreatAssessment'),#Valoracion de activos / amenazas
    url(r'^project/assessment/safeguarding$', safeguardingAssessment, name='safeguardingAssessment'),#Valoracion de activos / amenazas
    url(r'^project/assessment/threatSafeguarding$', threatSafeguardingAssessment, name='threatSafeguardingAssessment'),#Valoracion de activos / amenazas
    url(r'^project/assessment/activeThreatResult$', activeThreatAssessmentResult, name='activeThreatAssessmentResult'),#Valoracion de activos / amenazas


)