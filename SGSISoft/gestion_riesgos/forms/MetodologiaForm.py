# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.models import Metodologia
from gestion_riesgos.servicios.MetodologiaServicios import MetodologiaServicios
from util.Comunicaciones.NotificacionesUtil import NotificacionesUtil
from util.Comunicaciones.CorreoUtil import CorreoUtil
from django import forms
from django.forms.models import ModelForm
from datetime import date

class MetodologiaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        self.id = kwargs.pop('id', None)
        self.user = kwargs.pop('user', None)
        self.persona = kwargs.pop('persona', None)
        super(MetodologiaForm, self).__init__(*args, **kwargs)

        #Definicion de elementos necesarios
        self.metodologiaServicios = MetodologiaServicios()

    class Meta:
        model = Metodologia
        exclude = ['autor']
        select = {'data-placeholder': "Seleccionar", 'class': "chzn-select", 'tabindex': "2"}
        selectMultiple = {'class': 'chzn-select', 'multiple': '', 'tabindex': '15', 'style': 'width:300px'}

        widgets = {
            'requerimientoSeguridad': forms.SelectMultiple(attrs=selectMultiple),
            'escalaValoracion': forms.SelectMultiple(attrs=selectMultiple),
            'amenazas': forms.SelectMultiple(attrs=selectMultiple),
            'salvaguardas': forms.SelectMultiple(attrs=selectMultiple),
        }

    def save(self, commit=True):
        if not self.edit:
            metodologia = super(MetodologiaForm, self).save(commit=False)
            metodologia.autor = self.persona
            self.metodologiaServicios.insert(metodologia)
            self.save_m2m()

            print metodologia.__dict__

            notificacionesUtil=NotificacionesUtil()
            #notificacionesUtil.notificarNuevaActividad(self.user,metodologia);
            #correoUtil=CorreoUtil()
            #correoUtil.correoCreacionActividad(metodologia, metodologia.autor)
        else:
            metodologia = super(MetodologiaForm, self).save(commit=False)

            self.save_m2m()
            self.metodologiaServicios.update(metodologia)


