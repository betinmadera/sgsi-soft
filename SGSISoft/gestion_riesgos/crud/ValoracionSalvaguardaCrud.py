__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.models import ValoracionSalvaguarda
from util.Sesiones import SesionesUtil
from util.Util import Util

class ValoracionSalvaguardaCrud:

    def create(self, valoracionSalvaguarda):
        valoracionSalvaguarda.setCreateUser(SesionesUtil.actualUser.username)
        valoracionSalvaguarda.save()

# region Read
    def readAll(self, isDisable=False):
        return self.whereList(isDisable= isDisable)

    def readId(self, id, isDisable=False):
        return self.whereObject(pk=id, isDisable= isDisable)

    def readRango(self, inicio, fin, isDisable=False):
        return self.whereList(isDisable=isDisable)[inicio:fin]

    def whereObject(self, *args, **kwargs):
        queryset = Util._get_queryset(ValoracionSalvaguarda)
        try:
            valoracionSalvaguarda = queryset.get(*args, **kwargs)
            return valoracionSalvaguarda
        except queryset.model.DoesNotExist:
            return None
            #TODO: utilizar en el controller : raise Http404('No existe %s que coincida con el query.' % queryset.model._meta.object_name)

    def whereList(self, *args, **kwargs):
        queryset = Util._get_queryset(ValoracionSalvaguarda)
        obj_list = list(queryset.filter(*args, **kwargs))
        if not obj_list:
            obj_list=list()
        return obj_list

# endregion

    def update(self, valoracionSalvaguarda):
        valoracionSalvaguarda.setUpdateUser(SesionesUtil.actualUser.username)
        valoracionSalvaguarda.save()

    def delete(self, valoracionSalvaguarda):
        valoracionSalvaguarda.setUpdateUser(SesionesUtil.actualUser.username)
        valoracionSalvaguarda.setDisable(True)
        valoracionSalvaguarda.save()

