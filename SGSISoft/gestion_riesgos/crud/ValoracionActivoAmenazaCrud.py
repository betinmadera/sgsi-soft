__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.models import ValoracionActivoAmenaza
from util.Sesiones import SesionesUtil
from util.Util import Util

class ValoracionActivoAmenazaCrud:

    def create(self, valoracionActivoAmenaza):
        valoracionActivoAmenaza.setCreateUser(SesionesUtil.actualUser.username)
        valoracionActivoAmenaza.save()

# region Read
    def readAll(self, isDisable=False):
        return self.whereList(isDisable= isDisable)

    def readId(self, id, isDisable=False):
        return self.whereObject(pk=id, isDisable= isDisable)

    def readRango(self, inicio, fin, isDisable=False):
        return self.whereList(isDisable=isDisable)[inicio:fin]

    def whereObject(self, *args, **kwargs):
        queryset = Util._get_queryset(ValoracionActivoAmenaza)
        try:
            valoracionActivoAmenaza = queryset.get(*args, **kwargs)
            return valoracionActivoAmenaza
        except queryset.model.DoesNotExist:
            return None
            #TODO: utilizar en el controller : raise Http404('No existe %s que coincida con el query.' % queryset.model._meta.object_name)

    def whereList(self, *args, **kwargs):
        queryset = Util._get_queryset(ValoracionActivoAmenaza)
        obj_list = list(queryset.filter(*args, **kwargs))
        if not obj_list:
            obj_list=list()
        return obj_list

# endregion

    def update(self, valoracionActivoAmenaza):
        valoracionActivoAmenaza.setUpdateUser(SesionesUtil.actualUser.username)
        valoracionActivoAmenaza.save()

    def delete(self, valoracionActivoAmenaza):
        valoracionActivoAmenaza.setUpdateUser(SesionesUtil.actualUser.username)
        valoracionActivoAmenaza.setDisable(True)
        valoracionActivoAmenaza.save()

