__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.models import ValoracionAmenazaDetalle
from util.Sesiones import SesionesUtil
from util.Util import Util

class ValoracionAmenazaDetalleCrud:

    def create(self, valoracionAmenazaDetalle):
        valoracionAmenazaDetalle.setCreateUser(SesionesUtil.actualUser.username)
        valoracionAmenazaDetalle.save()

# region Read
    def readAll(self, isDisable=False):
        return self.whereList(isDisable= isDisable)

    def readId(self, id, isDisable=False):
        return self.whereObject(pk=id, isDisable= isDisable)

    def readRango(self, inicio, fin, isDisable=False):
        return self.whereList(isDisable=isDisable)[inicio:fin]

    def whereObject(self, *args, **kwargs):
        queryset = Util._get_queryset(ValoracionAmenazaDetalle)
        try:
            valoracionAmenazaDetalle = queryset.get(*args, **kwargs)
            return valoracionAmenazaDetalle
        except queryset.model.DoesNotExist:
            return None
            #TODO: utilizar en el controller : raise Http404('No existe %s que coincida con el query.' % queryset.model._meta.object_name)

    def whereList(self, *args, **kwargs):
        queryset = Util._get_queryset(ValoracionAmenazaDetalle)
        obj_list = list(queryset.filter(*args, **kwargs))
        if not obj_list:
            obj_list=list()
        return obj_list

# endregion

    def update(self, valoracionAmenazaDetalle):
        valoracionAmenazaDetalle.setUpdateUser(SesionesUtil.actualUser.username)
        valoracionAmenazaDetalle.save()

    def delete(self, valoracionAmenazaDetalle):
        valoracionAmenazaDetalle.setUpdateUser(SesionesUtil.actualUser.username)
        valoracionAmenazaDetalle.setDisable(True)
        valoracionAmenazaDetalle.save()

