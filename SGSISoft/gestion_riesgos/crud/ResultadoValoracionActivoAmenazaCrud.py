__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.models import ResultadoValoracionActivoAmenaza
from util.Sesiones import SesionesUtil
from util.Util import Util
from django.db.models import Sum

class ResultadoValoracionActivoAmenazaCrud:

    def create(self, resultadoValoracionActivoAmenza):
        resultadoValoracionActivoAmenza.setCreateUser(SesionesUtil.actualUser.username)
        resultadoValoracionActivoAmenza.save()

# region Read
    def readAll(self, isDisable=False):
        return self.whereList(isDisable= isDisable)

    def readId(self, id, isDisable=False):
        return self.whereObject(pk=id, isDisable= isDisable)

    def readRango(self, inicio, fin, isDisable=False):
        return self.whereList(isDisable=isDisable)[inicio:fin]

    def whereObject(self, *args, **kwargs):
        queryset = Util._get_queryset(ResultadoValoracionActivoAmenaza)
        try:
            resultadoValoracionActivoAmenaza = queryset.get(*args, **kwargs)
            return resultadoValoracionActivoAmenaza
        except queryset.model.DoesNotExist:
            return None
            #TODO: utilizar en el controller : raise Http404('No existe %s que coincida con el query.' % queryset.model._meta.object_name)

    def whereList(self, *args, **kwargs):
        queryset = Util._get_queryset(ResultadoValoracionActivoAmenaza)
        obj_list = list(queryset.filter(*args, **kwargs))
        if not obj_list:
            obj_list=list()
        return obj_list

    def groupBy(self,*args, **kwargs):
        queryset = Util._get_queryset(ResultadoValoracionActivoAmenaza)
        obj_list = list(queryset.filter(**kwargs).values(*args).annotate(total = Sum('valor')))
        if not obj_list:
            obj_list=list()
        return obj_list

# endregion

    def update(self, resultadoValoracionActivoAmenaza):
        resultadoValoracionActivoAmenaza.setUpdateUser(SesionesUtil.actualUser.username)
        resultadoValoracionActivoAmenaza.save()

    def delete(self, resultadoValoracionActivoAmenaza):
        resultadoValoracionActivoAmenaza.setUpdateUser(SesionesUtil.actualUser.username)
        resultadoValoracionActivoAmenaza.setDisable(True)
        resultadoValoracionActivoAmenaza.save()




