# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.db import models
from gestion_activos.models import Activo
from gestion_roles.models import Persona


class TipoAmenaza(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre


class Amenaza(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    tipo = models.ForeignKey(TipoAmenaza)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class TipoSalvaguarda(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre


class Salvaguarda(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    tipo = models.ForeignKey(TipoSalvaguarda)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class RequerimientoSeguridad(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre


class EscalaValoracion(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class Nivel(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    posicion = models.IntegerField()
    maximo = models.IntegerField(max_length=2)
    minimo = models.IntegerField(max_length=2)
    valor = models.IntegerField(max_length=2)
    escalaValoracion = models.ForeignKey(EscalaValoracion)


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class Metodologia(models.Model):
    nombre = models.CharField(max_length=100)
    autor = models.ForeignKey(Persona)
    descripcion = models.TextField()
    fechaCreacion = models.DateTimeField(auto_now_add=True)
    fechaActualizacion = models.DateTimeField(auto_now=True)
    requerimientoSeguridad = models.ManyToManyField(RequerimientoSeguridad)
    escalaValoracion = models.ManyToManyField(EscalaValoracion)
    amenazas = models.ManyToManyField(Amenaza)
    salvaguardas = models.ManyToManyField(Salvaguarda)



# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class Proyecto(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    alcance = models.TextField()
    objetivo = models.TextField()
    metodologia = models.ForeignKey(Metodologia)
    fechaCreacion = models.DateTimeField(auto_now_add=True)
    activos = models.ManyToManyField(Activo)
    amenazas = models.ManyToManyField(Amenaza)
    salvaguardas = models.ManyToManyField(Salvaguarda)
    #Entidades de valoracion


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class ValoracionActivos(models.Model):
    proyecto = models.ForeignKey(Proyecto)


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.proyecto.nombre

class ValoracionSalvaguarda(models.Model):
    proyecto = models.ForeignKey(Proyecto)


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.proyecto.nombre

class ValoracionSalvaguardaDetalle(models.Model):
    valoracionSalvaguarda = models.ForeignKey(ValoracionSalvaguarda)
    salvaguarda = models.ForeignKey(Salvaguarda)
    nivel = models.ForeignKey(Nivel)
    #valor = models.IntegerField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user

    def __unicode__(self):
        return self.valoracionSalvaguarda.proyecto.nombre

class ValoracionActivosDetalle(models.Model):
    valoracionActivo = models.ForeignKey(ValoracionActivos)
    activo = models.ForeignKey(Activo)
    requerimientoSeguridad = models.ForeignKey(RequerimientoSeguridad)
    nivel = models.ForeignKey(Nivel)
    valor = models.IntegerField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.valoracionActivo.proyecto.nombre

class ValoracionAmenaza(models.Model):
    proyecto = models.ForeignKey(Proyecto)


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.proyecto.nombre

class ValoracionAmenazaDetalle(models.Model):
    valoracionAmenaza = models.ForeignKey(ValoracionAmenaza)
    amenaza = models.ForeignKey(Amenaza)
    nivel = models.ForeignKey(Nivel)
    valor = models.CharField(max_length=10)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.valoracionAmenaza.proyecto.nombre

class ValoracionActivoAmenaza(models.Model):
    proyecto = models.ForeignKey(Proyecto)


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.proyecto.nombre

class ValoracionActivoAmenazaDetalle(models.Model):
    valoracionActivoAmenaza = models.ForeignKey(ValoracionActivoAmenaza)
    activo = models.ForeignKey(Activo)
    amenaza = models.ForeignKey(Amenaza)
    requerimientoSeguridad = models.ForeignKey(RequerimientoSeguridad)
    degradacion = models.IntegerField()
    nivelFrecuencia = models.ForeignKey(Nivel, related_name="frecuencia")

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.valoracionActivoAmenaza.proyecto.nombre

# Impacto causado por amenaza al activo para cada requerimiento de seguridad

class ResultadoValoracionActivoAmenaza(models.Model):
    proyecto = models.ForeignKey(Proyecto)
    activo = models.ForeignKey(Activo)
    amenaza = models.ForeignKey(Amenaza)
    requerimientoSeguridad = models.ForeignKey(RequerimientoSeguridad)
    valor = models.FloatField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.proyecto.nombre

class RiesgoIntrinseco(models.Model):
    proyecto = models.ForeignKey(Proyecto)
    activo = models.ForeignKey(Activo)
    valor = models.FloatField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.proyecto.nombre


class RiesgoEfectivo(models.Model):
    proyecto = models.ForeignKey(Proyecto)
    activo = models.ForeignKey(Activo)
    valor = models.FloatField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.proyecto.nombre

class ValoracionAmenazaSalvaguarda(models.Model):
    proyecto = models.ForeignKey(Proyecto)


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.proyecto.nombre

class ValoracionAmenazaSalvaguardaDetalle(models.Model):
    valoracionAmenazaSalvaguarda = models.ForeignKey(ValoracionAmenazaSalvaguarda)
    amenaza = models.ForeignKey(Amenaza)
    salvaguarda = models.ForeignKey(Salvaguarda)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.valoracionAmenazaSalvaguarda.proyecto.nombre

#endregion

