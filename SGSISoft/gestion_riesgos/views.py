# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

import json
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from util.Util import Util
from gestion_riesgos.forms.MetodologiaForm import MetodologiaForm
from gestion_riesgos.servicios.MetodologiaServicios import MetodologiaServicios
from gestion_roles.servicios.PersonaServicios import PersonaServicios
from django.core.serializers.json import DjangoJSONEncoder
from gestion_riesgos.servicios.ProyectoServicios import ProyectoServicios
from gestion_activos.servicios.ActivoServicios import ActivoServicios
from gestion_riesgos.servicios.AmenazaServicios import AmenazaServicios
from gestion_riesgos.servicios.SalvaguardaServicios import SalvaguardaServicios
from gestion_riesgos.models import Proyecto, Metodologia
from gestion_riesgos.servicios.RequerimientoSeguridadServicios import RequerimientoSeguridadServicios
from gestion_riesgos.models import ValoracionActivos
from gestion_riesgos.servicios.ValoracionActivoServicios import ValoracionActivoServicios
from gestion_riesgos.servicios.ValoracionActivoDetalleServicios import ValoracionActivoDetalleServicios
from gestion_riesgos.models import ValoracionAmenaza
from gestion_riesgos.servicios.ValoracionAmenazaDetalleServicios import ValoracionAmenazaDetalleServicios
from gestion_riesgos.servicios.ValoracionAmenazaServicios import ValoracionAmenazaServicios
from gestion_riesgos.models import ValoracionActivoAmenaza
from gestion_riesgos.servicios.ValoracionActivoAmenazaDetalleServicios import ValoracionActivoAmenazaDetalleServicios
from gestion_riesgos.servicios.ValoracionActivoAmenazaServicios import ValoracionActivoAmenazaServicios
from gestion_riesgos.models import ValoracionSalvaguarda
from gestion_riesgos.servicios.ValoracionSalvaguardaDetalleServicios import ValoracionSalvaguardaDetalleServicios
from gestion_riesgos.servicios.ValoracionSalvaguardaServicios import ValoracionSalvaguardaServicios
from gestion_riesgos.models import ValoracionAmenazaSalvaguarda
from gestion_riesgos.servicios.ValoracionAmenazaSalvaguardaDetalleServicios import ValoracionAmenazaSalvaguardaDetalleServicios
from gestion_riesgos.servicios.ValoracionAmenazaSalvaguardaServicios import ValoracionAmenazaSalvaguardaServicios
from gestion_riesgos.servicios.ResultadoValoracionActivoAmenazaServicios import ResultadoValoracionActivoAmenazaServicios


metodologiaServicios = MetodologiaServicios()
personaServicios = PersonaServicios()
proyectoServicios = ProyectoServicios()
activoServicios = ActivoServicios()
amenazaServicios = AmenazaServicios()
salvaguardaServicios = SalvaguardaServicios()
requerimientoSeguridadServicios = RequerimientoSeguridadServicios()
valoracionActivoServicios = ValoracionActivoServicios()
valoracionActivoDetalleServicios = ValoracionActivoDetalleServicios()

valoracionAmenazaServicios = ValoracionAmenazaServicios()
valoracionAmenazaDetalleServicios = ValoracionAmenazaDetalleServicios()

valoracionActivoAmenazaServicios = ValoracionActivoAmenazaServicios()
valoracionActivoAmenazaDetalleServicios = ValoracionActivoAmenazaDetalleServicios()
resultadoValoracionActivoAmenazaServicios = ResultadoValoracionActivoAmenazaServicios()

valoracionAmenazaSalvaguardaServicios = ValoracionAmenazaSalvaguardaServicios()
valoracionAmenazaSalvaguardaDetalleServicios = ValoracionAmenazaSalvaguardaDetalleServicios()

valoracionSalvaguardaServicios = ValoracionSalvaguardaServicios()
valoracionSalvaguardaDetalleServicios = ValoracionSalvaguardaDetalleServicios()
resultadoValoracionActivoAmenazaServicios = ResultadoValoracionActivoAmenazaServicios()


# region Resource Render
def methodologyRender(request,id):
    if request.method == 'GET':
        metodologia = metodologiaServicios.get(id)
    return Util.pagina(request, "gestion_riesgos/metodologia_detalle.html", {'metodologia':metodologia})

def methodologyNewRender(request):
    if request.method == 'POST':
        _usuario = request.user

        persona = personaServicios.getPersonaByUser(_usuario)

        formulario = MetodologiaForm(request.POST, persona = persona)
        print formulario.errors
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect(reverse('lMetodologia'))
    else:
        formulario = MetodologiaForm()

    return Util.pagina(request, "gestion_riesgos/metodologia_form.html", {'formulario':formulario})


def methodologyEditRender(request, id):
    metodologia = metodologiaServicios.get(id)
    if request.method == 'POST':
        formulario = MetodologiaForm(request.POST, edit=True,instance=metodologia)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect(reverse('lMetodologia'))
    else:
        formulario = MetodologiaForm(instance=metodologia)

    return Util.pagina(request, 'gestion_riesgos/metodologia_form.html', {'formulario': formulario})


def methodologyAllRender(request):

    return Util.pagina(request, 'gestion_riesgos/metodologia_listar.html',{})


# endregion

# region Resource Object
def methodologyRead(request):
    if request.is_ajax():
        metodologias = metodologiaServicios.getAll()
        respuesta = [metodologiaServicios.getDTO(metodologia) for metodologia in metodologias]

        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def methodologyDelete(request):
    if request.is_ajax():
        idMetodologia = request.GET['idMetodologia']
        metodologia = metodologiaServicios.get(idMetodologia)
        metodologiaServicios.delete(metodologia)
        respuesta = []
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def methodologyThreatsRead(request):
    if request.is_ajax():
        idMetodologia = request.GET['id_metodologia']
        metodologia = metodologiaServicios.get(idMetodologia)

        respuesta = [amenazaServicios.getDTO(amenaza) for amenaza in metodologia.amenazas.all()]

        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion


# region Resource Render

def projectNewRender(request):
    id_proyecto = 0

    metodologias = metodologiaServicios.getAll()
    metodologiasDict = [metodologiaServicios.getDTO(metodologia) for metodologia in metodologias]

    return Util.pagina(request, 'gestion_riesgos/proyecto_form.html', {'edit': False,
                                                                       'id_proyecto': id_proyecto,
                                                                       'metodologias':metodologiasDict
                                                                       })


def projectEditRender(request, id):

    metodologias = metodologiaServicios.getAll()
    metodologiasDict = [metodologiaServicios.getDTO(metodologia) for metodologia in metodologias]

    #Se obtiene proyecto a editar
    proyecto = proyectoServicios.get(id)

    #Establecen los estados de ese proyecto en valoraciones y sesiones

    estadoValoracionActivos = valoracionActivoServicios.stateByProject(proyecto)
    estadoValoracionAmenazas = valoracionAmenazaServicios.stateByProject(proyecto)
    estadoValoracionSalvaguardas = valoracionSalvaguardaServicios.stateByProject(proyecto)
    estadoValoracionActivoAmenaza = valoracionActivoAmenazaServicios.stateByProject(proyecto)
    estadoValoracionAmenazaSalvaguarda = valoracionAmenazaSalvaguardaServicios.stateByProject(proyecto)

    return Util.pagina(request, 'gestion_riesgos/proyecto_form.html', {'edit': True,
                                                                       'id_proyecto': id,
                                                                       'metodologias':metodologiasDict,
                                                                       'estadoValoracionActivos' : estadoValoracionActivos,
                                                                       'estadoValoracionAmenazas' : estadoValoracionAmenazas,
                                                                       'estadoValoracionSalvaguardas' : estadoValoracionSalvaguardas,
                                                                       'estadoValoracionActivoAmenaza' : estadoValoracionActivoAmenaza,
                                                                       'estadoValoracionAmenazaSalvaguarda': estadoValoracionAmenazaSalvaguarda
                                                                       })


def projectAllRender(request):

    return Util.pagina(request, 'gestion_activos/recurso_listar.html',{})

def projectRender(request):

    proyectos = proyectoServicios.getAll()

    return Util.pagina(request, 'gestion_riesgos/proyecto_listar.html',{'proyectos':proyectos})

def projectCreate(request):
    if request.is_ajax():

        nombre = request.GET['nombre']
        descripcion = request.GET['descripcion']
        alcance = request.GET['alcance']
        objetivo = request.GET['objetivo']
        id_metodologia = request.GET['metodologia']

        array_activos_id = json.loads(request.GET['activos'])
        array_amenazas_id = json.loads(request.GET['amenazas'])


        activos = proyectoServicios.convertListObjectsActive(array_activos_id)
        amenazas = proyectoServicios.convertListObjectsThreat(array_amenazas_id)


        proyecto = Proyecto()
        metodologia = metodologiaServicios.get(id_metodologia)


        proyecto.nombre = nombre
        proyecto.descripcion = descripcion
        proyecto.alcance = alcance
        proyecto.objetivo = objetivo
        proyecto.metodologia = metodologia

        resultado = proyectoServicios.insert(proyecto)

        proyectoServicios.addActiveToProject(proyecto,activos)
        proyectoServicios.addThreatToProject(proyecto,amenazas)

        response = {"response":True}

        serializado = json.dumps(response)

        return HttpResponse(serializado, content_type="application/json")

    return Util.pagina(request, 'gestion_activos/recurso_listar.html',{})

def projectUpdate(request):
    if request.is_ajax():
        id_proyecto = request.GET['id_proyecto']
        nombre = request.GET['nombre']

        alcance = request.GET['alcance']
        objetivo = request.GET['objetivo']

        descripcion = request.GET['descripcion']
        id_metodologia = request.GET['metodologia']
        array_activos_id = json.loads(request.GET['activos'])
        array_amenazas_id = json.loads(request.GET['amenazas'])


        proyecto = proyectoServicios.get(id_proyecto)

        activos = proyectoServicios.convertListObjectsActive(array_activos_id)
        amenazas = proyectoServicios.convertListObjectsThreat(array_amenazas_id)

        metodologia = metodologiaServicios.get(id_metodologia)


        proyecto.nombre = nombre
        proyecto.descripcion = descripcion
        proyecto.alcance = alcance
        proyecto.objetivo = objetivo
        proyecto.metodologia = metodologia

        resultado = proyectoServicios.update(proyecto)

        proyectoServicios.addActiveToProject(proyecto,activos)
        proyectoServicios.addThreatToProject(proyecto,amenazas)

        response = {"response":True}

        serializado = json.dumps(response)

        return HttpResponse(serializado, content_type="application/json")

    return Util.pagina(request, 'gestion_activos/recurso_listar.html',{})

def projectRead(request):
    #Buscar valorariones amenazas y activos
    if request.is_ajax():
        id_proyecto = request.GET['id_proyecto']

        proyecto = proyectoServicios.get(id_proyecto)

        proyecto_json = proyectoServicios.getDTO(proyecto)



        #serializado = json.dumps(proyecto_json)

        valoracionAmenaza = valoracionAmenazaServicios.getThreatAssessmentByProject(proyecto)

        valoracionesAmenazas = valoracionAmenazaDetalleServicios.getEvaluationValues(valoracionAmenaza)

        valoracionActivo = valoracionActivoServicios.getActiveAssessmentByProject(proyecto)

        valoracionesActivos = valoracionActivoDetalleServicios.getEvaluationValues(valoracionActivo)

        valoracionActivoAmenaza = valoracionActivoAmenazaServicios.getActiveThreatAssessmentByProject(proyecto)
        valoracionActivoAmenazas = valoracionActivoAmenazaDetalleServicios.getAssessmentActiveThreat(valoracionActivoAmenaza)


        valoracionSalvaguarda = valoracionSalvaguardaServicios.getSafeguardingAssessmentByProject(proyecto)

        valoracionSalvaguardas = valoracionSalvaguardaDetalleServicios.getEvaluationValues(valoracionSalvaguarda)

        valoracionAmenazaSalvaguarda = valoracionAmenazaSalvaguardaServicios.getThreatSafeguardAssessmentByProject(proyecto)
        valoracionAmenazasSalvaguardas = valoracionAmenazaSalvaguardaDetalleServicios.getThreatSafeguardingAssessment(valoracionAmenazaSalvaguarda)


        _proyecto = {
            "proyecto": proyecto_json,
            "ValoracionesAmenazas": valoracionesAmenazas,
            "ValoracionesActivos": valoracionesActivos,
            "ValoracionesSalvaguardas": valoracionSalvaguardaDetalleServicios.valuesDictionary(valoracionSalvaguardas),
            "ValoracionesActivosAmenazas": valoracionActivoAmenazaDetalleServicios.valuesDictionary(valoracionActivoAmenazas),
            "ValoracionAmenazasSalvaguardas": valoracionAmenazaSalvaguardaDetalleServicios.valuesDictionary(valoracionAmenazasSalvaguardas)
        }

        serializado = json.dumps(_proyecto)


        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def projectRemoveElement(request):
    if request.is_ajax():
        id_proyecto = request.GET['id_proyecto']
        id_element = request.GET['id_element']
        type = request.GET['type']

        proyecto = proyectoServicios.get(id_proyecto)

        if type == "activo":
            proyectoServicios.removeActiveProject(proyecto,id_element)
        elif type == "amenaza":
            proyectoServicios.removeThreatProject(proyecto,id_element)
        elif type == "salvaguarda":
            proyectoServicios.removeSafeguardingProject(proyecto,id_element)
        else:
            element = None


        response = {'response' : True}

        serializado = json.dumps(response)

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)


def threatSearch(request):
    if request.is_ajax():
        query = request.GET['query']
        amenazas = amenazaServicios.whereLike(query)
        respuesta = [amenazaServicios.getDTO(amenaza) for amenaza in amenazas]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def safeguardingSearch(request):
    if request.is_ajax():
        query = request.GET['query']
        salvaguardas = salvaguardaServicios.whereLike(query)
        respuesta = [salvaguardaServicios.getDTO(salvaguarda) for salvaguarda in salvaguardas]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

#Se obtiene todas las salvaguardas
def getSafeguards(request):
    if request.is_ajax():
        salvaguardas = salvaguardaServicios.getAll()
        respuesta = [salvaguardaServicios.getDTO(salvaguarda) for salvaguarda in salvaguardas]
        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

#Guarda valoraciones de activos
def activeAssessment(request):
    if request.is_ajax():
        id_proyecto = json.loads(request.GET['proyecto'])
        id_requerimientos = json.loads(request.GET['requerimientos'])
        id_activos = json.loads(request.GET['activos'])
        valoraciones = json.loads(request.GET['valoraciones'])

        proyecto = proyectoServicios.get(id_proyecto)
        #Se obtiene la escala de valoracion de activos
        escalaValoracion= proyecto.metodologia.escalaValoracion.all()[0]

        valoracioActivo = ValoracionActivos()
        valoracioActivo.proyecto = proyecto

        #Insertar valoracionActivos
        if valoracionActivoServicios.getActiveAssessmentByProject(proyecto) is None:
            valoracionActivoServicios.insert(valoracioActivo)

            requerimientos = requerimientoSeguridadServicios.convertListObjectsRequirement(id_requerimientos)
            activos = activoServicios.convertListObjectsActive(id_activos)

            valoracionActivoDetalleServicios.insertListOfListAssessment(escalaValoracion,valoracioActivo,requerimientos,activos,valoraciones)

        respuesta = {"response":True}
        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)
#Guarda valoraciones de amenezas
def threatAssessment(request):
    if request.is_ajax():
        id_proyecto = json.loads(request.GET['proyecto'])
        id_amenazas = json.loads(request.GET['amenazas'])
        valoraciones = json.loads(request.GET['valoraciones'])
        proyecto = proyectoServicios.get(id_proyecto)
        #Se obtiene la escala de valoracion de amenazas

        escalaValoracion = proyecto.metodologia.escalaValoracion.all()[1]


        valoracionAmenaza = ValoracionAmenaza()
        valoracionAmenaza.proyecto = proyecto
        if valoracionAmenazaServicios.getThreatAssessmentByProject(proyecto) is None:
            valoracionAmenazaServicios.insert(valoracionAmenaza)
            amenazas = amenazaServicios.convertListObjectsThreat(id_amenazas)
            valoracionAmenazaDetalleServicios.insertListOfListAssessment(escalaValoracion,valoracionAmenaza,amenazas,valoraciones)

        respuesta = {"response":True}
        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")


    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# Guardar Valoraciones de activos amenazas
def activeThreatAssessment(request):
    if request.is_ajax():
        id_proyecto = json.loads(request.GET['proyecto'])
        id_requerimientos = json.loads(request.GET['requerimientos'])
        #valoraciones = json.loads(request.GET['valoraciones'])
        data = json.loads(request.GET['data'])

        proyecto = proyectoServicios.get(id_proyecto)

        #Se obtiene la escala de valoracion de amenazas

        escalaValoracionAmenazas = proyecto.metodologia.escalaValoracion.all()[1]
        escalaValoracionActivos = proyecto.metodologia.escalaValoracion.all()[0]

        escalaValoracion = {
            'amenaza' : escalaValoracionAmenazas,
            'activo' : escalaValoracionActivos
        }



        requerimientos = requerimientoSeguridadServicios.convertListObjectsRequirement(id_requerimientos)

        valoracionActivoAmenaza = ValoracionActivoAmenaza()

        valoracionActivoAmenaza.proyecto = proyecto
        if valoracionActivoAmenazaServicios.getActiveThreatAssessmentByProject(proyecto) is None:
            valoracionActivoAmenazaServicios.insert(valoracionActivoAmenaza)
            valoracionActivoAmenazaDetalleServicios.insertAssessmentActiveThreat(valoracionActivoAmenaza,escalaValoracion,requerimientos,data)

        respuesta = {"response":True}

        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")


    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# Guardar resultados Valoraciones de activos amenazas
def activeThreatAssessmentResult(request):
    if request.is_ajax():
        id_proyecto = json.loads(request.GET['proyecto'])

        proyecto = proyectoServicios.get(id_proyecto)

        valoracionActivoAmenaza = valoracionActivoAmenazaServicios.getActiveThreatAssessmentByProject(proyecto)


        #Para pruebas
        # valoraciones = valoracionActivoAmenazaDetalleServicios.getAssessmentActiveThreatGroupByActive(valoracionActivoAmenaza)

        #Insertar valoraciones

        resultados = resultadoValoracionActivoAmenazaServicios.getResultsByProject(proyecto)

        if not resultados:
            resultadoValoracionActivoAmenazaServicios.insertResults(proyecto)

        #Calculo de riesgo intrinseco
            resultadoValoracionActivoAmenazaServicios.calculatingSumImpacts(proyecto)

        respuesta = {"response":True}

        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")


    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

#Guarda valoracion de salvaguardas
def safeguardingAssessment(request):
    if request.is_ajax():
        id_proyecto = json.loads(request.GET['proyecto'])

        data = json.loads(request.GET['data'])

        proyecto = proyectoServicios.get(id_proyecto)


        valoracionSalvaguarda = ValoracionSalvaguarda()
        valoracionSalvaguarda.proyecto = proyecto

        if valoracionSalvaguardaServicios.getSafeguardingAssessmentByProject(proyecto) is None:

            valoracionSalvaguardaServicios.insert(valoracionSalvaguarda)

            #Se obtiene la escala de valoracion de amenazas

            escalaValoracionSalvaguardas = proyecto.metodologia.escalaValoracion.all()[2]

            valoracionSalvaguardaDetalleServicios.insertAssessmentSafeguarding(escalaValoracionSalvaguardas,valoracionSalvaguarda,data)

        respuesta = {"response":True}

        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion

def threatSafeguardingAssessment(request):
    if request.is_ajax():
        id_proyecto = json.loads(request.GET['proyecto'])
        data = json.loads(request.GET['data'])

        proyecto = proyectoServicios.get(id_proyecto)

        valoracionAmenazaSalvaguarda = ValoracionAmenazaSalvaguarda()
        valoracionAmenazaSalvaguarda.proyecto = proyecto

        if valoracionAmenazaSalvaguardaServicios.getThreatSafeguardAssessmentByProject(proyecto) is None:

            valoracionAmenazaSalvaguardaServicios.insert(valoracionAmenazaSalvaguarda)

            valoracionAmenazaSalvaguardaDetalleServicios.insertAssessmentThreatSafeguarding(valoracionAmenazaSalvaguarda,data)

        respuesta = {"response":True}

        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")


    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion

