# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_riesgos.models import *
from django.contrib import admin

admin.site.register(RequerimientoSeguridad)
admin.site.register(EscalaValoracion)
admin.site.register(TipoSalvaguarda)
admin.site.register(Salvaguarda)
admin.site.register(TipoAmenaza)
admin.site.register(Amenaza)
admin.site.register(Nivel)
admin.site.register(Proyecto)
admin.site.register(ValoracionAmenaza)
admin.site.register(ValoracionAmenazaDetalle)
admin.site.register(ValoracionActivos)
admin.site.register(ValoracionActivosDetalle)
admin.site.register(ValoracionActivoAmenaza)
admin.site.register(ValoracionActivoAmenazaDetalle)
admin.site.register(ValoracionAmenazaSalvaguarda)
admin.site.register(ValoracionAmenazaSalvaguardaDetalle)
admin.site.register(ValoracionSalvaguarda)
admin.site.register(ValoracionSalvaguardaDetalle)
admin.site.register(ResultadoValoracionActivoAmenaza)
admin.site.register(RiesgoIntrinseco)









