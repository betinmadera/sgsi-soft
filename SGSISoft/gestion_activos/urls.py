__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.conf.urls import patterns, url
from gestion_activos.views import *

urlpatterns = patterns('',
    url(r'^active/([\d]+)$', activeRender, name='vActivo'),#Detalle: pagina para informacion de activo
    url(r'^active/new$', activeNewRender, name='nActivo'),#Mostrar: pagina para crear activo
    url(r'^active$', activeAllRender, name='lActivo'),#Mostrar: pagina para listar activos
    url(r'^active/edit/([\d]+)$', activeEditRender, name='eActivo'),#Mostrar: pagina para EDITAR activos
    url(r'^active/all$', activeRead, name='activeRead'),#Listar: activos
    url(r'^active/delete$', activeDelete, name='activeDelete'),#Eliminar activos
    url(r'^active/search$', activeSearch, name='activeSearch'),#Listar: activos

    url(r'^resource/([\d]+)$', resourceRender, name='vRecurso'),#Detalle: pagina para informacion de activo
    url(r'^resource/new$', resourceNewRender, name='nRecurso'),#Mostrar: pagina para crear activo
    url(r'^resource$', resourceAllRender, name='lRecurso'),#Mostrar: pagina para listar activos
    url(r'^resource/edit/([\d]+)$', resourceEditRender, name='eRecurso'),#Mostrar: pagina para EDITAR activos
    url(r'^resource/all$', resourceRead, name='resourceRead'),#Listar: activos
    url(r'^resource/delete$', resourceDelete, name='resourceDelete'),#Eliminar activos
)