# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ActivoDTO(DTO):
    def __init__(self, activo,personaServicios,activoServicios):
        self.id = activo.id
        self.codigo = activo.codigo
        self.nombre = activo.nombre
        self.descripcion = activo.descripcion
        self.atributos = activo.atributos
        if(activo.tipo is not None):
            self.tipo = activo.tipo.nombre
        else:
            self.tipo = None
        self.dependencias = [activoServicios.getDTO(r) for r in activo.dependencias.all()]
        self.responsables = [personaServicios.getDTO(r) for r in activo.responsables.all()]
        self.cantidad= activo.cantidad