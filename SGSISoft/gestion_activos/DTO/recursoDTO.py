# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class RecursoDTO(DTO):
    def __init__(self, recurso,personaServicios,activoServicios):
        self.id = recurso.id
        self.nombre = recurso.nombre
        self.descripcion = recurso.descripcion
        if(recurso.tipo is not None):
            self.tipo = recurso.tipo.nombre
        else:
            self.tipo = None
        self.activos = [activoServicios.getDTO(r) for r in recurso.activos.all()]
        self.usuarios = [personaServicios.getDTO(r) for r in recurso.usuarios.all()]