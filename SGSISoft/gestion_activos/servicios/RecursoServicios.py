__author__ = 'Jhonny Madera'
from gestion_activos.servicios import ActivoServicios
from gestion_activos.crud.RecursoCrud import RecursoCrud
from gestion_roles.servicios.PersonaServicios import PersonaServicios
from gestion_activos.DTO.recursoDTO import RecursoDTO



class RecursoServicios:

    def __init__(self):
        self.recursoCrud= RecursoCrud()
        self.personaServicios = PersonaServicios()
        self.activosServicios = ActivoServicios()

# region Servicios Basicos

    def get(self, id):
        return self.recursoCrud.readId(id)

    def getAll(self):
        return self.recursoCrud.readAll()

    def getDTO(self, recurso):
        recursoDTO= RecursoDTO(recurso,self.personaServicios,self.activosServicios)
        return recursoDTO.toJSON()

    def insert(self, recurso):
        return self.recursoCrud.create(recurso)

    def update(self, recurso):
        return self.recursoCrud.update(recurso)

    def delete(self, recurso):
        return self.recursoCrud.delete(recurso)

# endregion

# region Otros servicios
    def getRecursosByActivo(self,activo):

        return self.recursoCrud.whereList(activos = activo,isDisable = False)
# endregion
