from gestion_activos.DTO import ActivoDTO
from gestion_activos.crud import ActivoCrud
from gestion_roles.servicios.PersonaServicios import PersonaServicios

__author__ = 'JHONNY'

class ActivoServicios:

    def __init__(self):
        self.activoCrud= ActivoCrud()
        self.personaServicios = PersonaServicios()

# region Servicios Basicos
    def get(self, id):
        return self.activoCrud.readId(id)

    def getAll(self):
        return self.activoCrud.readAll()

    def getDTO(self, activo):
        activoDTO= ActivoDTO(activo,self.personaServicios,self)
        return activoDTO.toJSON()

    def insert(self, activo):
        return self.activoCrud.create(activo)

    def update(self, activo):
        return self.activoCrud.update(activo)

    def delete(self, activo):
        return self.activoCrud.delete(activo)

    def whereLike(self,query):
        return self.activoCrud.whereList(nombre__icontains = query)

    def convertListObjectsActive(self,list_id):
        activos = []
        for id_activo in list_id:
            activo = self.get(id_activo)
            activos.append(activo)

        return activos
# endregion






