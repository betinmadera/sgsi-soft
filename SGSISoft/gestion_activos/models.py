# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.db import models
from gestion_roles.models import Persona

class TipoRecurso(models.Model):
    nombre = models.CharField(max_length=250)
    padre = models.ForeignKey('self',null=True, blank=True)
    descripcion = models.TextField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class TipoActivo(models.Model):
    nombre = models.CharField(max_length=250)
    padre = models.ForeignKey('self',null=True, blank=True)
    descripcion = models.TextField()

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

# endregion

#region NoBorrables
class Activo(models.Model):
    codigo = models.CharField(max_length=10, unique=True)
    nombre = models.CharField(max_length=20)
    descripcion = models.TextField()
    dependencias = models.ManyToManyField("self", null=True, blank=True)
    responsables = models.ManyToManyField(Persona, null=True, blank=True)
    atributos = models.TextField()
    relevancia = models.CharField(max_length=15)
    cantidad = models.IntegerField(null = False)
    tipo = models.ForeignKey(TipoActivo,null=True, blank=True)


# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre

class Recurso(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    activos = models.ManyToManyField(Activo,null=False,blank=False)
    tipo = models.ForeignKey(TipoRecurso)
    usuarios = models.ManyToManyField(Persona,null=False,blank=False)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion

    def __unicode__(self):
        return self.nombre









    
