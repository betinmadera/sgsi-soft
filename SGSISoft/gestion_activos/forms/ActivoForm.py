# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django import forms
from django.forms.models import ModelForm
from gestion_activos.models import *
from gestion_activos.servicios import ActivoServicios
from util.Util import Util

class ActivoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        super(ActivoForm, self).__init__(*args, **kwargs)

        self.activoServicios = ActivoServicios()

    class Meta:
        model = Activo
        exclude = ["relevancia","atributos"]
        selectMultiple = {'class': 'chzn-select', 'multiple': '', 'tabindex': '15', 'style': 'width:300px'}
        select = {'data-placeholder': "Seleccionar", 'class': "chzn-select", 'tabindex': "2"}

        widgets = {
            'responsables': forms.SelectMultiple(attrs=selectMultiple),
            'dependencias': forms.SelectMultiple(attrs=selectMultiple),
            'tipo': forms.Select(attrs=select),

        }

    def save(self, commit=True):
        if not self.edit:
            activo = super(ActivoForm, self).save(commit=False)

            self.activoServicios.insert(activo)
            self.save_m2m()
        else:
            activo = super(ActivoForm, self).save(commit=False)

            self.save_m2m()
            self.activoServicios.update(activo)