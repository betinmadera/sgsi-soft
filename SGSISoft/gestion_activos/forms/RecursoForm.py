# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_activos.models import Recurso
from django import forms
from django.forms.models import ModelForm
from gestion_activos.servicios.RecursoServicios import RecursoServicios

class RecursoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        super(RecursoForm, self).__init__(*args, **kwargs)

        self.recursoServicios = RecursoServicios()

    class Meta:
        model = Recurso

        selectMultiple = {'class': 'chzn-select', 'multiple': '', 'tabindex': '15', 'style': 'width:300px'}
        select = {'data-placeholder': "Seleccionar", 'class': "chzn-select", 'tabindex': "2"}

        widgets = {
            'activos': forms.SelectMultiple(attrs=selectMultiple),
            'usuarios': forms.SelectMultiple(attrs=selectMultiple),
            'tipo':forms.Select(attrs=select)
        }

    def save(self, commit=True):
        if not self.edit:
            recurso = super(RecursoForm, self).save(commit=False)

            self.recursoServicios.insert(recurso)
            self.save_m2m()
        else:
            recurso = super(RecursoForm, self).save(commit=False)

            self.save_m2m()
            self.recursoServicios.update(recurso)
