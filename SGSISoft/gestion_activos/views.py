__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

# Create your views here.
import json
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from gestion_activos.forms import ActivoForm
from gestion_activos.servicios import ActivoServicios
from util.Util import Util
from gestion_activos.forms import RecursoForm
from gestion_activos.servicios.RecursoServicios import RecursoServicios
from gestion_activos.models import Activo

activoServicios = ActivoServicios()
recursoServicios = RecursoServicios()

# region Active Render
def activeRender(request,id):
    if request.method == 'GET':
        activo = activoServicios.get(id)
        recursos = recursoServicios.getRecursosByActivo(activo)
    return Util.pagina(request, "gestion_activos/activo_detalle.html", {'activo':activo, 'recursos':recursos})

def activeNewRender(request):
    if request.method == 'POST':
        formulario = ActivoForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect(reverse('lActivo'))
    else:
        formulario = ActivoForm()

    return Util.pagina(request, "gestion_activos/activo_form.html", {'formulario':formulario})


def activeEditRender(request, id):
    activo = activoServicios.get(id)
    if request.method == 'POST':
        formulario = ActivoForm(request.POST, edit=True,instance=activo)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect(reverse('lActivo'))
    else:
        formulario = ActivoForm(instance=activo)

    return Util.pagina(request, 'gestion_activos/activo_form.html', {'formulario': formulario})


def activeAllRender(request):

    return Util.pagina(request, 'gestion_activos/activo_listar.html',{})


# endregion

# region Active Object
def activeRead(request):
    if request.is_ajax():
        activos = activoServicios.getAll()
        respuesta = [activoServicios.getDTO(activo) for activo in activos]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def activeSearch(request):
    if request.is_ajax():
        query = request.GET['query']
        activos = activoServicios.whereLike(query)
        respuesta = [activoServicios.getDTO(activo) for activo in activos]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)


def activeDelete(request):
    if request.is_ajax():
        idActivo = request.GET['idActivo']
        activo = activoServicios.get(idActivo)
        activoServicios.delete(activo)
        respuesta = []
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion

# region Resource Render
def resourceRender(request,id):
    if request.method == 'GET':
        recurso = recursoServicios.get(id)
    return Util.pagina(request, "gestion_activos/recurso_detalle.html", {'recurso':recurso})

def resourceNewRender(request):
    if request.method == 'POST':
        formulario = RecursoForm(request.POST)
        print formulario.errors
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect(reverse('lRecurso'))
    else:
        formulario = RecursoForm()

    return Util.pagina(request, "gestion_activos/recurso_form.html", {'formulario':formulario})


def resourceEditRender(request, id):
    recurso = recursoServicios.get(id)
    if request.method == 'POST':
        formulario = RecursoForm(request.POST, edit=True,instance=recurso)
        if formulario.is_valid():
            formulario.save()
            return HttpResponseRedirect(reverse('lRecurso'))
    else:
        formulario = RecursoForm(instance=recurso)

    return Util.pagina(request, 'gestion_activos/recurso_form.html', {'formulario': formulario})


def resourceAllRender(request):

    return Util.pagina(request, 'gestion_activos/recurso_listar.html',{})


# endregion


# region Resource Object
def resourceRead(request):
    if request.is_ajax():
        recursos = recursoServicios.getAll()
        respuesta = [recursoServicios.getDTO(recurso) for recurso in recursos]


        #serializado = json.dumps(respuesta, ensure_ascii=False)
        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def resourceDelete(request):
    if request.is_ajax():
        idRecurso = request.GET['idRecurso']
        recurso = recursoServicios.get(idRecurso)
        recursoServicios.delete(recurso)
        respuesta = []
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion

