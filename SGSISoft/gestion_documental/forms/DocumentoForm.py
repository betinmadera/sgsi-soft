# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_documental.servicios import DocumentoServicios
from django import forms
from django.forms.models import ModelForm
from gestion_documental.models import *

class DocumentoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        super(DocumentoForm, self).__init__(*args, **kwargs)
        #Definicion de elementos necesarios
        self.documentoServicio= DocumentoServicios()

    class Meta:
        model = Documento
        fields = ['titulo', 'descripcion', 'tipoDocumento', 'actividad']

        widgets = {
            'titulo':forms.TextInput(attrs={ 'required': 'true' }),
            'actividad': forms.HiddenInput()
        }

    def save(self, commit=True):
        if not self.edit:
            documento = super(DocumentoForm, self).save(commit=False)
            estado = EstadoDocumento.BORRADOR.getValor()
            tipo = TipoDocumento.INFORME.getValor()
            #Crear version 0.1
            documento.estadoDocumento = estado
            documento.tipoDocumento = tipo
            documento.version = "0.1"
            self.documentoServicio.insert(documento)
        else:
            documento = super(DocumentoForm, self).save(commit=False)
            self.documentoServicio.update(documento)