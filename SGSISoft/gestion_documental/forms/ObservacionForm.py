# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.forms.models import ModelForm
from gestion_documental.models import *
from django import forms

class ObservacionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        self.documento = kwargs.pop('documento', None)
        super(ObservacionForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Observacion
        fields = ['descripcion','documento','autor']
        atributos = {'class':'span12', 'rows':'6', 'cols': '0'}
        widgets = {'documento': forms.HiddenInput(),
                   'autor':forms.HiddenInput(),
                   'descripcion': forms.Textarea(attrs=atributos)
                   }

    def save(self, commit=True):
        if not self.edit:
            observacion = super(ObservacionForm, self).save(commit=False)
            #TODO: Cambiar por usos de servicios.
            observacion.save()
        else:
            observacion = super(ObservacionForm, self).save(commit=False)
            observacion.save()