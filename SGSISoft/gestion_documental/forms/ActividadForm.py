# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from util.Comunicaciones.NotificacionesUtil import NotificacionesUtil

from gestion_documental.servicios import ActividadServicios
from util.Comunicaciones.CorreoUtil import CorreoUtil
from django import forms
from django.forms.models import ModelForm
from gestion_documental.models import *
from datetime import date

class ActividadForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        self.id = kwargs.pop('id', None)
        self.user = kwargs.pop('user', None)
        super(ActividadForm, self).__init__(*args, **kwargs)

        #Definicion de elementos necesarios
        self.actividadServicio= ActividadServicios()

    class Meta:
        model = Actividad
        exclude = ['estadoActividad', 'fecha_creacion']
        atributos = {'data-placeholder': "Seleccionar", 'class': "chzn-select", 'tabindex': "2"}
        actividad = {'title': 'Titulo para identificar la actividad', 'class': 'span8 text-tip'}
        descripcion = {'rows': '8', 'class': 'span8'}
        responsables = {'class': 'chzn-select', 'multiple': '', 'tabindex': '15', 'style': 'width:210px'}

        widgets = {
            'tipoActividad': forms.Select(attrs=atributos),
            'titulo': forms.TextInput(attrs=actividad),
            'descripcion': forms.Textarea(attrs=descripcion),
            'responsables': forms.SelectMultiple(attrs=responsables),
        }

    def clean(self):
        form = super(ActividadForm, self).clean()
        inicio = form.get("inicio")
        fin = form.get("fin")

        if (not (fin >= date.today() and inicio >= date.today())):
            raise forms.ValidationError("Las fechas deben ser mayores o iguales a la actual")
        if (inicio > fin):
            raise forms.ValidationError("Las fecha inicial debe ser menor que la fecha final")

        return form

    def save(self, commit=True):
        if not self.edit:
            #TODO: validar que la actividad tenga por lo menos un responsable.
            actividad = super(ActividadForm, self).save(commit=False)
            actividad.estadoActividad = EstadoActividad.ACTIVA.getValor()
            self.actividadServicio.insert(actividad)
            self.save_m2m()

            notificacionesUtil=NotificacionesUtil()
            notificacionesUtil.notificarNuevaActividad(self.user,actividad);
            correoUtil=CorreoUtil()
            correoUtil.correoCreacionActividad(actividad, actividad.responsables.all())
        else:
            actividad = super(ActividadForm, self).save(commit=False)

            self.save_m2m()
            self.actividadServicio.update(actividad)
            #TODO: notificar a los interesados del cambio de actividad.
