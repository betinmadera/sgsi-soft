# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

import mimetypes
from django.core import serializers
import json
from django.core.files.uploadedfile import UploadedFile
from django.http import HttpResponse, HttpResponseBadRequest
from django.template.defaultfilters import length
from django.template.loader import render_to_string
from django.utils import simplejson
from gestion_documental.forms import *
from gestion_documental.models import Observacion, Documento, VersionDocumento, Actividad, EstadoDocumento
from home.models import Log
from gestion_roles.views import personaServicios
from gestion_documental.servicios import ActividadServicios, DocumentoServicios, ObservacionServicios, VersionDocumentoServicios
from util.PermisosUtil import permisos_req
from util.Sesiones import SesionesUtil
from util.Util import *

from django.db.models.signals import post_save
from django.dispatch import receiver


# region serviciosUtilitarios
actividadServicios = ActividadServicios()
documentoServicios = DocumentoServicios()
observacionServicios = ObservacionServicios()
versionDocumentoServicios = VersionDocumentoServicios()
# endregion

# region Activity Render
def activityNewRender(request):
    """
    Crea el formulario creación de actividad
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.is_ajax():
        formulario = ActividadForm(initial={'inicio': Util.fechaActualFormateada(), 'fin': Util.fechaActualFormateada()})
        rendered = render_to_string('actividad_form.html', {'formulario': formulario})
        return HttpResponse(rendered, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)
    

def activityNewPost(request):
    """
    Crea una nueva actividad
    :param request: HttpRequest
    :return:HttpResponse
    """
    if request.is_ajax():
        if request.method == 'POST':
            formulario = ActividadForm(request.POST, user=request.user)
            if formulario.is_valid():
                formulario.save()
                return HttpResponse(json.dumps({"success": True}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"success": False, "response":"Formulario Invalido", 'errors': formulario.errors}), content_type="application/json", status=500)
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)


def activityEditRender(request, id):
    """
    crear el formulario edición de actividad
    :param request: HttpRequest
    :param id: id de la actividad a editar
    :return: HttpResponse
    """
    if request.is_ajax():
        actividad = actividadServicios.get(id)
        actividadForm = ActividadForm(instance=actividad)
        rendered = render_to_string('actividad_form.html', {'formulario': actividadForm, 'now': Util.fechaActualFormateada(), 'editId':id})
        return HttpResponse(rendered, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)


def activityEditPost(request, id):
    """
    Modifica los datos de una actividad
    :param request: HttpRequest
    :param id: int actividad a editar
    :return:HttpResponse
    """
    if request.is_ajax():
        if request.method == 'POST':
            actividad = actividadServicios.get(id)
            actividadForm = ActividadForm(request.POST, edit=True, user=request.user, instance=actividad)
            if actividadForm.is_valid():
                actividadForm.save()
                return HttpResponse(json.dumps({"success": True}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"success": False, "response":"Formulario Invalido", 'errors': actividadForm.errors}), content_type="application/json", status=500)
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)


@permisos_req
def activityUserRender(request, id):
    """
    lista las actividades de un usuario
    :param request: HttpRequest
    :param id: id del usuario cuyas actividades seran listadas
    :return: HttpResponse
    """
    actividades = actividadServicios.getActividadesByUserId(id)
    return Util.pagina(request, 'actividad_listar.html', {'actividades': actividades})


@permisos_req
def activityAllRender(request):
    """
    lista actividades del usuario conectado
    :param request: HttpRequest
    :return: HttpResponse
    """
    return Util.pagina(request, 'actividad_listar.html', {'tipo': 'admin'})


def activityAdminRender(request):
    #TODO: revisar. esto retorna todas las actividades creadas.
    return Util.pagina(request, 'actividad_listar.html', {'tipo': 'admin'})

@permisos_req
def activityRender(request, id):
    """
    Permite ver los detalles de una actividad
    :param request: HttpRequest
    :param id: id de la actividad
    :return: HttpResponse
    """

    actividad = actividadServicios.get(id)
    documentos = documentoServicios.getDocumentosByActividad(actividad)
    observ = observacionServicios.getObservacionesByActividad(actividad)

    datos = {'actividad': actividad, 'documentos': documentos, 'observaciones': observ,
             'conteo': length(observ)}

    return Util.pagina(request, 'actividad_detalle.html', datos)

def activityDocuments(request, id):
    if request.is_ajax():
        actividad = actividadServicios.get(id)
        documentos = documentoServicios.getDocumentosByActividad(actividad)
        respuesta = [documentoServicios.getDTO(documento) for documento in documentos]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")
    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion

# region Activity Object
def activityRead(request):
    if request.is_ajax():
        tipo = request.GET['type']
        if tipo == 'admin':
            actividades = actividadServicios.getAll()
            respuesta = [actividadServicios.getDTO(actividad) for actividad in actividades]
            serializado = json.dumps(respuesta)
            return HttpResponse(serializado, content_type="application/json")
        else:
            usuario = request.user
            actividades = actividadServicios.getActividadesByUsuario(usuario)
            respuesta = [actividadServicios.getDTO(actividad) for actividad in actividades]
            serializado = json.dumps(respuesta)

            return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def activityDelete(request): #TODO: hacer por medio de post con un crsf midleware token.
    if request.is_ajax():
        idActividad = request.GET['idActividad']
        actividad = actividadServicios.get(idActividad)
        actividadServicios.delete(actividad)
        respuesta = []
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion

# region Document Render
def documentNewRender(request, id):
    if request.method == 'POST':
        actividad = actividadServicios.get(id)
        documento = Documento(actividad=actividad)
        formulario = DocumentoForm(request.POST, instance=documento)
        if formulario.is_valid():
            formulario.save()
            respuesta = [{'titulo': documento.titulo, 'url': documento.url}]
            serializado = json.dumps(respuesta)
            return HttpResponse(serializado, content_type="application/json")
            #return HttpResponseRedirect(reverse('lDocumento'))

    else:
        actividad = actividadServicios.get(id)
        documento = Documento(actividad=actividad)
        formulario = DocumentoForm(instance=documento)

    return Util.pagina(request, "documento_form.html", {'formulario': formulario})


def documentEditRender(request, id):
    documento = documentoServicios.get(id)
    if request.method == 'POST':
        formulario = DocumentoForm(request.POST, edit=True, instance=documento)
        #TODO: quitar request.FIlES no es necesario. Los documentos se suben a otro servidor.
        if formulario.is_valid():
            formulario.save()
            return Util.redireccionar("lDocumento")
    else:
        formulario = DocumentoForm(instance=documento)

    return Util.pagina(request, "documento_form.html", {'formulario': formulario})

@permisos_req
def documentAllRender(request):
    if request.method == 'GET':
        return Util.pagina(request, "documento_listar.html", {'tipo': ''})

def documentRender(request, id):
    if request.method == "GET":
        documento = documentoServicios.get(id)
        observaciones = observacionServicios.getObservacionByDocumento(documento)
    return Util.pagina(request, "documento_detalle.html",
                       {'documento': documento, 'observaciones': observaciones, 'versiones': documento.versiones.order_by("-id"), "conteo":observaciones.__len__(), "conteoVesiones":documento.versiones.all().__len__()})


# def documentAllRender(request):
#     if request.method == 'GET':
#         return Util.pagina(request, "gestion_documental/documento_listar.html", {'tipo': ''})


def documentAdminRender(request):
    if request.method == 'GET':
        return Util.pagina(request, "documento_listar.html", {'tipo': 'admin'})


# endregion

# region Document Object
def documentRead(request):
    if request.is_ajax():
        tipo = request.GET['type']
        if tipo == 'admin':
            documentos = documentoServicios.getAll()
            respuesta = [documentoServicios.getDTO(documento) for documento in documentos]
            serializado = json.dumps(respuesta)
            return HttpResponse(serializado, content_type="application/json")
        else:
            usuario = request.user
            persona = personaServicios.getPersonaByUser(usuario)
            documentos = documentoServicios.listarByUsuario(persona)
            respuesta = [documentoServicios.getDTO(documento) for documento in documentos]
            serializado = json.dumps(respuesta)
            return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def documentDelete(request):
    if request.is_ajax():
        idDocumento = request.GET['idDocumento']
        documento = documentoServicios.get(idDocumento)
        documentoServicios.delete(documento)
        respuesta = []
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

def documentChangeStatus(request):
    if request.is_ajax():
        idDocumento = request.GET['idDocumento']
        idEstadoDocumento = request.GET['idEstadoDocumento']
        documentoServicios.cambiarEstadoDocumento(idDocumento, idEstadoDocumento, None)
        respuesta = [{'idDocumento': idDocumento, 'idEstado': idEstadoDocumento}]
        serializado = json.dumps(respuesta)
        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)


def documentUpload(request):
    if request.method == 'POST':
        if request.FILES == None:
            return HttpResponseBadRequest('¡Debe tener archivos!')

        #Se obtiene el archivo
        file = request.FILES[u'files']

        #Se crea la version
        vD = VersionDocumento()
        vD.archivo = file
        vD.version=str('0.0.0')
        versionDocumentoServicios.insert(vD)

        #Se obtienen los datos del archivo creado
        wrapped_file = UploadedFile(vD.archivo)
        filename = wrapped_file.name
        file_size = wrapped_file.file.size

        documentoCrear = Documento()
        formulario = DocumentoForm(request.POST, instance=documentoCrear)

        if formulario.is_valid():
            documentoCrear.versionActual= vD
            formulario.save()
            documentoCrear.versiones.add(vD)
            documentoCrear.save()
             #generating json response array
            files = []
            files.append({
                "name":filename,
                "size":file_size,
                "url":vD.archivo.url,
                "thumbnail_url":vD.archivo.url,
                "delete_url": '',
                "delete_type":"POST",
                #'type': mimetypes.guess_type(file.path)[0] or 'image/png',
            })
            data = {'files': files}
            response = JSONResponse(data, mimetype=response_mimetype(request))
            response['Content-Disposition'] = 'inline; filename=files.json'
            return HttpResponse(response, mimetype='application/json')
        else:
            return HttpResponse(json.dumps({"success": False, "response":"Formulario Invalido", 'errors': formulario.errors}), content_type="application/json", status=500)


def documentVersioning(request):
    if request.method == 'POST':
        if request.FILES == None:
            return HttpResponseBadRequest('¡Debe tener archivos!')


        #Se obtiene el archivo
        file = request.FILES[u'files']

        estado_doc = request.POST['_estadoDocumento']
        id_doc = request.POST['_documento']

        #se agrega la observacion
        descripcion = request.POST['textoObservacion']
        documento = documentoServicios.get(id_doc)
        persona = personaServicios.getPersonaByUser(request.user)
        observacion = Observacion(documento=documento, autor=persona, descripcion=descripcion)
        observacionServicios.crear(observacion)

        documentoServicios.cambiarEstadoDocumento(id_doc, estado_doc, file)

        files = []
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return HttpResponse(response, mimetype='application/json')

        #
        # #Se crea la version
        # vD = VersionDocumento()
        # vD.archivo = file
        # vD.version=str('0.0.0')
        # versionDocumentoServicios.insert(vD)
        #
        # #Se obtienen los datos del archivo creado
        # wrapped_file = UploadedFile(vD.archivo)
        # filename = wrapped_file.name
        # file_size = wrapped_file.file.size
        #
        # documentoCrear = Documento()
        # formulario = DocumentoForm(request.POST, instance=documentoCrear)
        #
        # if formulario.is_valid():
        #     documentoCrear.versionActual= vD
        #     formulario.save()
        #     documentoCrear.versiones.add(vD)
        #     documentoCrear.save()
        #      #generating json response array
        #     files = []
        #     files.append({
        #         "name":filename,
        #         "size":file_size,
        #         "url":vD.archivo.url,
        #         "thumbnail_url":vD.archivo.url,
        #         "delete_url": '',
        #         "delete_type":"POST",
        #         #'type': mimetypes.guess_type(file.path)[0] or 'image/png',
        #     })
        #     data = {'files': files}
        #     response = JSONResponse(data, mimetype=response_mimetype(request))
        #     response['Content-Disposition'] = 'inline; filename=files.json'
        #     return HttpResponse(response, mimetype='application/json')
        # else:
        #     return HttpResponse(json.dumps({"success": False, "response":"Formulario Invalido", 'errors': formulario.errors}), content_type="application/json", status=500)

# endregion

# region Observation Render
def observationRender(request, idDocumento):
    documento = documentoServicios.get(idDocumento)
    observaciones = observacionServicios.getObservacionByDocumento(documento)
    return Util.pagina(request, "gestion_documental/observaciones.html",
                       {'observaciones': observaciones, 'documento': documento})


# endregion

#region Obervation Object
def observationCreate(request):
    if request.is_ajax():
        idDocumento = request.GET['idDocumento']
        descripcion = request.GET['observacion']
        idEstadoDocumento = request.GET['idEstadoDocumento']
        documento = documentoServicios.get(idDocumento)


        persona = personaServicios.getPersonaByUser(request.user)
        observacion = Observacion(documento=documento, autor=persona, descripcion=descripcion)
        #Se crea la observacion

        observacionServicios.crear(observacion)
        documentoServicios.cambiarEstadoDocumento(idDocumento, idEstadoDocumento, None)
        respuesta = [{'id': idDocumento, 'observacion': descripcion}]
        serializado = json.dumps(respuesta)

        return HttpResponse(serializado, content_type="application/json")

    return HttpResponse("Pagina no encontrada", content_type="application/json", status=404)

# endregion

#region Calendar
@permisos_req
def calendarRender(request):
    usuario = request.user
    actividades = actividadServicios.getFechasActividades(usuario)

    return Util.pagina(request, 'calendario.html', {'actividades': actividades})
#endregion

@receiver(post_save, sender=Actividad)
def on_actividad_post_save(sender, **kwargs):
    """
    This method is called when a actividad is saved. crea el log de Actividad
    :param sender: Actividad almacenada.
    :param kwargs: variables adicionales de notificacion. Funcionan como metadata.
    :return:
    """
    actividad = kwargs['instance']
    log = Log(usuario=SesionesUtil.actualUser,
          titulo='creacion de actividad',
          descripcion= "{0:s} ha creado actividad con titulo {1:s}".format(SesionesUtil.actualUser.username, actividad.titulo))
    
    if not kwargs.get('created', False):
        log.titulo='actualizacion de actividad'
        log.descripcion= "{0:s} ha actualizado actividad con titulo {1:s}".format(SesionesUtil.actualUser.username, actividad.titulo)
    
    log.save()
    
@receiver(post_save, sender=Documento)
def on_documento_post_save(sender, **kwargs):
    """
    This method is called when a documento is saved. crea el log de Documento
    :param sender: Documento almacenado.
    :param kwargs: variables adicionales de notificacion. Funcionan como metadata.
    :return:
    """
    documento = kwargs['instance']
    log = Log(usuario=SesionesUtil.actualUser,
          titulo='creacion de documento',
          descripcion= "{0:s} ha creado documento con titulo {1:s}".format(SesionesUtil.actualUser.username, documento.titulo))

    if not kwargs.get('created', False):
        log.titulo='actualizacion de documento'
        log.descripcion = "{0:s} ha actualizado documento con titulo {1:s}".format(SesionesUtil.actualUser.username, documento.titulo)
    
    log.save()
    
@receiver(post_save, sender=Observacion)
def on_observacion_post_save(sender, **kwargs):
    """
    This method is called when a Observacion is saved. crea el log de Observacion
    :param sender: Observacion almacenada.
    :param kwargs: variables adicionales de notificacion. Funcionan como metadata.
    :return:
    """
    observacion = kwargs['instance']
    log = Log(usuario=SesionesUtil.actualUser,
          titulo='creacion de observacion',
          descripcion= "{0:s} ha creado observacion para el documento con titulo {1:s}".format(SesionesUtil.actualUser.username, observacion.documento.titulo))
          
    if not kwargs.get('created', False):
        log.titulo='actualizacion de observacion'
        log.descripcion= "{0:s} ha actualizado observacion para el documento con titulo {1:s}".format(SesionesUtil.actualUser.username, observacion.documento.titulo)
    
    log.save()