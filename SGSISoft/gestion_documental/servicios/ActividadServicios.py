# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from datetime import date
from gestion_documental.DTO import ActividadDTO
from gestion_documental.crud import ActividadCrud
from gestion_roles.servicios import PersonaServicios

class ActividadServicios:
    def __init__(self):
        self.personaServicios = PersonaServicios()
        self.actividadCrud = ActividadCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.actividadCrud.readId(id)

    def getAll(self):
        return self.actividadCrud.readAll()

    def getDTO(self, actividad):
        actividadDTO = ActividadDTO(actividad)
        return actividadDTO.toJSON()

    def insert(self, actividad):
        return self.actividadCrud.create(actividad)

    def update(self, actividad):
        return self.actividadCrud.update(actividad)

    def delete(self, actividad):
        return self.actividadCrud.delete(actividad)

    # endregion

    def getActividadesByPersona(self, persona):
        """
        Retorna todas las actividades asociadas a una persona
        :param persona:
        :return:
        """
        actividades = self.actividadCrud.whereList(responsables=persona)
        return actividades

    def getActividadesByUsuario(self, usuario):
        persona = self.personaServicios.getPersonaByUser(usuario)
        actividades = self.actividadCrud.whereList(responsables=persona, isDisable=False)
        return actividades

    def getActividadesByUserId(self, id):
        actividades = self.actividadCrud.whereList(responsables=id, isDisable=False)
        return actividades

    def getFechasActividades(self, usuario):
        persona = self.personaServicios.getPersonaByUser(usuario)
        actividades = self.getActividadesByPersona(persona)
        now = date.today()
        fechas = []

        for actividad in actividades:
            titulo = actividad.titulo
            inicio = actividad.inicio - now
            fin = actividad.fin - now

            item = {'titulo': titulo, 'inicio': inicio.days, 'fin': fin.days + 1}
            fechas.append(item)

        return fechas




