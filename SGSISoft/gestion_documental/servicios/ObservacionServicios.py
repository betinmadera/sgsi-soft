# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from django.shortcuts import get_object_or_404

from gestion_documental.DTO import ObservacionDTO
from gestion_documental.crud import ObservacionCrud
from gestion_documental.forms import ObservacionForm
from gestion_documental.models import Observacion, Documento
import DocumentoServicios

class ObservacionServicios:
    def __init__(self):
        self.documentoServicios = DocumentoServicios.DocumentoServicios()
        self.observacionCrud= ObservacionCrud()

# region Servicios Basicos
    def get(self, id):
        return self.observacionCrud.readId(id)

    def getAll(self):
        return self.observacionCrud.readAll()

    def getDTO(self, observacion):
        observacionDTO= ObservacionDTO(observacion)
        return observacionDTO.toJSON()

    def insert(self, observacion):
        return self.observacionCrud.create(observacion)

    def update(self, observacion):
        return self.observacionCrud.update(observacion)

    def delete(self, observacion):
        return self.observacionCrud.delete(observacion)
# endregion

    def crear(self, observacion):
        self.observacionCrud.create(observacion)

    def listarByDocumento(self, id):
        documento = get_object_or_404(Documento, pk=id)
        #TODO: remover get_object_or_404
        observaciones = self.observacionesByDocumento(documento)
        return observaciones

    def observacionesByDocumento(self, documento):
        observaciones = Observacion.objects.filter(documento=documento).order_by("-id")[:10]
        datos = {'observaciones': observaciones, 'documento': documento}
        return datos

    def getObservacionByDocumento(self, documento):
        observacion = self.observacionCrud.whereList(documento=documento)#.order_by("-id")
        return observacion

    def getObservacionesByActividad(self, actividad):
        documentos= self.documentoServicios.getDocumentosByActividad(actividad)
        observaciones = []
        for documento in documentos:
            observacion = self.getObservacionByDocumento(documento)
            for obser in observacion:
                observaciones.append(obser)
        observ = sorted(observaciones[:10], key=lambda observacion: observacion.createDate, reverse=True)
        return observ

    def getFormObservacionesByDocumentos(self, documentos, persona):
        formularios = []

        for documento in documentos:
            observacion = Observacion(documento=documento, autor=persona)
            formulario = ObservacionForm(instance=observacion)
            form = {'formulario': formulario, 'documento': documento}
            formularios.append(form)
        return formularios



