# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_documental.DTO import DocumentoDTO
from gestion_documental.crud import DocumentoCrud
from gestion_documental.models import EstadoDocumento, VersionDocumento
import ActividadServicios
from util.Util import Util

class DocumentoServicios:
    def __init__(self):
        self.actividadServicios = ActividadServicios.ActividadServicios()
        self.documentoCrud = DocumentoCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.documentoCrud.readId(id)

    def getAll(self):
        return self.documentoCrud.readAll()

    def getDTO(self, documento):
        acciones = self.getAccionesForDocumento(documento.estadoDocumento)
        documentoDTO = DocumentoDTO(documento, acciones)
        return documentoDTO.toJSON()

    def insert(self, documento):
        return self.documentoCrud.create(documento)

    def update(self, documento):
        return self.documentoCrud.update(documento)

    def delete(self, documento):
        return self.documentoCrud.delete(documento)

    # endregion

    def getDocumentosByActividad(self, actividad):
        documentos = self.documentoCrud.whereList(actividad=actividad, isDisable=False)
        return documentos

    def listarByUsuario(self, persona):
        documents = []
        actividades = self.actividadServicios.getActividadesByPersona(persona)
        for actividad in actividades:
            documentos = self.getDocumentosByActividad(actividad)
            for documento in documentos:
                documents.append(documento)

        #datos = {'documentos': documents, 'formularios': formularios}
        return documents

    def cambiarEstadoDocumento(self, idDocumento, idEstadoDoc, file):
        documento = self.get(int(idDocumento))
        estado = EstadoDocumento.get(int(idEstadoDoc))


        if int(idEstadoDoc) == EstadoDocumento.PUBLICADO.getValor():
            vdNew = VersionDocumento()
            vdNew.archivo = documento.versionActual.archivo
            vdNew.version = Util.versionDocumento(documento.versionActual.version, estado)
            vdNew.save()

            documento.versionActual= vdNew
            documento.versiones.add(vdNew)
            documento.estadoDocumento = estado.getValor()
        elif estado.getValor() == EstadoDocumento.REVISION.getValor():
            vdNew = VersionDocumento()
            vdNew.archivo = file
            vdNew.version = Util.versionDocumento(documento.versionActual.version, estado)
            vdNew.save()

            documento.versionActual= vdNew
            documento.versiones.add(vdNew)
            documento.estadoDocumento = estado.getValor()
        elif estado.getValor() == EstadoDocumento.VERIFICAR.getValor():
            vdNew = VersionDocumento()
            vdNew.archivo = file
            vdNew.version = Util.versionDocumento(documento.versionActual.version, estado)
            vdNew.save()

            documento.versionActual= vdNew
            documento.versiones.add(vdNew)
            documento.estadoDocumento = estado.getValor()
        else:
            documento.estadoDocumento = estado.getValor()

        #TODO: Revisar la actualizaciónde los objetos
        self.documentoCrud.update(documento)

    def getAccionesForDocumento(self, estado_doc):
        acciones = []
        if (estado_doc == EstadoDocumento.BORRADOR.getValor()):
            acciones.append(EstadoDocumento.REVISION.__dict__)
        elif (estado_doc == EstadoDocumento.APROBADO.getValor()):
            acciones.append(EstadoDocumento.PUBLICADO.__dict__)
        elif (estado_doc == EstadoDocumento.REVISION.getValor()):
            acciones.append(EstadoDocumento.VERIFICAR.__dict__)
            acciones.append(EstadoDocumento.APROBADO.__dict__)
        elif (estado_doc == EstadoDocumento.VERIFICAR.getValor()):
            acciones.append(EstadoDocumento.APROBADO.__dict__)
            acciones.append(EstadoDocumento.VERIFICAR.__dict__)
            acciones.append(EstadoDocumento.BORRADOR.__dict__)
        elif (estado_doc == EstadoDocumento.PUBLICADO.getValor()):
            acciones.append(EstadoDocumento.BORRADOR.__dict__)
        return acciones
















