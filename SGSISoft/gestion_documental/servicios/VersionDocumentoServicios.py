# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_documental.DTO import VersionDocumentoDTO
from gestion_documental.crud import VersionDocumentoCrud

class VersionDocumentoServicios:
    def __init__(self):
        self.versionDocumentoCrud = VersionDocumentoCrud()

    # region Servicios Basicos
    def get(self, id):
        return self.versionDocumentoCrud.readId(id)

    def getAll(self):
        return self.versionDocumentoCrud.readAll()

    def getDTO(self, versionDocumento):
        versionDocumentoDTO = VersionDocumentoDTO(versionDocumento)
        return versionDocumentoDTO.toJSON()

    def insert(self, versionDocumento):
        return self.versionDocumentoCrud.create(versionDocumento)

    def update(self, versionDocumento):
        return self.versionDocumentoCrud.update(versionDocumento)

    def delete(self, versionDocumento):
        return self.versionDocumentoCrud.delete(versionDocumento)

    # endregion