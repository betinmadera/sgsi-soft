# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_documental.models import VersionDocumento
from util.Sesiones import SesionesUtil
from util.Util import Util

class VersionDocumentoCrud:

    def create(self, versionDocumento):
        versionDocumento.setCreateUser(SesionesUtil.actualUser.username)
        versionDocumento.save()

# region Read
    def readAll(self, isDisable=False):
        return self.whereList(isDisable=isDisable)

    def readId(self, id, isDisable=False):
        return self.whereObject(pk=id, isDisable=isDisable)

    def readRango(self, inicio, fin, isDisable=False):
        return self.whereList(isDisable=isDisable)[inicio:fin]

    def whereObject(self, *args, **kwargs):
        queryset = Util._get_queryset(VersionDocumento)
        try:
            versionDocumento =  queryset.get(*args, **kwargs)
            return versionDocumento
        except queryset.model.DoesNotExist:
            return None

    def whereList(self, *args, **kwargs):
        queryset = Util._get_queryset(VersionDocumento)
        obj_list = list(queryset.filter(*args, **kwargs))
        if not obj_list:
            obj_list=list()

        return obj_list

# endregion

    def update(self, versionDocumento):
        versionDocumento.setUpdateUser(SesionesUtil.actualUser.username)
        versionDocumento.save()


    def delete(self, versionDocumento):
        versionDocumento.setUpdateUser(SesionesUtil.actualUser.username)
        versionDocumento.setDisable(True)
        versionDocumento.save()
