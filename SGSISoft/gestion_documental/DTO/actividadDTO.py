# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_documental.models import TipoActividad, EstadoActividad
from gestion_roles.servicios.PersonaServicios import PersonaServicios

from home.DTO import DTO

class ActividadDTO(DTO):
    def __init__(self, activity):
        personaServicios = PersonaServicios()
        self.id= activity.id
        self.titulo= activity.titulo
        self.inicio= activity.inicio.isoformat()
        self.tipoID = activity.tipoActividad
        self.estadoID = activity.estadoActividad
        self.tipo= str(TipoActividad.get(activity.tipoActividad).getNombre())
        self.estado= str(EstadoActividad.get(activity.estadoActividad).getNombre())
        self.estiloEstado= str(EstadoActividad.get(activity.estadoActividad).estilo)
        self.descripcion= activity.descripcion
        self.responsables = [personaServicios.getDTO(x) for x in activity.responsables.all()]
        self.fin= activity.fin.isoformat()