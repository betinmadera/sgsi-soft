# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from home.DTO import DTO

class ObservacionDTO(DTO):
    def __init__(self, observacion):
        self.id=observacion.id
        self.autorId= observacion.autor.id
        self.autor= "{0:s} {1:s}".format(observacion.autor.nombre,observacion.autor.apellido)
        self.descripcion= observacion.descripcion
        self.creacion= observacion.creacion.isoformat()
        self.documentoId= observacion.documento.id