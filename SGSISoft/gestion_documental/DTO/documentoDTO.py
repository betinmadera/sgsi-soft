# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"
from gestion_documental.models import EstadoDocumento, TipoDocumento
from home.DTO import DTO

class DocumentoDTO(DTO):

    def __init__(self, documento,acciones):
        self.id= documento.id
        self.titulo= documento.titulo
        self.tipoID = documento.tipoDocumento
        self.estadoID = documento.estadoDocumento
        self.tipo= str(TipoDocumento.get(documento.tipoDocumento).getNombre())
        self.estado= str(EstadoDocumento.get(documento.estadoDocumento).getNombre())
        self.estiloEstado= str(EstadoDocumento.get(documento.estadoDocumento).estilo)
        self.iconEstado= str(EstadoDocumento.get(documento.estadoDocumento).icon)
        self.version= documento.versionActual.version
        self.inicio = documento.actividad.inicio.isoformat()
        self.fin = documento.actividad.fin.isoformat()
        self.actualizacion = documento.updateDate.isoformat()
        self.actividadID = documento.actividad.id
        self.url = documento.versionActual.archivo.url
        self.acciones = acciones
