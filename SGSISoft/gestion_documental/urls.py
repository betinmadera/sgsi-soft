# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from gestion_documental.views import *
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    #url(r'^$', index, name='documental.index'),
    #views Render
    url(r'^document/new/([\d]+)$', documentNewRender, name='nDocumento'),#Mostrar: pagina para CREAR documento
    url(r'^document/edit/([\d]+)$', documentEditRender, name='eDocumento'),#Mostrar: pagina para EDITAR
    url(r'^document$', documentAllRender, name='lDocumento'),#Mostrar: pagina para listar documentos del usuario 'mis documentos'
    url(r'^document/admin$', documentAdminRender, name='allDocumento'),#Mostrar: pagina para mostrar TODOS los documentos
    url(r'^document/([\d]+)$', documentRender, name='vDocumento'),#Detalle: informacion de un documento
    url(r'^document/upload$', documentUpload, name='uploadDocumento'),#Detalle: informacion de un documento
    url(r'^document/versioning$', documentVersioning, name='versioningDocumento'),#Detalle: informacion de un documento

    url(r'^document/all$', documentRead, name='documentRead'),#Listar: documentos
    url(r'^document/delete$', documentDelete, name='documentDelete'),#Eliminar documento
    url(r'^document/change/status$', documentChangeStatus, name='documentChangeStatus'),#Actualizar: modificar estado de documento

    url(r'^observation/create$', observationCreate, name='observationCreate'),#Crear: observacion
    url(r'^observation/document/([\d]+)$', observationRender, name='oDocumento'),#Listar: observaciones de documento determinado

    url(r'^activity$', activityAllRender, name='lActividad'),#Mostrar: pagina para listar actividades del usuario conectado
    url(r'^activity/all$', activityRead, name='activityRead'),#Listar: actividades
    url(r'^activity/new$', activityNewRender, name='nActividad'),#Mostrar: formulario para crear actividad
    url(r'^activity/new_ajax$', activityNewPost, name='nActividad_ajax'),#ajax: Ajax para CREAR actividad.
    url(r'^activity/edit/([\d]+)$', activityEditRender, name='eActividad'),#Mostrar: formulario para EDITAR actividad
    url(r'^activity/edit_ajax/([\d]+)$', activityEditPost, name='eActividad_ajax'),#ajax: ajax para EDITAR actividad
    url(r'^activity/delete$', activityDelete, name='activityDelete'),#Eliminar: actividades
    url(r'^activity/user/([\d]+)$', activityUserRender, name='uActividad'),#Mostrar: pagina LISTAR actividades de usuario TODO: quitar.
    url(r'^activity/admin$', activityAdminRender, name='allActividad'),#Mostrar : pagina para LISTAR TODAS las actividades
    url(r'^activity/([\d]+)$', activityRender, name='vActividad'),#Detalle: informacion de actividad
    url(r'^activity/documents/([\d]+)$', activityDocuments, name='vActividadDocuments'),#Detalle: documentos asociados a una actividad.

    url(r'^calendar$', calendarRender, name='calendario'),#Mostrar pagina de inicio con calendario

)