# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

import uuid
import os
from django.db import models

# Create your models here.
from gestion_roles.models import Persona
from util.Util import EnumElemento

# region Enumeraciones
class TipoActividad:
    ISO=EnumElemento("ISO", 1)
    INFORME=EnumElemento("Informe", 2)

    @staticmethod
    def listaEnumerada():
        return [(value.getValor(), value.getNombre()) for key, value in TipoActividad.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def listaElementos():
        return [value for key, value in TipoActividad.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def get(id):
        valord= [value for key, value in TipoActividad.__dict__.iteritems() if isinstance(value, EnumElemento) and value.getValor()==id][0]
        return valord

class EstadoActividad:
    CREADA=EnumElemento("Creada", 3,estilo = 'label-default')
    ACTIVA=EnumElemento("Activa", 1, estilo = 'label-success')
    INACTIVA=EnumElemento("Inactiva", 2,estilo = 'label-default')
    RESUELTA=EnumElemento("Resuelta", 3,estilo = 'label-default')
    CERRADA=EnumElemento("Cerrada", 3,estilo = 'label-warning')

    @staticmethod
    def listaEnumerada():
        return [(value.getValor(), value.getNombre()) for key, value in EstadoActividad.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def listaElementos():
        return [value for key, value in EstadoActividad.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def get(id):
        valord= [value for key, value in EstadoActividad.__dict__.iteritems() if isinstance(value, EnumElemento) and value.getValor()==id][0]
        return valord

class TipoDocumento:
    ISO=EnumElemento("ISO", 1)
    INFORME=EnumElemento("Informe", 2)

    @staticmethod
    def listaEnumerada():
        return [(value.getValor(), value.getNombre()) for key, value in TipoDocumento.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def listaElementos():
        return [value for key, value in TipoDocumento.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def get(id):
        valord= [value for key, value in TipoDocumento.__dict__.iteritems() if isinstance(value, EnumElemento) and value.getValor()==id][0]
        return valord

class EstadoDocumento:
    BORRADOR=EnumElemento("Borrador", 1, estilo='label-default', icon='')
    VERIFICAR=EnumElemento("Verificar", 2 , estilo='label-info',icon = 'icon-edit')
    REVISION=EnumElemento("Revisión", 3, estilo='label-warning', icon = 'icon-check')
    APROBADO=EnumElemento("Aprobado", 4, estilo = 'label-inverse',icon = 'icon-ok')
    PUBLICADO=EnumElemento("Publicado", 5, estilo = 'label-success', icon = 'icon-globe')

    @staticmethod
    def listaEnumerada():
        return [(value.getValor(), value.getNombre()) for key, value in EstadoDocumento.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def listaElementos():
        return [value for key, value in EstadoDocumento.__dict__.iteritems() if isinstance(value, EnumElemento)]

    @staticmethod
    def get(id):
        valord = [value for key, value in EstadoDocumento.__dict__.iteritems() if isinstance(value, EnumElemento) and value.getValor()==id][0]
        return valord
# endregion

# region NoBorrables
class Actividad(models.Model):
    titulo = models.CharField(max_length=50)
    inicio = models.DateField()
    fin = models.DateField()
    descripcion = models.TextField()
    estadoActividad = models.IntegerField(choices=EstadoActividad.listaEnumerada())
    tipoActividad = models.IntegerField(choices=TipoActividad.listaEnumerada())
    responsables = models.ManyToManyField(Persona)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion
    def __unicode__(self):
        return self.titulo


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('files', filename)

class VersionDocumento(models.Model):
    version = models.TextField()
    archivo = models.FileField(upload_to=get_file_path)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion
    def __unicode__(self):
        return self.version

class Documento(models.Model):
    titulo = models.CharField(max_length=50)
    descripcion = models.TextField(blank=True)
    estadoDocumento = models.IntegerField(choices=EstadoDocumento.listaEnumerada())
    tipoDocumento = models.IntegerField(choices=TipoDocumento.listaEnumerada())
    actividad = models.ForeignKey(Actividad)
    versiones = models.ManyToManyField(VersionDocumento, related_name='documento')
    versionActual = models.ForeignKey(VersionDocumento, related_name='documentoActual')

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion
    def __unicode__(self):
        return self.titulo

class Observacion(models.Model):
    autor = models.ForeignKey(Persona)
    descripcion = models.TextField()
    documento = models.ForeignKey(Documento)

# region Auditoria
    isDisable= models.BooleanField(default=False)
    createDate= models.DateTimeField(auto_now_add=True)
    createUser= models.TextField(null=True, blank=True)
    updateDate= models.DateTimeField(auto_now=True)
    updateUser= models.TextField(null=True, blank=True)

    def setDisable(self, disable):
        self.isDisable= disable

    def setUpdateUser(self, user):
        self.updateUser= user

    def setCreateUser(self, user):
        self.createUser= user
        self.updateUser= user
# endregion
    def __unicode__(self):
        return self.descripcion
# endregion




