# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import os
from util.Constantes import Constantes

from django.core.mail.message import EmailMessage

class CorreoUtil:

    def __enviarCorreo(self, titulo, contenido, correosDestino):
        correo = EmailMessage(titulo, contenido, to=correosDestino)
        correo.send()

    def __enviarCorreoDetallado(self, titulo, contenido, persona):
        """
        :param titulo:
        :param contenido:
        :param correo:
        """
        strFrom = Constantes.CORREO_ENVIO

        # Create the root message and fill in the from, to, and subject headers
        msgRoot = MIMEMultipart('related')
        msgRoot['Subject'] = titulo
        msgRoot['From'] = strFrom
        msgRoot['To'] = persona.email
        msgRoot.preamble = 'This is a multi-part message in MIME format.'

        # Encapsulate the plain and HTML versions of the message body in an
        # 'alternative' part, so message agents can decide which they want to display.
        msgAlternative = MIMEMultipart('alternative')
        msgRoot.attach(msgAlternative)

        mensajeAlternativo = contenido
        msgText = MIMEText(mensajeAlternativo)
        msgAlternative.attach(msgText)

        # We reference the image in the IMG SRC attribute by the ID we give it below
        textoHTMl = Constantes.CORREO_PLANTILLA.format(titulo, contenido, persona.nombre, persona.apellido)#TODO: cambiar a un archivo de plantillas
        msgText = MIMEText(textoHTMl, 'html')
        msgAlternative.attach(msgText)

        # This example assumes the image is in the current directory
        directorioImagenes = os.path.join(os.path.dirname(__file__), 'img_correo')
        fp = open('%s%sSGSISoft-Logo.jpg' % (directorioImagenes, os.sep), 'rb')

        msgImage = MIMEImage(fp.read())
        fp.close()

        # Define the image's ID as referenced above
        msgImage.add_header('Content-ID', '<image1>')
        msgRoot.attach(msgImage)

        # Send the email (this example assumes SMTP authentication is required)
        #try:
        smtp = smtplib.SMTP('smtp.gmail.com:587')
        smtp.starttls()
        smtp.login(Constantes.CORREO_ENVIO,Constantes.CORREO_ENVIO_PASS)
        smtp.sendmail(strFrom, persona.email, msgRoot.as_string())
        smtp.quit()
        #except Exception as detail:
         #   print detail

    def correoCreacionEmpleado(self, usuario, contrasena, persona):
        titulo = Constantes.TITULO_CORREO_CUENTA_CREACION
        contenido = Constantes.CONTENIDO_CORREO_CUENTA_CREACION.format(usuario, contrasena)
        self.__enviarCorreoDetallado(titulo, contenido, persona)

    def correoCreacionActividad(self, actividad, responsables):
        titulo = Constantes.TITULO_CORREO_ACTIVIDAD_CREACION
        contenido =Constantes.CONTENIDO_CORREO_ACTIVIDAD_CREACION.format(actividad.titulo, actividad.inicio.strftime("%d/%m/%Y"),actividad.fin.strftime("%d/%m/%Y"))
        for responsable in responsables:
            self.__enviarCorreoDetallado(titulo, contenido, responsable)
