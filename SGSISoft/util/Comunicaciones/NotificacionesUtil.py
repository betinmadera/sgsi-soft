# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from notifications import notify
from gestion_documental.servicios.ActividadServicios import ActividadServicios
from home.DTO.notificationDTO import NotificationDTO
from util.Constantes import Constantes
import json

class NotificacionesUtil:
    def __init__(self):
        self.actividadServicios = ActividadServicios()

    def getNotificacionesByUser(self, user):
        """
        retorna las notificaciones asociadas a un usuario
        :param user: Usuario en sesion
        :return: Lista de notificaciones
        """
        notifications = user.notifications.unread().order_by('timestamp')
        return notifications

    def getDTO(self, notification):
        notificationDTO = NotificationDTO(notification)
        return notificationDTO.toJSON()

    def __notificar(self, actor, recipients, verb, description):
         """
         Método que almacena las notificaciones y notifica a los usuarios asociados.
         :param actor: Quien envia los datos
         :param recipients: Quienes reciben las notificaciones
         :param verb: Título de la notificacion
         :param action_object: Objeto que implica la accion de notificar
         :param target: Objetivo donde se abre la notificacion
         :param description: Descripcion de la notificacion
         :return: void
         """

         for recipient in recipients:
            notify.send(
                actor,
                recipient=recipient.usuario,
                verb=verb,
                #action_object=action_object,
                #target=target,
                description=description
            )

    def notificarNuevaActividad(self, usuario, actividad):
        """
        Permite notificar la creacion de una nueva actividad
        :param usuario: Usuario que notifica
        :param actividad: actividad a ser notificada
        :return:
        """

        desc = json.dumps({"url": Constantes.NOTIFICACION_ASIGNACION_ACTIVIDAD_URL.format(str(actividad.id)) , "object": self.actividadServicios.getDTO(actividad)})
        self.__notificar(usuario, recipients=actividad.responsables.all(), verb= Constantes.NOTIFICACION_ASIGNACION_ACTIVIDAD.format(actividad.titulo),
                                            description= desc)
