# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

import re
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import resolve
from urlparse import urlparse
from django.http import HttpResponse
from gestion_roles.models import Permiso
from gestion_roles.servicios import PermisoServicios
from home.models import Log
from util.Util import Util


def permisos_req(f):
    def decorador(request, *args, **kwargs):
        permisoServicio= PermisoServicios()

        user = request.user
        user.menu = permisoServicio.getMenuByUser(user)
        user.recent = Log.objects.order_by("-fecha")[:4]

        if user.is_superuser:
            user.permisos = [4] #Se le da rol de administrador
            return f(request, *args, **kwargs)

        if f.func_name == 'home':
            user.permisos = [3] #Se le da rol de usuario
            return f(request, *args, **kwargs)

        url_r = request.get_full_path()
        path= f.func_globals['__package__']+"/"+f.__name__
        if user.username:
            perm= permisoServicio.getPermisoByUserAndUrl(user, path)
            if perm:
                user.permisos = perm
                return f(request, *args, **kwargs)
            else:
                return Util.redireccionar('inicio')
        return Util.redireccionar('inicio')

    decorador.__doc__ = f.__doc__
    decorador.__name__ = f.__name__
    return decorador

def permisos_req_rol(rol=None):
    def _permisos_req_rol(f):
        def decorator(request, *args, **kwargs):
            if rol is None:
                return HttpResponse("No tienes permiso para ejecutar esta accion", content_type="application/json", status=403)

            user = request.user
            if user.is_superuser:
                return f(request, *args, **kwargs)

            #Si el usuario no es Super se busca el permiso en el Referer
            if not "HTTP_REFERER" in request.META:
                return HttpResponse("No tienes permiso para ejecutar esta accion", content_type="application/json", status=403)
            view, args1, kwargs1 = resolve(urlparse(request.META['HTTP_REFERER'])[2])

            permisoServicio= PermisoServicios()
            path= f.func_globals['__package__']+"/"+view.__name__

            if user.username:
                perm= permisoServicio.getPermisoByUserAndUrl(user, path)
                exist = False
                for item in perm:
                    if item >= rol.getValor():
                        exist=True
                        break

                if exist==True:
                    return f(request, *args, **kwargs)
                else:
                    return HttpResponse("No tienes permiso para ejecutar esta accion", content_type="application/json", status=403)
            return HttpResponse("No tienes permiso para ejecutar esta accion", content_type="application/json", status=403)

            decorator.__doc__ = f.__doc__
            decorator.__name__ = f.__name__
        return decorator
    return _permisos_req_rol