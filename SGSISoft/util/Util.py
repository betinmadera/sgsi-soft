# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

import json
from random import choice
from datetime import datetime
from django.core.urlresolvers import reverse
from django.db.models.manager import Manager
from django.db.models.query import QuerySet
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from util.Constantes import Constantes

class Util:
    @staticmethod
    def contrasena_aleatoria():
        longitud = 10
        valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        p = ""
        p = p.join([choice(valores) for i in range(longitud)])
        return p

    @staticmethod
    def redireccionar(vista):
        return HttpResponseRedirect(reverse(vista))

    @staticmethod
    def pagina(request, pagina, diccionarioDatos):
        return render_to_response(pagina, diccionarioDatos, context_instance=RequestContext(request))

    @staticmethod
    def fechaActualFormateada():
        now= datetime.now()
        dateFormat = now.strftime("%d/%m/%Y") # fecha con formato
        return dateFormat

    @staticmethod
    def versionDocumento(versionActual,estado):
        #TODO: este metodo deberia estar con los documentos. en el modulo gestion documental

        versionFinal = ""
        caracteres = versionActual.split('.')
        size = len(caracteres)

        #versionar documento para revision
        if estado.getValor() == 3:#Revision
            valorModificar = caracteres[1]
            numero = int(valorModificar)
            valorModificar = numero + 1
            caracteres[1] = valorModificar
            caracteres[2] = int(0)

        elif estado.getValor() == 2:#Verificar
            valorModificar = caracteres[2]
            numero = int(valorModificar)
            valorModificar = numero + 1
            caracteres[2] = valorModificar

        elif estado.getValor() == 5:#publicado
            #versionar documento para publicacion
            valorModificar = caracteres[0]
            numero = int(valorModificar)
            valorModificar = numero + 1
            caracteres[0] = valorModificar
            caracteres[1] = int(0)
            caracteres[2] = int(0)

        contador = 1

        for caracter in caracteres:
            if contador == size:
                versionFinal += str(caracter)
            else:
                versionFinal += str(caracter) + "."
            contador += 1

        return versionFinal

    @staticmethod
    def _get_queryset(klass):
        """
        retorna un QuerySet de un modelo, Manager, or QuerySet.
        """
        if isinstance(klass, QuerySet):
            return klass
        elif isinstance(klass, Manager):
            manager = klass
        else:
            manager = klass._default_manager
        return manager.all()

    @staticmethod
    def getEdad(fecha_nacimiento):
        hoy = datetime.now().date()
        edad = hoy.year - fecha_nacimiento.year
        mesNacimiento = fecha_nacimiento.month
        diaNacimiento = fecha_nacimiento.day
        mes = hoy.month
        dia = hoy.day

        if mes < mesNacimiento:
            edad = edad-1
        elif mes == mesNacimiento:
            if dia < diaNacimiento:
                edad= edad-1

        return edad

    @staticmethod
    def calcularaRelevancia(impactos):

        relevancia = impactos[0] * Constantes.IMPACTO_LEGAL + impactos[1] * Constantes.IMPACTO_IMAGEN + impactos[2] * Constantes.IMPACTO_CONFIANZA + impactos[3] * Constantes.IMPACTO_INTERES
        resultado = ""
        print relevancia
        if relevancia >= 27 and relevancia <= 35:
            resultado = "Alta"

        elif relevancia >= 17 and relevancia <= 26.5:

            resultado = "Media"

        elif relevancia >= 7 and relevancia <= 16.5:
            resultado = "Baja"

        return resultado


class EnumElemento:

    def __init__(self, nombre, valor, **kwargs):
        self.nombre=nombre
        self.valor=valor
        for key, value in kwargs.items():
            setattr(self, key, value)

    def getValor(self):
        return self.valor

    def getNombre(self):
        return self.nombre


MIMEANY = '*/*'
MIMEJSON = 'application/json'
MIMETEXT = 'text/plain'


def response_mimetype(request):
    """response_mimetype -- Return a proper response mimetype, accordingly to
    what the client accepts, as available in the `HTTP_ACCEPT` header.

    request -- a HttpRequest instance.

    """
    can_json = MIMEJSON in request.META['HTTP_ACCEPT']
    can_json |= MIMEANY in request.META['HTTP_ACCEPT']
    return MIMEJSON if can_json else MIMETEXT

class JSONResponse(HttpResponse):
    """JSONResponse -- Extends HTTPResponse to handle JSON format response.

    This response can be used in any view that should return a json stream of
    data.

    Usage:

        def a_iew(request):
            content = {'key': 'value'}
            return JSONResponse(content, mimetype=response_mimetype(request))

    """
    def __init__(self, obj='', json_opts=None, mimetype=MIMEJSON, *args, **kwargs):
        json_opts = json_opts if isinstance(json_opts, dict) else {}
        content = json.dumps(obj, **json_opts)
        super(JSONResponse, self).__init__(content, mimetype, *args, **kwargs)
















