# -*- encoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from util.Constantes import Constantes

from django.contrib.auth import logout
from django.contrib import messages
import datetime


class SessionIdleTimeout:
    """Middleware class to timeout a session after a specified time period.
    """
    def process_request(self, request):
        # Timeout is done only for authenticated logged in users.
        if request.user.is_authenticated():
            current_datetime = datetime.datetime.now()

            # Timeout if idle time period is exceeded.
            if request.session.has_key('last_activity') and \
                (current_datetime - request.session['last_activity']).seconds > \
                Constantes.DURACION_SESION:
                logout(request)
                messages.add_message(request, messages.ERROR, 'Su sesión ha expirado')

            # Set last activity time in current session.
            else:
                SesionesUtil.actualUser= request.user
                request.session['last_activity'] = current_datetime

        return None

class SesionesUtil:
    actualUser = None