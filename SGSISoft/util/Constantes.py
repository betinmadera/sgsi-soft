# -*- encoding: utf-8 -*-
# -*- decoding: utf-8 -*-
__author__ = "Andres Betin, Jhonny Madera y Raul Martelo"
__copyright__ = "Copyright 2016, Universidad de Cartagena"
__credits__ = ["Andres Betin", "Jhonny Madera", "Raul Martelo",
                    "Grupo de Investigacion GIMATICA",
                    "Universidad de Cartagena"]
__license__ = "GPL"
__version__ = "1.0.0"
__status__ = "Production"

from SGSISoft import settings


class Constantes:
    #Constantes de la aplicacion
    NOMBRE_APLICACION="SGSISoft"
    AUTORES_APLICACION="Andres Betin - Jhonny Madera"

    #Constantes para el host.
    #if settings.DEBUG:
    HOST= "http://localhost:8000/"
    HOST_NODE= "http://localhost:8004/"
    #else:
    #    HOST= "http://andresbetin.koding.io:8000/"
    #    HOST_NODE= "http://andresbetin.koding.io:8004/"

    #constantes para calculo de relevancia de un activo
    IMPACTO_LEGAL = 2.5
    IMPACTO_IMAGEN = 2
    IMPACTO_CONFIANZA = 1.5
    IMPACTO_INTERES = 1

    #600
    DURACION_SESION = 6000

    NOTIFICACION_ASIGNACION_ACTIVIDAD = 'Le ha sido asignada la actividad {0:s}'
    NOTIFICACION_ASIGNACION_ACTIVIDAD_URL= "documental/activity/{0:s}"

    NOTIFICACION_ACTIVIDAD_PENDIENTE_TITULO = 'Actividad pendiente'
    NOTIFICACION_ACTIVIDAD_PENDIENTE_DESC2 = ' en la fecha '

    TIPO_EVENTO_INFO = 'info'
    TIPO_EVENTO_COMPLETADO = 'completado'
    TIPO_EVENTO_ADVERTENCIA = 'advertencia'
    TIPO_EVENTO_ERROR = 'error'

    ERROR_INGRESO_ACTIVACION= "El usuario no ha sido activado"
    ERROR_CONTRASENAS_NO_COINCIDEN= "Las contrasenas no coinciden"
    ERROR_CONTRASENA_INCORRECTA= "Contrasena incorrecta"
    ERROR_USUARIO_NO_EXISTE = "El usuario no se ha encontrado"

    #Configuración de los correos.
    CORREO_ENVIO='infosgsisoft@gmail.com'
    CORREO_ENVIO_PASS= '5432ytrewq'
    CORREO_CONTACTO='infosgsisoft@gmail.com'
    CORREO_PLANTILLA="""

<body>
<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#e6e6e6">
    <tbody>
    <tr>
        <td width="100%" bgcolor="#e6e6e6">
            <table width="584" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                    <td width="584"><br> <br>
                        <table width="583" cellspacing="0" cellpadding="0" style="display:block">
                            <tbody>
                            <tr>
                                <td width="583" bgcolor="#000000"><a name="144d265b9da76ce7_header graphic"
                                                                     style="display:block;margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;border:none"
                                                                     href="http://sgsisoft-sgsisoft.rhcloud.com"
                                                                     target="_blank"><img width="583" height="70"
                                                                                          border="0"
                                                                                          style="display:block;margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;border:none"
                                                                                          alt="SGSISoft"
                                                                                          src="cid:image1"></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
						<div style="background-color:#ffffff;">
                        <table width="584" cellspacing="0" cellpadding="0" style="display:block" bgcolor="#ffffff">
                            <tbody>
                            <tr>
                                <td width="580"
                                    style="background-color:#fff;background-repeat:repeat-y;padding-left:2px;padding-right:2px">
                                    <table width="580" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td width="600"
                                                style="background-color:#ffffff;background-repeat:repeat-x;padding:20px 40px 20px 40px">
                                                <p
                                                   style="font-family:Helvetica,Verdana,arial,helvetica,sans-serif;font-weight:normal!important;font-size:24px;line-height:33px;color:#7e7d7e;margin-top:10px;margin-left:0;margin-bottom:15px;margin-right:0;padding-top:10px;padding-bottom:0px;font-weight:bold">
                                                    {0:s}</p>
												<p style="font-family:Helvetica,Verdana,arial,helvetica,sans-serif;font-weight:normal;font-size:15px;line-height:18px;color:#7e7d7e;margin-top:20px;margin-left:0;margin-bottom:10px;margin-right:0;padding:0">
													<b>{2:s} {3:s}</b>,
													<br/>
													<br/>
													{1:s}</p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
						</div>
                        <table cellspacing="0" cellpadding="10" border="0" align="left" style="width:100%">
                            <tbody>
                            <tr>
                                <td align="center">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td align="left" style="padding-top:10px"><p
                                                    style="font-family:helvetica,arial,sans-serif;font-size:11px;line-height:17px;color:#7e7d7e;margin-top:0;margin-bottom:0px;margin-left:0;margin-right:0;padding:0">
                                                <span lang="es">Para mayor informaci&oacute;n, comun&iacute;cate con nosotros a trav&eacute;s de </span><a
                                                    href="mailto:infosgsisoft@gmail.com"
                                                    style="color:#7e7d7e">infosgsisoft@gmail.com</a>.
													</span>
                                            </p></td>
                                        </tr>
                                        </tbody>
                                    </table>
								</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
                 """

    #:
    TITULO_CORREO_CUENTA_CREACION="Creacion Cuenta {0:s}".format(NOMBRE_APLICACION)
    CONTENIDO_CORREO_CUENTA_CREACION= """
    Usuario: {0:s}
    <br/>
    Contrase&ntilde;a: {1:s}
    """

    #Plantillas de correo para Aplicaccion
    TITULO_CORREO_ACTIVIDAD_CREACION= "Nueva Actividad {0:s}".format(NOMBRE_APLICACION)
    CONTENIDO_CORREO_ACTIVIDAD_CREACION= """
    Titulo: {0:s}
    <br/>
    Inicio: {1:s}
    <br/>
    Fin: {2:s}
    """

